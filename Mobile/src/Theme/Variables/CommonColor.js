
module.exports = {

  themeColor:"#00aff0",
  defaultTextColor:"#585858",
  defaultLightTextColor:"#fff",
  defaultThemeTextColor:"#00aff0",
  headerColor: "#313d3f",
  dangerColor: "#FF0000",
  
  get getDefaultTextColor() {
    return this.defaultTextColor;
  },

  get getDefaultLightTextColor() {
    return this.defaultLightTextColor;
  },

  get getDefaultThemeTextColor() {
    return this.defaultThemeTextColor;
  },

  get getThemeColor() {
    return this.themeColor;
  },

  get getHeaderColor() {
    return this.headerColor;
  },

  get getDangerColor() {
    return this.dangerColor;
  }
};
