import { BaseService } from "./BaseService";

export default class BusinessPlaceService extends BaseService {

  async list(request) {
    return await this.get(this.servicesUrl + 'BusinessPlaces' + request.conditionsUrl);
  };
}
