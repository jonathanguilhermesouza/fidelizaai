import NetInfo from "@react-native-community/netinfo";
import {
    BaseService
} from "./BaseService";

export default class NetworkService extends BaseService {
    static isConnected() {
        return NetInfo.fetch().then((state) => {
            return state.isConnected;
        })
    };

    async isAuthenticated() {
        var responseAuthenticated = {
            isAuthenticated: false,
            message: "",
            isError: false,
            dateTime: new Date()
        }
        return await this.get(this.servicesUrl + '/account/IsAuthenticated')
            .then((response) => {
                responseAuthenticated.isAuthenticated = response;
            })
            .catch((error) => {
                responseAuthenticated.isError = true;
            })
            .finally((response) => {
                return responseAuthenticated;
            })
    };
}