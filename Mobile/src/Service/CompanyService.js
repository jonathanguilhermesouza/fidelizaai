import { BaseService } from "./BaseService";

const entity = 'CompanyApi';
export default class CustomerService extends BaseService {

  async list(request) {
    return await this.get(this.servicesUrl + entity + '/GetListCompany');
  };

  async getById(request) {
    return await this.get(this.servicesUrl + entity + '/GetDetailCompanyById/' + request.id);
  };

  async create(customer) {
    return await this.post(this.servicesUrl + entity, customer)
  };

  async edit(cardCode, customer) {
    return await this.put(this.servicesUrl + entity + `('${cardCode}')`, customer)
  };

  async remove(request) {
    return await this.delete(this.servicesUrl + entity + request.conditionsUrl);
  };
}
