//service contendo tabelas padroes do sistema, geralmente para compor combos
import { BaseService } from "./BaseService";

export default class SisService extends BaseService {

  async list(request) {
    return await this.get(this.servicesUrl + request.entity + request.conditionsUrl);
  };

  async getSeriesService_GetDefaultSeries(request) {
    return await this.post(this.servicesUrl + request.entity, request.object);
  };
}
