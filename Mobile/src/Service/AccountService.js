import { AsyncStorage } from "react-native";
import {
  BaseService
} from "./BaseService";

export default class AccountService extends BaseService {

  async forgotPassword(forgotRequest) {
    var forgotResponse = {};
    return await this.post(this.servicesUrl + 'account/ForgotPassword', forgotRequest)
      .then((response) => {
        forgotResponse = response;
      })
      .catch((error) => {
        forgotResponse = error;
      })
      .finally((response) => {
        return forgotResponse;
      })
  };

  async signInWithExternalLogin(loginRequest) {
    return await this.post(this.servicesUrl + 'account/LoginWithExternalProvider', loginRequest)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      })
  };

  async register(registerRequest) {
    var loginResponse = {
      isAuthenticated: false,
      message: "",
      dateTime: new Date(),
      token: ""
    }
    return await this.post(this.servicesUrl + 'account/Register', registerRequest)
      .then((response) => {
        loginResponse.isAuthenticated = response.is_loged;
        loginResponse.token = response.access_token;
        //alert(JSON.stringify(loginResponse));
      })
      .catch((error) => {
        loginResponse.isAuthenticated = false;
        loginResponse.Message = error.message;
      })
      .finally((response) => {
        return loginResponse;
      })
  };

  async login(loginRequest) {
    var loginResponse = {
      isAuthenticated: false,
      message: "",
      dateTime: new Date(),
      token: ""
    }
    return await this.post(this.servicesUrl + 'account/Login', loginRequest)
      .then((response) => {
        loginResponse.isAuthenticated = response.is_loged;
        loginResponse.token = response.access_token;
      })
      .catch((error) => {
        loginResponse.isAuthenticated = false;
        loginResponse.Message = error.message;
      })
      .finally((response) => {
        return loginResponse;
      })
  };

  async logout() {
    return await this.post(this.servicesUrl + 'Logout', null)
      .then((response) => {

        AsyncStorage.getItem('BASIC_DATA_USER_LOGGED', (err, result) => {
          if (result !== null) {
            let basicDataUserLogged = JSON.parse(result);
            basicDataUserLogged.sessionId = "";
            AsyncStorage.setItem('BASIC_DATA_USER_LOGGED', JSON.stringify(basicDataUserLogged));
          }
        });

      })
      .catch((error) => {
        //alert('erro deslogando');
      })
      .finally((response) => {
        return null;
      })
  };
}
