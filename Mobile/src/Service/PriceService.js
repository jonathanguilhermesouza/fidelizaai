import { BaseService } from "./BaseService";

const entity = 'PriceLists';
export default class PriceService extends BaseService {

  async list(request) {
    return await this.get(this.servicesUrl + entity + request.conditionsUrl);
  };
}
