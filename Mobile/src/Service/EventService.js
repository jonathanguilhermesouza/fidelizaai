import {
  BaseService
} from "./BaseService";

export  default class EventService extends BaseService {  
  
  api_url = this.servicesUrl + "Event/Event/"

  async list(listRequest) {
    return await this.post(this.api_url + 'List/', listRequest)
  };
  async retrieve(id) {
    return await this.post(this.api_url + 'Retrieve/', id)
  };
}