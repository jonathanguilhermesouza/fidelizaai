import { BaseService } from "./BaseService";

const entity = 'SalesPersons';
export default class SalesPersonsService extends BaseService {

  async list(request) {
    return await this.get(this.servicesUrl + entity + request.conditionsUrl);
  };
}
