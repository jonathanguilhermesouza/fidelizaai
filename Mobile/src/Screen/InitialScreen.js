import React from 'react'
import {
    AsyncStorage
} from "react-native";
import NetworkService from "@Service/NetworkService";
import NavigationService from '@Service/Navigation'

let NetworkServiceInstance = new NetworkService();
export default class InitialScreen extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {

        let userAuthenticated = false;

        NetworkServiceInstance.isAuthenticated()
            .then((response) => {
                userAuthenticated = response.isAuthenticated;
            }).catch((error) => {
                userAuthenticated = false;
            }).finally((response) => {
                if (userAuthenticated)
                    NavigationService.navigate("TabsMerchant");
                else
                    NavigationService.navigate("Login");
            });

        //this.props.navigation.navigate('Login');
    }
    render() {
        return null;
    }
}