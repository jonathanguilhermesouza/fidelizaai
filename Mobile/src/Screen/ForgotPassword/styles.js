import CommonColor from '@Theme/Variables/CommonColor';
const React = require("react-native");
const { Dimensions, Platform } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  forgotPasswordContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    marginTop:
      deviceWidth < 330
        ? Platform.OS === "android"
          ? deviceHeight / 9 - 20
          : deviceHeight / 8 - 10
        : Platform.OS === "android"
          ? deviceHeight / 7 - 20
          : deviceHeight / 6 - 30
  },
  forgotPasswordHeader: {
    textAlign: "center",
    alignSelf: "center",
    fontSize: 22,
    padding: 10,
    fontWeight: "bold",
    color: "#FFF",
    marginTop:
      Platform.OS === "android" ? deviceHeight / 6 : deviceHeight / 6 + 10
  },
  background: {
    backgroundColor: CommonColor.themeColor,
  },
  formErrorText1: {
    fontSize: Platform.OS === "android" ? 12 : 15,
    color: "red",
    textAlign: "right",
    top: -10
  },
  formErrorText2: {
    fontSize: Platform.OS === "android" ? 12 : 15,
    color: "transparent",
    textAlign: "right",
    top: -10
  },
  recoverPassword: {
    height: 50,
    marginTop: 10,
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: "#fff",
    borderWidth: 0
  },
  helpBtns: {
    opacity: 0.9,
    fontSize: 14,
    fontWeight: "bold",
    color: "#00aff0"
  },
  buttonGetPassord: {
    height: 50,
    width: deviceWidth,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignSelf: "center",
  },
  buttonGoOut: {
    flex: 1,
    height: 60,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    backgroundColor: '#EA3788',
    alignSelf: "flex-end",
    width: deviceWidth
  },
  viewContainer: {
    flex: 9,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonFooter: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }
};
