//Libs
import React, { Component } from "react";
import { StatusBar, TouchableOpacity } from "react-native";
import {
  Container,
  Text,
  Button,
  View
} from "native-base";
import { reduxForm } from "redux-form";
import { Field } from 'react-native-validate-form';

//Styles
import styles from "./styles";
import CommonColor from '@Theme/Variables/CommonColor';

//Services
import AccountService from '@Service/AccountService';

//Components
import InputField from '@Component/Input/InputField';

//Consts
const lightColor = CommonColor.getDefaultLightTextColor;
const required = value => (value ? undefined : "Obrigatório");
const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "Endereço de email inválido"
    : undefined;

var AccountServiceInstance = new AccountService();
class ForgotPasswordForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      errors: []
    };
  }

  forgotPassword() {
      let forgorRequest = {
        email: this.state.email
      }
      AccountServiceInstance.forgotPassword(forgorRequest)
        .then((data) => {
          this.props.navigation.goBack();
        })
        .catch((error) => {
          console.log(error);
        }).finally(() => {
      
        });
  }

  render() {
    return (
      <Container style={styles.background}>
        <StatusBar barStyle="light-content" />
        <View style={{ flex: 1 }}>
          <Text style={styles.forgotPasswordHeader}>
            Recuperação de senha
          </Text>
          <View style={styles.forgotPasswordContainer}>
            <View>
              <Field
                      autoCapitalize="none"
                      textColor={lightColor}
                      tintColor={lightColor}
                      baseColor={lightColor}
                      label="INFORME SEU EMAIL"
                      secureTextEntry={false}
                      required
                      component={InputField}
                      validations={[required, email]}
                      name="email"
                      value={this.state.email}
                      onChangeText={(val) => this.setState({ email: val })}
                      customStyle={{ width: 100 }}
                      errors={this.state.errors}/>
            </View>
            <Button block onPress={() => this.forgotPassword()} style={styles.recoverPassword}>
              <Text style={{ color: "#00aff0" }}>Recuperar</Text>
            </Button>
          </View>
        </View>
        <View style={styles.buttonFooter}>
          <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={[styles.buttonGoOut]}>
            <Text style={{ fontSize: 20, color: CommonColor.getDefaultLightTextColor }}>Voltar para login</Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  }
}

const ForgotPassword = reduxForm({
  form: "forgotPassword"
})(ForgotPasswordForm);
export default ForgotPassword;
