//Components
import React from "react";
import { Text, View, Content } from "native-base";
import { FlatList, TouchableOpacity, Modal, ScrollView } from 'react-native';
import TaxInformationCreate from './TaxInformationCreate';

//Services
import NavigationService from '@Service/Navigation';

//Styles
import styles from "./styles";

export default class extends React.Component {
    listOfAddress = [];

    constructor(props) {
        super(props);
        this.state = {
            /*listTaxInformation: [
                {
                    Address: "endereco 1",
                    CNAECode: "1112",
                    TaxId0: "00223.098.0001/63",
                    TaxId1: "1234",
                    TaxId2: "5678",
                    TaxId3: "90123",
                    TaxId4: "901.122.323-25",
                    TaxId5: "",
                    TaxId6: "",
                    TaxId7: "",
                    AddrType: "bo_BillTo"
                }
            ],*/
            listTaxInformation: [],
            isModalVisible: false,
            taxInformationEdit: {},
            isUpdate: false,
            indexUpdate: 0
        }
    }

    goToState(state) {
        this.props.hideViewStep();
        setTimeout(() => {
            NavigationService.navigate(state)
        }, 10)
    }

    validForm() {
        return new Promise((resolve, reject) => {
            resolve(true);
        })
    }

    closeModal(modalState) {
        if (modalState.isSave)
            this.serializeTaxInformationItem(modalState);
        this.setState({
            isModalVisible: modalState.isModalVisible,
        });
    }

    serializeTaxInformationItem(taxInformationItem) {

        let newTaxInformationItem = {
            //CardCode: taxInformationItem.CardCode,
            Address: taxInformationItem.Address,
            TaxId0: taxInformationItem.TaxId0,
            TaxId1: taxInformationItem.TaxId1,
            TaxId2: taxInformationItem.TaxId2,
            TaxId3: taxInformationItem.TaxId3,
            TaxId4: taxInformationItem.TaxId4,
            TaxId5: taxInformationItem.TaxId5,
            TaxId6: taxInformationItem.TaxId6,
            TaxId7: taxInformationItem.TaxId7,
            TaxId8: taxInformationItem.TaxId8,
            CNAECode: taxInformationItem.CNAECode,
            AddrType: taxInformationItem.AddrType,
            BPCode: taxInformationItem.BPCode
        }

        if (this.state.isUpdate) {
            this.state.listTaxInformation.splice(this.state.indexUpdate, 1);
            this.setState({ listTaxInformation: this.state.listTaxInformation });
        }

        let tempList = [];
        tempList.push(newTaxInformationItem);
        tempList = [...this.state.listTaxInformation, ...tempList];
        this.setState({ listTaxInformation: tempList });

        this.props.setTaxInformationList(tempList);
    }

    editTaxInformation(taxInformation, index) {
        this.setState({ taxInformationEdit: taxInformation, isUpdate: true, indexUpdate: index });
        setTimeout(() => {
            this.setState({ isModalVisible: true });
        }, 10)
    }

    openModal() {
        this.setState({ isModalVisible: true });
    }

    renderRow(item, index) {
        return (
            <TouchableOpacity onPress={() => this.editTaxInformation(item, index)}>
                <View style={styles.listItemContainer}>
                    <View style={styles.detailViewContainer}>
                        <View style={styles.detailsViewContainerWrap}>
                            <View style={[styles.nameViewContainer, { flex: 3 }]}>
                                <Text>{item.Address}</Text>
                                <View style={styles.descriptionViewItem}>
                                    <Text style={styles.text}>{item.TaxId0}</Text>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <Content style={styles.page}>
                <ScrollView>
                    <Modal visible={this.state.isModalVisible} presentationStyle={"fullScreen"}>
                        <TaxInformationCreate closeModal={this.closeModal.bind(this)} taxInformation={this.state.taxInformationEdit} isUpdate={this.state.isUpdate} listOfAddress={this.props.listOfAddress} />
                    </Modal>
                    <View style={styles.viewContainer}>
                        <FlatList
                            keyboardShouldPersistTaps={'handled'}
                            data={this.state.listTaxInformation}
                            //extraData={this.state.orderBy}
                            renderItem={({ item, index }) => this.renderRow(item, index)}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </ScrollView>
            </Content>
        )
    }
}