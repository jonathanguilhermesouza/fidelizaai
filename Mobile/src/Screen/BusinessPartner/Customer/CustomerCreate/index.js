//Libs
import React from "react";
import { Container, Text, View, Header, Button, Icon, Left, Right, Body } from 'native-base';
import { StatusBar } from 'react-native';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import { ProgressSteps, ProgressStep } from 'react-native-progress-steps';

//Components
import AddressList from '../CustomerCreate/AddressList';
import TaxInformationList from '../CustomerCreate/TaxInformationList';
import CustomerBasicInformation from "../CustomerCreate/CustomerBasicInformation";
import ContactList from '../CustomerCreate/ContactList';
import NavigationService from '@Service/Navigation';

//Styles
import GlobalStyle from '@Theme/GlobalStyle';
import styles from "./styles";

//Services
import CustomerService from "@Service/CustomerService";
import SisService from '@Service/SisService';

var SisInstance = new SisService();
var CustomerServiceInstance = new CustomerService();
export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            errors: false,
            currentPage: 1,
            isResponseError: false,
            responseError: {},
            isValidForm: false,
            isStepVisible: true,
            isBasicInformationFormValid: false,
            isAddressFormValid: false,
            isTaxInformationFormValid: false,
            isContactFormValid: false,
            isRegisterCompleted: false,
            customer: {},
            listOfAddress: [],
            listOfTaxInformation: [],
            listOfContact: [],
            basicInformation: {}

        }
        this.basicInformationElement = React.createRef();
        this.addressElement = React.createRef();
        this.taxInformationElement = React.createRef();
        this.contactElement = React.createRef();
    }

    componentWillReceiveProps(nextProps, nextState) {
        if (nextState.currentPage != this.state.currentPage) {
            if (this.viewPager) {
                this.viewPager.setPage(nextState.currentPage)
            }
        }
    }

    componentDidMount() {
        this.loadBasicInformation();
        this.getSeries();
    }

    loadBasicInformation() {
        return new Promise((resolve, reject) => {
            var request = {
                conditionsUrl: `('${this.props.navigation.state.params.CardCode}')`
            }
            CustomerServiceInstance.getById(request)
                .then((data) => {
                    console.log(data);
                    this.setAddressList(data.BPAddresses);
                    this.setTaxInformationList(data.BPFiscalTaxIDCollection);
                    this.setContactList(data.ContactEmployees);
                    this.basicInformationElement.current.setState({ ...data },
                      () => {
                        this.basicInformationElement.current.loadListGroupCode();
                        this.basicInformationElement.current.loadListPrice();
                        this.basicInformationElement.current.loadListSalesPerson();
                      }  );
                    resolve(data);
                })
                .catch((error) => {
                    reject(error);
                    console.log(error);
                });

        })
    }

    onNextStep = () => {
        if(this.state.currentPage == 1){
            this.basicInformationElement.current.submitForm()
                .then((resBasicInformationForm) => {
                    this.setState({
                        isBasicInformationFormValid: resBasicInformationForm
                    });
                    if(resBasicInformationForm)
                        this.setState({currentPage: this.state.currentPage + 1}, ()=> this.loadViewData());
                });
            }
            else if(this.state.currentPage == 2)
            {
                this.addressElement.current.validForm()
                .then((responseForm) => {
                    this.setState({
                        isAddressFormValid: responseForm
                    });
                    if(responseForm)
                        this.setState({currentPage: this.state.currentPage + 1}, ()=> this.loadViewData());
                });
            }
            else if(this.state.currentPage == 3)
            {
                this.taxInformationElement.current.validForm()
                .then((responseForm) => {
                    this.setState({
                        isTaxInformationFormValid: responseForm
                    });
                    if(responseForm)
                        this.setState({currentPage: this.state.currentPage + 1}, ()=> this.loadViewData());
                });
            } 
            else if(this.state.currentPage == 4)
            {
                this.contactElement.current.validForm()
                .then((responseForm) => {
                    this.setState({
                        isContactFormValid: responseForm
                    });
                    if(responseForm)
                        this.setState({currentPage: this.state.currentPage + 1}, ()=> this.loadViewData());
                });
            }
    }

    onPrevStep = () => {
        this.setState({currentPage: this.state.currentPage - 1}, ()=> this.loadViewData());
    }

    onSubmit = () => {
            if (!this.state.isAddressFormValid | !this.state.isBasicInformationFormValid | !this.state.isContactFormValid | !this.state.isTaxInformationFormValid)
                alert('Verifique as inconsistências e tente novamente.');
            else
                this.serializeCustomer();
    }

    removeAttributesJson(key, value) {
        if (key == "GroupCodeText") return undefined;
        else if (key == "SlpCodeText") return undefined;
        else if (key == "PriceListNumText") return undefined;
        else if (key == "isValidForm") return undefined;
        else if (key == "errors") return undefined;
        else if (key == "isValid") return undefined;
        else return value;
    }

    serializeCustomer() {

        let customer = {
            ...this.state.customer,
            Series: this.state.Series,
            BPAddresses: this.state.listOfAddress,
            ContactEmployees: this.state.listOfContact,
            BPFiscalTaxIDCollection: this.state.listOfTaxInformation
        };
        let newCustomerSerialized = JSON.parse(JSON.stringify(customer, this.removeAttributesJson));
        if (this.props.navigation.state.params && this.props.navigation.state.params.CardCode) {
            CustomerServiceInstance.edit(this.props.navigation.state.params.CardCode, newCustomerSerialized)
                .then((response) => {
                    this.setState({
                        isResponseError: false,
                        responseError: {}
                    });
                    NavigationService.navigate("Customer");
                    console.log(response);
                })
                .catch((error) => {
                    let messageResponse = JSON.parse(error.request._response);
                    this.setState({
                        isResponseError: true,
                        responseError: messageResponse.error.message.value
                    });
                    alert("Não foi possível salvar o cliente, verifique as inconsistêcias.");
                    console.log(error);
                });
        } else {
            CustomerServiceInstance.create(newCustomerSerialized)
                .then((response) => {
                    this.setState({
                        isResponseError: false,
                        responseError: {}
                    });
                    NavigationService.navigate("Customer");
                    console.log(data);
                })
                .catch((error) => {
                    let messageResponse = JSON.parse(error.request._response);
                    this.setState({
                        isResponseError: true,
                        responseError: messageResponse.error.message.value
                    });
                    alert("Não foi possível salvar o cliente, verifique as inconsistêcias.");
                    console.log(error);
                });
        }
    }

    loadViewData(){
            setTimeout(() => {
                    if(this.state.currentPage == 1) {
                        this.basicInformationElement.current.setState({ ...this.state.customer },
                        () => {
                          this.basicInformationElement.current.loadListGroupCode();
                          this.basicInformationElement.current.loadListPrice();
                          this.basicInformationElement.current.loadListSalesPerson();
                        });
                    }
                    else if(this.state.currentPage == 2) {
                        if(this.state.listOfAddress && this.state.listOfAddress.length == 0 && this.state.customer.BPAddresses && this.state.customer.BPAddresses.length > 0)
                            this.addressElement.current.setState({ listAddress: this.state.customer.BPAddresses });
                        else
                            this.addressElement.current.setState({ listAddress: this.state.listOfAddress });
                    } 
                    else if(this.state.currentPage == 3){
                        if(this.state.listOfTaxInformation && this.state.listOfTaxInformation.length == 0 && this.state.customer.BPFiscalTaxIDCollection && this.state.customer.BPFiscalTaxIDCollection.length > 0)
                           this.taxInformationElement.current.setState({ listTaxInformation: this.state.customer.BPFiscalTaxIDCollection });
                        else
                            this.taxInformationElement.current.setState({ listTaxInformation: this.state.listOfTaxInformation });
                    } 
                    else if(this.state.currentPage == 4){
                        if(this.state.listOfContact && this.state.listOfContact.length == 0 && this.state.customer.ContactEmployees && this.state.customer.ContactEmployees.length > 0)
                            this.contactElement.current.setState({ listContact: this.state.customer.ContactEmployees });
                        else
                            this.contactElement.current.setState({ listContact: this.state.listOfContact });
                    }
            }, 0)
    }

    getSeries() {
        var request = {
            entity: "SeriesService_GetDefaultSeries",
            object: {
                DocumentTypeParams: {
                    Document: "2",
                    DocumentSubType: "C"
                }
            }
        }
        SisInstance.getSeriesService_GetDefaultSeries(request)
            .then((data) => {
                this.setState({
                    Series: data.Series
                });
                console.log(data);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    submitSuccess() {
        this.setState({ isValidForm: true });
        console.log("Submit Success!");
    }

    submitFailed() {
        this.setState({ isValidForm: false });
        console.log("Submit Faield!");
    }

    goBak(navigate, state) {
        this.setState({ isStepVisible: false }, () => { navigate('Customer', { go_back_key: state.key }) });
    }

    hideViewStep() {
        this.setState({ isStepVisible: false });
    }

    handleClick(modal) {
        if (modal == 'CREATE_ADDRESS')
            this.addressElement.current.openModal();
        else if (modal == 'CREATE_TAX_INFORMATION')
            this.taxInformationElement.current.openModal();
        else if (modal == 'CREATE_CONTACT')
            this.contactElement.current.openModal();
    }

    setAddressList(list){
        this.setState({listOfAddress: list});
    }

    setTaxInformationList(list){
        this.setState({listOfTaxInformation: list});
    }

    setContactList(list){
        this.setState({listOfContact: list});
    }

    setBasicInformation(obj){
        this.setState({customer: obj});
    }

    render() {
        const { state, navigate } = this.props.navigation;
        return (
            <Container>
                <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs}>
                    <StatusBar barStyle="light-content" backgroundColor="#999" />
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.goBak(navigate, state)}>
                            <Icon active name='arrow-left' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 3 }}>
                        <Text style={[GlobalStyle.actionBarText, { alignSelf: 'center' }]}>{this.props.navigation.state.params && this.props.navigation.state.params.CardCode ? "ALTERAR CLIENTE" : "NOVO CLIENTE"}</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        {this.state.currentPage == 2 ?
                            <Button transparent onPress={() => this.handleClick('CREATE_ADDRESS')}>
                                <IconFontAwesome name={"plus"} color={"#fff"} size={25} />
                            </Button>
                            : this.state.currentPage == 3 ?
                                <Button transparent onPress={() => this.handleClick('CREATE_TAX_INFORMATION')}>
                                    <IconFontAwesome name={"plus"} color={"#fff"} size={25} />
                                </Button>
                                : this.state.currentPage == 4 ?
                                    <Button transparent onPress={() => this.handleClick('CREATE_CONTACT')}>
                                        <IconFontAwesome name={"plus"} color={"#fff"} size={25} />
                                    </Button> : null
                        }
                    </Right>
                </Header>
                <View style={styles.viewContainer}>
                    {this.state.isStepVisible ?
                        <View style={{ flex: 1 }}>
                           <ProgressSteps activeStepIconBorderColor={"#25D366"} completedProgressBarColor={"#25D366"} completedStepIconColor={"#25D366"}>
                            <ProgressStep errors={!this.state.isBasicInformationFormValid}  previousBtnText="Voltar" nextBtnText="Avançar" onNext={this.onNextStep} onPrevious={this.onPrevStep}>
                                <Text style={{alignSelf:"center"}}>Informações básicas</Text>
                                <CustomerBasicInformation ref={this.basicInformationElement} setBasicInformation={this.setBasicInformation.bind(this)} loadBasicInformation={this.loadBasicInformation.bind(this)} cardCode={this.props.navigation.state.params ? this.props.navigation.state.params.CardCode : null} />
                            </ProgressStep>
                            <ProgressStep errors={!this.state.isAddressFormValid} previousBtnText="Voltar" nextBtnText="Avançar" onNext={this.onNextStep} onPrevious={this.onPrevStep}>
                                <Text style={{alignSelf:"center"}}>Endereço</Text>
                                <AddressList ref={this.addressElement} setAddressList={this.setAddressList.bind(this)} hideViewStep={this.hideViewStep.bind(this)} cardCode={this.props.navigation.state.params ? this.props.navigation.state.params.CardCode : null} />
                            </ProgressStep>
                            <ProgressStep errors={!this.state.isTaxInformationFormValid} previousBtnText="Voltar" nextBtnText="Avançar" onNext={this.onNextStep} onPrevious={this.onPrevStep}>
                                <Text style={{alignSelf:"center"}}>Informações fiscais</Text>
                                <TaxInformationList ref={this.taxInformationElement} setTaxInformationList={this.setTaxInformationList.bind(this)} hideViewStep={this.hideViewStep.bind(this)} listOfAddress={this.state.listOfAddress} />
                            </ProgressStep>
                            <ProgressStep errors={!this.state.isContactFormValid} previousBtnText="Voltar" nextBtnText="Avançar" onNext={this.onNextStep} onPrevious={this.onPrevStep}>
                                <Text style={{alignSelf:"center"}}>Contatos</Text>
                                <ContactList ref={this.contactElement} setContactList={this.setContactList.bind(this)} hideViewStep={this.hideViewStep.bind(this)} />
                            </ProgressStep>
                            <ProgressStep previousBtnText="Voltar" finishBtnText="Salvar" onSubmit={this.onSubmit} onPrevious={this.onPrevStep}>
                                <Text style={{alignSelf:"center", marginBottom: 50}}>Validação</Text>
                                <View style={styles.page}>
                                                {
                                                    this.state.isAddressFormValid && this.state.isBasicInformationFormValid && this.state.isContactFormValid && this.state.isTaxInformationFormValid && !this.state.isResponseError ?
                                                        <Text>Os dados estão completos, clique em SALVAR </Text> : null
                                                }
                                                {this.state.isResponseError ?
                                                    <View>
                                                        <Text style={{ color: 'red', marginBottom: 20 }}>Corrija a inconsistência e tente novamente</Text>
                                                        <Text>{this.state.responseError}</Text>
                                                    </View> : null
                                                }
                                            </View>
                            </ProgressStep>
                        </ProgressSteps>
                        </View>
                    : null}
                </View>
            </Container>
        )
    }
}