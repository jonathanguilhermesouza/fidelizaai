import CommonColor from '@Theme/Variables/CommonColor';

const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  viewContainer: {
    flex: 1,
    marginTop: 40,
    backgroundColor: "#fff"
  },
  page: {
    flex: 1,
    paddingLeft: 20,
    paddingRight: 20
  },
  listItemContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingBottom: 20
  },
  detailViewContainer: {
    flex: 4,
    justifyContent: "center",
    borderBottomColor: "rgba(92,94,94,0.5)",
    borderBottomWidth: 0.25,
    paddingBottom: 5
  },
  detailsViewContainerWrap: {
    flex: 1,
    alignItems: "center",
    flexDirection: "row"
  },
  nameViewContainer: {
    alignItems: "flex-start",
    flex: 1
  },
  descriptionViewItem: {
    marginTop: 5,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  text: {
    fontWeight: '400',
    color: '#666',
    fontSize: 12
  },
  buttonRemove: {
    width: 45,
    height: 45,
    padding: 10,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#ff4c4c"
  },
  iconRemove: {
    color: "#fff",
    fontSize: 27,
    backgroundColor: "transparent"
  },
  viewButtonRemove: {
    flex: 1,
    alignItems: "flex-end"
  }
}