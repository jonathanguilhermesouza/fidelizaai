//Packages
import React from "react";
import { Container, Text, View } from "native-base";
import { Header, Button, Icon, Left, Right, Body } from 'native-base';
import { StatusBar, ScrollView, Modal, TouchableOpacity, DatePickerIOS } from 'react-native';
import { Form, Field } from 'react-native-validate-form';
import DatePicker from 'react-native-datepicker';
import DateTimePicker from "react-native-modal-datetime-picker";
import Moment from 'moment';

//Components
import InputField from '@Component/Input/InputField';
import InputDropdown from '@Component/Input/InputDropdown';

//Services
import NavigationService from '@Service/Navigation';

//Styles
import styles from "./styles";
import GlobalStyle from '@Theme/GlobalStyle';

const required = value => (value ? undefined : "Obrigatório");
const minLength = min => value => value && value.length < min ? `Deve ter no mímino ${min} caracteres` : undefined;
const maxLength = max => value => value && value.length > max ? `Deve ter no máximo ${max} caracteres` : undefined;
const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? "Endereço de email inválido" : undefined;
const genderList = [{ value: 'Masculino', id: 'M' }, { value: 'Feminino', id: 'F' }, { value: 'Indefinido', id: 'E' }];
const baseColor = "#333";

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.setDate = this.setDate.bind(this);
        this.state = {

            isSave: false,
            isUpdate: false,
            isModalVisible: true,
            errors: [],
            isValidForm: false,
            isDateTimePickerVisible: false,

            Name: '',
            Position: '',
            Address: '',
            Phone1: '',
            Phone2: '',
            MobilePhone: '',
            E_Mail: '',
            Remarks1: '',
            Remarks2: '',
            DateOfBirth: new Date(),
            Gender: '',
            Profession: '',
            Title: '',
            FirstName: '',
            MiddleName: '',
            LastName: '',
            Active: "tYES",
            InternalCode: ''
        }
    }

    componentWillMount() {
        this.loadContact();
    }

    loadContact() {
        if (this.props.isUpdate) {
            let contactItem = this.props.contact;

            let typeToCreate = '';
            if (contactItem.Gender == 'M' || contactItem.Gender == 'gt_Male')
                typeToCreate = 'Masculino';
            else if (contactItem.Gender == 'F' || contactItem.Gender == 'gt_Female')
                typeToCreate = 'Feminino';
            else
                typeToCreate = 'Indefinido';

            this.setState({
                ...contactItem,
                Gender: typeToCreate
            });
        }
    }

    submitSuccess() {
        this.setState({ isValidForm: true });
        console.log("Submit Success!");
    }

    submitFailed() {
        this.setState({ isValidForm: false });
        console.log("Submit Faield!");
    }

    save() {
        this.setState({ isSave: true });
        this.submitForm();
    }

    submitForm() {
        let submitResults = this.contactForm.validate();
        let errors = [];

        submitResults.forEach(item => {
            errors.push({ field: item.fieldName, error: item.error });
        });
        this.setState({ errors: errors });

        let isValidForm = true;
        submitResults.map((item) => {
            if (item.isValid == false)
                isValidForm = false;
        });

        if (isValidForm) {
            this.onChangeGender(this.state.Gender);
            this.closeModal();
        }
    }

    closeModal() {
        this.setState({
            isModalVisible: false
        }, () => { this.props.closeModal(this.state) });
    }

    showDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: true });
    };

    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    };

    handleDatePicked = date => {
        this.setState({ DateOfBirth: date });
        this.hideDateTimePicker();
    };

    onChangeGender(genderSelected) {
        let id = '';
        if (genderSelected == "Masculino" || genderSelected == "M")
            id = 'M';
        else if (genderSelected == "Feminino" || genderSelected == "F")
            id = 'F';
        else
            id = 'E';

        this.setState({ Gender: id });
    }

    setDate(newDate) {
        this.setState({ DateOfBirth: newDate });
    }

    render() {
        return (
            <Container style={{ flex: 1 }}>
                <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs}>
                    <StatusBar barStyle="light-content" />
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.closeModal()}>
                            <Icon active name='arrow-left' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 3 }}>
                        <Text style={[GlobalStyle.actionBarText, { alignSelf: 'center' }]}>{'novo contato'.toUpperCase()}</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.save()}>
                            <Icon active name='content-save' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Right>
                </Header>
                <View style={{ flex: 1 }}>
                    <ScrollView contentContainerStyle={[styles.page]}>
                        <Form
                            ref={(ref) => this.contactForm = ref}
                            validate={true}
                            submit={this.submitSuccess.bind(this)}
                            failed={this.submitFailed.bind(this)}
                            errors={this.state.errors}>

                            <View style={{ marginTop: 50 }}>
                                <Field
                                    autoCapitalize="none"
                                    textColor={baseColor}
                                    tintColor={baseColor}
                                    baseColor={baseColor}
                                    label="Nome do Contato"
                                    secureTextEntry={false}
                                    component={InputField}
                                    validations={[maxLength(50)]}
                                    name="Name"
                                    value={this.state.Name}
                                    onChangeText={(val) => this.setState({ Name: val })}
                                    customStyle={{ width: 100 }}
                                    errors={this.state.errors}
                                />
                            </View>

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Posição"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(90)]}
                                name="Position"
                                value={this.state.TaxId0}
                                onChangeText={(val) => this.setState({ TaxId0: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Endereço do Contato"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(100)]}
                                name="Address"
                                value={this.state.Address}
                                onChangeText={(val) => this.setState({ Address: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Telefone 1 de Contato"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(20)]}
                                name="Phone1"
                                value={this.state.Phone1}
                                onChangeText={(val) => this.setState({ Phone1: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Telefone 2 de Contato"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(20)]}
                                name="Phone2"
                                value={this.state.Phone2}
                                onChangeText={(val) => this.setState({ Phone2: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Celular"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(50)]}
                                name="MobilePhone"
                                value={this.state.MobilePhone}
                                onChangeText={(val) => this.setState({ MobilePhone: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Email do Contato"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(100)]}
                                name="E_Mail"
                                value={this.state.E_Mail}
                                onChangeText={(val) => this.setState({ E_Mail: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Nota 1"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(100)]}
                                name="Remarks1"
                                value={this.state.Remarks1}
                                onChangeText={(val) => this.setState({ Remarks1: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Nota 2"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(100)]}
                                name="Remarks2"
                                value={this.state.Remarks2}
                                onChangeText={(val) => this.setState({ Remarks2: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <DateTimePicker
                                isVisible={this.state.isDateTimePickerVisible}
                                onConfirm={this.handleDatePicked}
                                onCancel={this.hideDateTimePicker}
                                cancelTextIOS={"Cancelar"}
                                confirmTextIOS={"Confirmar"}
                                titleIOS={"Escolha uma data"}
                                locale='pt'
                                date={this.state.DateOfBirth}
                            />


                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 8 }}  >
                                    <TouchableOpacity>
                                        <Field
                                            onTouchStart={() => this.showDateTimePicker()}
                                            autoCapitalize="none"
                                            textColor={baseColor}
                                            tintColor={baseColor}
                                            baseColor={baseColor}
                                            label="Data de Nascimento"
                                            secureTextEntry={false}
                                            component={InputField}
                                            name="DateOfBirth"
                                            value={Moment(this.state.DateOfBirth).format('DD/MM/YYYY')}
                                            onChangeText={(val) => this.setState({ DateOfBirth: val })}
                                            customStyle={{ width: 100 }}
                                            errors={this.state.errors}
                                        />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ flex: 2, flexDirection: 'row', justifyContent: 'flex-end', alignSelf: 'center' }}>
                                    <Button transparent onPress={this.showDateTimePicker} style={{ backgroundColor: '#00D774' }}>
                                        <Icon active name='calendar-range' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                                    </Button>
                                </View>
                            </View>

                            <Field
                                baseColor={baseColor}
                                textColor={baseColor}
                                selectedItemColor={baseColor}
                                containerStyle={{ color: "#333" }}
                                label="Gênero"
                                component={InputDropdown}
                                name="Gender"
                                value={this.state.Gender}
                                onChangeText={(val) => this.onChangeGender(val)}
                                errors={this.state.errors}
                                data={genderList}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Profissão"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(50)]}
                                name="Profession"
                                value={this.state.Profession}
                                onChangeText={(val) => this.setState({ Profession: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Título (Sr. Sra.)"
                                secureTextEntry={false}
                                component={InputField}
                                validations={[maxLength(10)]}
                                name="Title"
                                value={this.state.Title}
                                onChangeText={(val) => this.setState({ Title: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Primeiro Nome"
                                secureTextEntry={false}
                                required
                                component={InputField}
                                validations={[required]}
                                name="FirstName"
                                value={this.state.FirstName}
                                onChangeText={(val) => this.setState({ FirstName: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Nome do Meio"
                                secureTextEntry={false}
                                required
                                component={InputField}
                                validations={[required]}
                                name="MiddleName"
                                value={this.state.MiddleName}
                                onChangeText={(val) => this.setState({ MiddleName: val })}
                                customStyle={{ width: 100 }}
                                errors={this.state.errors}
                            />

                            <View style={{ marginBottom: 100 }}>
                                <Field
                                    autoCapitalize="none"
                                    textColor={baseColor}
                                    tintColor={baseColor}
                                    baseColor={baseColor}
                                    label="Último Nome"
                                    secureTextEntry={false}
                                    required
                                    component={InputField}
                                    validations={[required]}
                                    name="LastName"
                                    value={this.state.LastName}
                                    onChangeText={(val) => this.setState({ LastName: val })}
                                    customStyle={{ width: 100 }}
                                    errors={this.state.errors}
                                />
                            </View>
                        </Form>
                    </ScrollView>
                </View>
            </Container>
        )
    }
}