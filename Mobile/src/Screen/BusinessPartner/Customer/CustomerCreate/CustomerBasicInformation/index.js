//Libs
import React from "react";
import { View, Content } from "native-base";
import { Form, Field } from 'react-native-validate-form';

//Components
import InputField from '@Component/Input/InputField';
import InputDropdown from '@Component/Input/InputDropdown';

//Services
import GroupCodeService from "@Service/GroupCodeService";
import PriceService from "@Service/PriceService";
import SalesPersonService from "@Service/SalesPersonService";

//Styles
import styles from "./styles";

const required = value => (value ? undefined : "Obrigatório");
const minLength = min => value => value && value.length < min ? `Deve ter no mímino ${min} caracteres` : undefined;
const maxLength = max => value => value && value.length > max ? `Deve ter no máximo ${max} caracteres` : undefined;
const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? "Endereço de email inválido" : undefined;
const baseColor = "#333";

var PriceServiceInstance = new PriceService();
var GroupCodeServiceInstance = new GroupCodeService();
var SalesPersonServiceInstance = new SalesPersonService();


export default class extends React.Component {
    listGroupCode = [];
    listPrice = [];
    listSalesPerson = [];
    errors = [];

    constructor(props) {
        super(props);
        this.state = {
            isValidForm: false,
            CardName: '',
            CardCode: '',
            CardType: 'C',
            GroupCode: '',
            GroupCodeText: '',
            Phone1: '',
            Phone2: '',
            EmailAddress: '',
            Fax: '',
            CreditLimit: '',
            PriceListNum: '',
            FreeText: '',
            SalesPersonCode: '',
            PriceListNumText: '',
            SlpCodeText: '',
            Cellular: '',
            AliasName: '',
            Valid: 'Y',
            isValid: false,
            errors: false
        }
    }

    componentDidMount() {
        if (!this.props.cardCode) {
            this.loadListGroupCode();
            this.loadListPrice();
            this.loadListSalesPerson();
        }
    }

    loadBasicInformation() {
        this.props.loadBasicInformation()
            .then((response) => {
                this.setState({
                    ...response
                }, () => {
                    this.loadListGroupCode();
                    this.loadListPrice();
                    this.loadListSalesPerson();
                });
            });
    }

    loadListGroupCode() {
        var request = {
            conditionsUrl: "?$filter=Type eq 'bbpgt_CustomerGroup'"
        }
        GroupCodeServiceInstance.list(request)
            .then((data) => {
                this.serializeListGroupCode(data.value);

                if (this.props.cardCode) {
                    let obj = data.value.find(x => x.Code == this.state.GroupCode);
                    if (obj)
                        this.setState({ GroupCodeText: obj.Name, GroupCode: obj.Code });
                }
                console.log(data);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    loadListPrice() {
        var request = {
            conditionsUrl: "?$select=PriceListNo,PriceListName"
        }
        PriceServiceInstance.list(request)
            .then((data) => {
                this.serializeListPrice(data.value);

                if (this.props.cardCode) {
                    let obj = data.value.find(x => x.PriceListNo == this.state.PriceListNum);
                    if (obj)
                        this.setState({ PriceListNumText: obj.PriceListName, PriceListNo: obj.PriceListNum });
                }
                console.log(data);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    loadListSalesPerson() {
        var request = {
            conditionsUrl: "?$select=SalesEmployeeCode,SalesEmployeeName&$filter=Active eq 'tYES' and Locked eq 'tNO'"
        }
        SalesPersonServiceInstance.list(request)
            .then((data) => {
                this.serializeListSalesPerson(data.value);

                if (this.props.cardCode) {
                    let obj = data.value.find(x => x.SalesEmployeeCode == this.state.SalesPersonCode);
                    if (obj)
                        this.setState({ SlpCodeText: obj.SalesEmployeeName, SalesPersonCode: obj.SalesEmployeeCode });
                }
                console.log(data);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    serializeListPrice(list) {
        list.map((item) => {
            this.listPrice.push({ value: item.PriceListName, PriceListNo: item.PriceListNo });
        });
    }

    serializeListGroupCode(list) {
        list.map((item) => {
            this.listGroupCode.push({ value: item.Name, Code: item.Code });
        });
    }

    serializeListSalesPerson(list) {
        list.map((item) => {
            this.listSalesPerson.push({ value: item.SalesEmployeeName, SalesEmployeeCode: item.SalesEmployeeCode });
        });
    }

    submitForm() {
        this.setBasicInformation();
        return new Promise((resolve, reject) => {
            let submitResults = this.customerForm.validate();
            let errors = [];
            submitResults.forEach(item => {
                errors.push({ field: item.fieldName, error: item.error });
            });
            this.errors = errors;
            let isValidForm = true;
            submitResults.map((item) => {
                if (item.isValid == false)
                    isValidForm = false;
            });
            resolve(isValidForm);
        })
    }

    setBasicInformation() {
        this.props.setBasicInformation(this.state);
    }

    submitSuccess() {
        this.setState({ isValidForm: true });
        console.log("Submit Success!");
    }

    submitFailed() {
        this.setState({ isValidForm: false });
        console.log("Submit Faield!");
    }

    onChangeGroupCode(groupCodeSelected) {
        let obj = this.listGroupCode.find(x => x.value == groupCodeSelected);
        if (obj)
            this.setState({ GroupCodeText: groupCodeSelected, GroupCode: obj.Code });
    }

    onChangeListPrice(priceSelected) {
        let obj = this.listPrice.find(x => x.value == priceSelected);
        if (obj)
            this.setState({ PriceListNumText: priceSelected, PriceListNum: obj.PriceListNum });
    }

    onChangeListSalesPerson(slpSelected) {
        let obj = this.listSalesPerson.find(x => x.value == slpSelected);
        if (obj)
            this.setState({ SlpCodeText: slpSelected, SalesPersonCode: obj.SalesEmployeeCode });
    }

    render() {
        return (
            <Content style={styles.content}>
                <Form
                    ref={(ref) => this.customerForm = ref}
                    validate={true}
                    submit={this.submitSuccess.bind(this)}
                    failed={this.submitFailed.bind(this)}
                    errors={this.errors}>

                    <Field
                        autoCapitalize="characters"
                        textColor={baseColor}
                        tintColor={baseColor}
                        baseColor={baseColor}
                        label="Razão Social"
                        secureTextEntry={false}
                        required
                        component={InputField}
                        validations={[required, minLength(1), maxLength(100)]}
                        name="CardName"
                        value={this.state.CardName}
                        onChangeText={(val) => this.setState({ CardName: val })}
                        customStyle={{ width: 100 }}
                        errors={this.errors}
                    />

                    <Field
                        autoCapitalize="characters"
                        textColor={baseColor}
                        tintColor={baseColor}
                        baseColor={baseColor}
                        label="Nome fantasia"
                        secureTextEntry={false}
                        //required
                        component={InputField}
                        validations={[maxLength(16)]}
                        name="AliasName"
                        value={this.state.AliasName}
                        onChangeText={(val) => this.setState({ AliasName: val })}
                        customStyle={{ width: 100 }}
                        errors={this.errors}
                    />

                    <Field
                        baseColor={baseColor}
                        textColor={baseColor}
                        selectedItemColor={baseColor}
                        containerStyle={{ color: "#333" }}
                        label='Grupo do cliente'
                        required
                        validations={[required]}
                        component={InputDropdown}
                        name="GroupCode"
                        value={this.state.GroupCodeText}
                        onChangeText={(val) => this.onChangeGroupCode(val)}
                        errors={this.errors}
                        data={this.listGroupCode}
                    />

                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 3, marginRight: 20 }}>
                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="DDD"
                                secureTextEntry={false}
                                required
                                component={InputField}
                                validations={[required, minLength(1), maxLength(20)]}
                                name="Phone1"
                                value={this.state.Phone1}
                                onChangeText={(val) => this.setState({ Phone1: val })}
                                customStyle={{ width: 100 }}
                                errors={this.errors}
                            />
                        </View>
                        <View style={{ flex: 7 }}>
                            <Field
                                autoCapitalize="none"
                                textColor={baseColor}
                                tintColor={baseColor}
                                baseColor={baseColor}
                                label="Telefone"
                                secureTextEntry={false}
                                required
                                component={InputField}
                                validations={[required, minLength(1), maxLength(20)]}
                                name="Phone2"
                                value={this.state.Phone2}
                                onChangeText={(val) => this.setState({ Phone2: val })}
                                customStyle={{ width: 100 }}
                                errors={this.errors}
                            />
                        </View>
                    </View>

                    <Field
                        autoCapitalize="none"
                        textColor={baseColor}
                        tintColor={baseColor}
                        baseColor={baseColor}
                        label="Celular"
                        secureTextEntry={false}
                        //required
                        component={InputField}
                        //validations={[required]}
                        name="Cellular"
                        value={this.state.Cellular}
                        onChangeText={(val) => this.setState({ Cellular: val })}
                        customStyle={{ width: 100 }}
                        errors={this.errors}
                    />

                    <Field
                        autoCapitalize="none"
                        textColor={baseColor}
                        tintColor={baseColor}
                        baseColor={baseColor}
                        label="Email"
                        secureTextEntry={false}
                        required
                        component={InputField}
                        validations={[required, minLength(1), maxLength(100), email]}
                        name="EmailAddress"
                        value={this.state.EmailAddress}
                        onChangeText={(val) => this.setState({ EmailAddress: val })}
                        customStyle={{ width: 100 }}
                        errors={this.errors}
                    />


                    <Field
                        autoCapitalize="none"
                        textColor={baseColor}
                        tintColor={baseColor}
                        baseColor={baseColor}
                        label="Fax"
                        secureTextEntry={false}
                        //required
                        component={InputField}
                        validations={[maxLength(20)]}
                        name="Fax"
                        value={this.state.Fax}
                        onChangeText={(val) => this.setState({ Fax: val })}
                        customStyle={{ width: 100 }}
                        errors={this.errors}
                    />

                    <Field
                        autoCapitalize="none"
                        textColor={baseColor}
                        tintColor={baseColor}
                        baseColor={baseColor}
                        label="Limite de crédito"
                        secureTextEntry={false}
                        //required
                        component={InputField}
                        //validations={[required]}
                        keyboardType={'decimal-pad'}
                        name="CreditLimit"
                        value={this.state.CreditLimit}
                        onChangeText={(val) => this.setState({ CreditLimit: val })}
                        customStyle={{ width: 100 }}
                        errors={this.errors}
                    />

                    <Field
                        baseColor={baseColor}
                        textColor={baseColor}
                        selectedItemColor={baseColor}
                        containerStyle={{ color: "#333" }}
                        label='Lista de preços'
                        required
                        validations={[required]}
                        component={InputDropdown}
                        name="PriceListNum"
                        value={this.state.PriceListNumText}
                        onChangeText={(val) => this.onChangeListPrice(val)}
                        errors={this.errors}
                        data={this.listPrice}
                    />

                    <Field
                        autoCapitalize="none"
                        textColor={baseColor}
                        tintColor={baseColor}
                        baseColor={baseColor}
                        label="Observações"
                        secureTextEntry={false}
                        //required
                        component={InputField}
                        validations={[maxLength(16)]}
                        name="FreeText"
                        value={this.state.FreeText}
                        onChangeText={(val) => this.setState({ FreeText: val })}
                        customStyle={{ width: 100 }}
                        errors={this.errors}
                    />

                    <View style={{ marginBottom: 100 }}>
                        <Field
                            baseColor={baseColor}
                            textColor={baseColor}
                            selectedItemColor={baseColor}
                            containerStyle={{ color: "#333" }}
                            label='Vendedor'
                            required
                            validations={[required]}
                            component={InputDropdown}
                            name="SlpCodeText"
                            value={this.state.SlpCodeText}
                            onChangeText={(val) => this.onChangeListSalesPerson(val)}
                            errors={this.errors}
                            data={this.listSalesPerson}
                        />
                    </View>
                </Form>
            </Content>
        )
    }
}
