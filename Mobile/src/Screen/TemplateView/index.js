//Components
import React from "react";
import { Container, Text, View } from "native-base";
import { Header, Button, Icon, Left, Right, Body } from 'native-base';
import { StatusBar } from 'react-native';

//Services
import NavigationService from '@Service/Navigation';

//Styles
import styles from "./styles";
import GlobalStyle from '@Theme/GlobalStyle';

export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        return (
            <Container>
                <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs}>
                    <StatusBar barStyle="light-content" backgroundColor="#999" />
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => NavigationService.openDrawer()}>
                            <Icon active name='menu' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 1 }}>
                        <Text style={[GlobalStyle.actionBarText, { alignSelf: 'center' }]}>{'Filiais'.toUpperCase()}</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                    </Right>
                </Header>
                <View style={styles.viewContainer}>
                </View>
            </Container>
        )
    }
}