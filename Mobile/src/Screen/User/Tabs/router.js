import React from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity, Platform } from 'react-native';
import { View, Text } from 'native-base';

import TabMarketplace from "@Screen/User/Tabs/TabMarketplace";
import TabQrCode from "@Screen/User/Tabs/TabQrCode";
import TabUserWarning from "@Screen/User/Tabs/TabUserWarning";

import NavigationService from '@Service/Navigation';

//Styles
import styles from "./styles";


var param = {};

const TabsUser = createBottomTabNavigator({
    TabMarketplace: {
        screen: props => <TabMarketplace {...props} />,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) =>
                <View style={styles.viewTab}>
                    <IconFontAwesome name="map-marker" color={tintColor} size={23} style={styles.iconTab}></IconFontAwesome>
                    <Text style={styles.textTab}>Marketplace</Text>
                </View>
        },
    },
    TabQrCode: {
        screen: props => <TabQrCode {...props} />,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) =>
                <View style={styles.viewTab}>
                    <IconFontAwesome name="qrcode" color={tintColor} size={40} style={styles.iconTab}></IconFontAwesome>
                </View>
        },
    },
    TabUserWarning: {
        screen: TabUserWarning,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) =>
                <View style={styles.viewTab}>
                    <IconFontAwesome name="bell" color={tintColor} size={23} style={styles.iconTab}></IconFontAwesome>
                    <Text style={styles.textTab}>Avisos</Text>
                </View>
        },
    },
}, {
        tabBarOptions: {
            activeTintColor: '#fff',
            inactiveTintColor: 'rgba(255,255,255,0.6)',
            style: {
                backgroundColor: 'rgba(0,175,240, 0.7)',
            },
            showLabel: false,
            tabBarSelectedItemStyle: {
                borderBottomWidth: 2,
            },
        },
        initialRouteName: "TabMarketplace",
        animationEnabled: true,
        swipeEnabled: true,
        tabBarPosition: 'bottom',
        tabBarComponent: (props) => {
            const {
                navigation: { state: { index, routes } },
                style,
                activeTintColor,
                inactiveTintColor,
                renderIcon,
                jumpTo
            } = props;
            //alert(JSON.stringify(props.navigation.state));
            return (
                <View style={[styles.viewButton, {
                    ...style
                }]}>
                    {
                        routes.map((route, idx) => (
                            <TouchableOpacity
                                key={route.key}
                                style={[styles.buttonTab, index === idx ? { borderTopColor: "#EA3788", borderTopWidth: 4 } : { borderTopWidth: 0 }]}
                                onPress={() => /*NavigationService.navigate("CustomerHistory", {props: props.navigation.state})*/ jumpTo(route.key)}>
                                {renderIcon({
                                    route,
                                    focused: index === idx,
                                    tintColor: index === idx ? activeTintColor : inactiveTintColor
                                })}
                            </TouchableOpacity>

                        ))
                    }
                </View >
            );
        },
    });
export default createAppContainer(TabsUser);