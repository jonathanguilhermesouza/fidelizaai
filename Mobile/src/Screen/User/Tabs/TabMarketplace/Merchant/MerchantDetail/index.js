
//Libs
import {
    Container,
    Text,
    View,
} from "native-base";
import React from "react";
import { Header, Button, Icon, Left, Right, Body } from 'native-base';
import { StatusBar, Image, TouchableOpacity, ScrollView, Alert } from 'react-native';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import ActionSheet from 'react-native-actionsheet';

//Style
import GlobalStyle from '@Theme/GlobalStyle';
import styles from "./styles";

//Services
import CompanyService from "@Service/CompanyService";
import NavigationService from '@Service/Navigation';

//constants
const avatarGeneric = require("@Asset/images/avatar-generic.jpg");

var CompanyServiceInstance = new CompanyService();
export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            companyId: '',
            company: {
                //BPAddresses: []
            }
        }
    }

    componentDidMount() {
        this._sub = this.props.navigation.addListener('didFocus', () => (
            this.setState(
                { companyId: this.props.navigation.state.params.companyId },
                () => this.getCustomerDetail()
            )
        ))
    }

    getCustomerDetail() {
        var request = {
            id: this.props.navigation.state.params.companyId
        }

        CompanyServiceInstance.getById(request)
            .then((data) => {
                alert(JSON.stringify(data));
                this.setState({
                    company: data
                });
                console.log(data);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    showActionSheet = () => {
        this.ActionSheet.show()
    }

    onPressAction = (action) => {
        if (action == 0)
            NavigationService.navigate("CustomerCreate", { CardCode: this.state.cardCode });
        else if (action == 1)
            this.confirmCustomerRemove();
    }

    remove() {
        var request = {
            conditionsUrl: `('${this.state.cardCode}')`
        }
        CompanyServiceInstance.remove(request)
            .then((data) => {
                NavigationService.navigate("Customer");
                console.log(data);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    confirmCustomerRemove() {
        Alert.alert(
            'Pergunta',
            'Deseja mesmo excluir este cliente?',
            [
                {
                    text: 'Sim',
                    onPress: () => this.remove()
                },
                {
                    text: 'Não',
                    //style: 'cancel',
                },
            ],
            { cancelable: false },
        );
    }

    render() {
        const { state, navigate } = this.props.navigation;
        //alert(JSON.stringify(this.props.navigation));
        return (
            <Container>
                <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs}>
                    <StatusBar barStyle="light-content" backgroundColor="#999" />
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => navigate('TabMarketplace', { go_back_key: state.key })}>
                            <Icon active name='arrow-left' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 3 }}>
                        <Text style={[GlobalStyle.actionBarText, { alignSelf: 'center' }]}>{'Dados do local'.toUpperCase()}</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        {/*<PopupMenu actions={['Edit', 'Remove']} onPress={this.onPopupEvent} />*/}
                        <Button transparent onPress={this.showActionSheet}>
                            <Icon active name='dots-vertical' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                        {/*<Button transparent onPress={() => this.setState({ isModalFilterVisible: true })}>
                    <IconFontAwesome name={"filter"} color={"#fff"} size={25} />
                    </Button>*/}
                    </Right>
                </Header>
                <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={'O que deseja fazer ?'}
                    options={['Editar', 'Excluir', 'Cancelar']}
                    cancelButtonIndex={2}
                    //destructiveButtonIndex={1}
                    onPress={(index) => { this.onPressAction(index) }}
                />
                <View style={styles.headerDetailCustomer}>
                    <View style={styles.viewImageHeaderProfile}>
                        <Image source={/*avatarGeneric*/{uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/nemanjaivanovic/128.jpg'}} style={styles.imageHeaderProfile} resizeMode='contain' />
                    </View>
                    <View style={styles.viewDescriptionHeaderProfile}>
                        <View>
                            <Text style={styles.typeUser}>Local</Text>
                            <Text style={styles.textNameUser}>{this.state.company.name}</Text>
                        </View>
                    </View>
                </View>
                <ScrollView>

                    <View style={styles.viewInfoCustomer}>
                        <View style={[styles.viewContainer, {}]}>
                            <Text style={styles.title}>NOME</Text>
                            <Text style={styles.description}>{this.state.company.name}</Text>

                            <Text style={styles.title}>TELEFONE</Text>
                            <Text style={styles.description}>{this.state.company.phoneNumber1}</Text>

                            <Text style={styles.title}>EMAIL</Text>
                            <Text style={styles.description}>{this.state.company.email}</Text>

                            <Text style={styles.title}>ENDEREÇO</Text>
                            <Text style={styles.description}>{this.state.company.fullAddress}</Text>

                            {/*<Text>{JSON.stringify(this.state.customer.BPAddresses)}</Text>*/}

                            {/*{this.state.customer.BPAddresses.map((address) => {
                                return (
                                    <View>
                                        <Text style={styles.title}>ENDEREÇO ({address.AddressName})</Text>
                                        <Text style={styles.description}>{address.TypeOfAddress} {address.Street}, {address.Block}, {address.StreetNo}, {address.ZipCode}, {address.City} - {address.State}</Text>
                                    </View>
                                );
                            })}*/}

                            {/*<Text style={styles.description}>{this.state.customer} Rua das Araras, n 343, 12222-987, SJC - SP</Text>*/}

                            <View style={styles.viewContact}>
                                <TouchableOpacity onPress={() => { }} style={styles.buttonContact}>
                                    <IconFontAwesome name={"phone"} style={styles.iconContact}>
                                    </IconFontAwesome>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => { }} style={styles.buttonContact}>
                                    <IconFontAwesome name={"envelope"} style={styles.iconContact}>
                                    </IconFontAwesome>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => { }} style={styles.buttonContact}>
                                    <IconFontAwesome name={"whatsapp"} style={styles.iconContact}>
                                    </IconFontAwesome>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => { }} style={styles.buttonContact}>
                                    <IconFontAwesome name={"map-marker"} style={styles.iconContact}>
                                    </IconFontAwesome>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => { }} style={styles.buttonContact}>
                                    <IconFontAwesome name={"commenting"} style={styles.iconContact}>
                                    </IconFontAwesome>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>

            </Container >
        )
    }
}