//Packages
import {
    Container,
    Text,
    View,
} from "native-base";
import React from "react";
import { Header, Button, Icon, Left, Right, Body } from 'native-base';
import { StatusBar, Image, TouchableOpacity } from 'react-native';

//Styles
import GlobalStyle from '@Theme/GlobalStyle';
import styles from "./styles";

//Services
import CustomerService from "@Service/CustomerService";

//constants
const avatarGeneric = require("@Asset/images/avatar-generic.jpg");

var CustomerServiceInstance = new CustomerService();
export default class extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cardCode: '',
            customer: {}
        }
    }

    componentDidMount() {
        this._sub = this.props.navigation.addListener('didFocus', () => (
            this.setState(
                { cardCode: this.props.navigation.dangerouslyGetParent().getParam('cardCode') },
                () => this.getCustomerDetail()
            )
        ))
    }

    getCustomerDetail() {
        var request = {
            conditionsUrl: `('${this.state.cardCode}')`
        }
        CustomerServiceInstance.getById(request)
            .then((data) => {
                this.setState({
                    customer: data
                });
                console.log(data);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    render() {
        const { state, navigate } = this.props.navigation;
        return (
            <Container>
                <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs}>
                    <StatusBar barStyle="light-content" backgroundColor="#999" />
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => navigate('TabClient', { go_back_key: state.key })}>
                            <Icon active name='arrow-left' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 3 }}>
                        <Text style={[GlobalStyle.actionBarText, { alignSelf: 'center' }]}>{'Dados do cliente'.toUpperCase()}</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        {/*<Button transparent onPress={() => this.setState({ isModalFilterVisible: true })}>
                        <IconFontAwesome name={"filter"} color={"#fff"} size={25} />
                        </Button>*/}
                    </Right>
                </Header>
                <View style={styles.headerDetailCustomer}>
                    <View style={styles.viewImageHeaderProfile}>
                        <Image source={avatarGeneric} style={styles.imageHeaderProfile} resizeMode='contain' />
                    </View>
                    <View style={styles.viewDescriptionHeaderProfile}>
                        <View>
                            <Text style={styles.typeUser}>Cliente</Text>
                            <Text style={styles.textNameUser}>{this.state.customer.CardName}</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.viewInfoCustomer}>
                    <View style={[styles.viewContainer, {}]}>
                        <View style={[styles.viewRow, { alignItems: "flex-end" }]}>
                            <View style={{ alignItems: "center" }}>
                                <TouchableOpacity onPress={() => { }} style={styles.button}>
                                    <Icon active name='cart-outline' style={styles.icon} type="MaterialCommunityIcons" />
                                </TouchableOpacity>
                                <Text style={styles.descriptionIcon}>Compras</Text>
                            </View>

                            <View style={{ alignItems: "center", marginLeft: 70 }}>
                                <TouchableOpacity onPress={() => { }} style={[styles.button]}>
                                    <Icon active name='briefcase-outline' style={styles.icon} type="MaterialCommunityIcons" />
                                </TouchableOpacity>
                                <Text style={styles.descriptionIcon}>Pedidos</Text>
                            </View>
                        </View>

                        <View style={[styles.viewRow, { marginTop: 50, alignItems: "flex-start" }]}>
                            <View style={{ alignItems: "center" }}>
                                <TouchableOpacity onPress={() => { }} style={styles.button}>
                                    <Icon active name='thumb-down-outline' style={styles.icon} type="MaterialCommunityIcons" />
                                </TouchableOpacity>
                                <Text style={styles.descriptionIcon}>Mais vendidos</Text>
                            </View>

                            <View style={{ alignItems: "center", marginLeft: 70 }}>
                                <TouchableOpacity onPress={() => { }} style={[styles.button]}>
                                    <Icon active name='thumb-up-outline' style={styles.icon} type="MaterialCommunityIcons" />
                                </TouchableOpacity>
                                <Text style={styles.descriptionIcon}>Menos vendidos</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </Container >
        )
    }
}