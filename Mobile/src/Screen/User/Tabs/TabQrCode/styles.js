import { Dimensions } from "react-native";

const variablesTheme = require("@Theme/Variables/CommonColor");
const deviceWidth = Dimensions.get("window").width;

export default {
    container: {
        flex: 1,
        width: null,
        height: null,
        backgroundColor: variablesTheme.getDefaultLightTextColor
    },
    viewButtonCamera: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonCamera: {
        alignItems: 'center',
        justifyContent: 'center',
        width: deviceWidth / 2,
        height: deviceWidth / 2,
        backgroundColor: variablesTheme.getThemeColor,
        borderRadius: 100
    },
    iconCamera: {
        fontSize: deviceWidth / 4,
        color: variablesTheme.getDefaultLightTextColor
    }
}