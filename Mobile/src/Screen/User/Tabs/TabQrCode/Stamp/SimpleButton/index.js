/*
 * @Author: Young
 * DSHARP
 * @flow
 * @Date: 2018-04-07 10:51:10
 * @Last Modified by: Young
 * @Last Modified time: 2018-08-31 14:03:18
 */
//github.com/danceyoung/react-native-selectmultiple-button
import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import _ from "lodash";
import { SelectMultipleButton } from "../../../../../../components/SelectMultipleButton/export";

const ios_blue = "#007AFF";
const multipleData = ["Corte masculino degrade", "corte masculino militar", "Barba", "Bigode", "Cerveja Skol"];

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      multipleSelectedData: [],
      multipleSelectedDataLimited: []
    };
  }

  render() {
    return (
      <View>
        <Text style={styles.welcome}>
          Selecione os produtos comprados e servicos realizados
        </Text>
        <Text style={{ color: ios_blue, marginLeft: 10 }}>
        {_.join(this.state.multipleSelectedData, ", ")}
        </Text>
        <View
          style={{
            flexWrap: "wrap",
            flexDirection: "row",
            justifyContent: "center"
          }}
        >
          {multipleData.map(interest => (
            <SelectMultipleButton
              key={interest}
              buttonViewStyle={{
                borderRadius: 10,
                height: 40
              }}
              textStyle={{
                fontSize: 15
              }}
              highLightStyle={{
                borderColor: "gray",
                backgroundColor: "transparent",
                textColor: "gray",
                borderTintColor: ios_blue,
                backgroundTintColor: ios_blue,
                textTintColor: "white"
              }}
              value={interest}
              selected={this.state.multipleSelectedData.includes(interest)}
              singleTap={valueTap =>
                this._singleTapMultipleSelectedButtons(interest)
              }
            />
          ))}
        </View>

       
      </View>
    );
  }

  _singleTapRadioSelectedButtons(valueTap, gender) {
    // Alert.alert('', valueTap)
    this.setState({
      radioSelectedData: gender
    });
  }

  _singleTapMultipleSelectedButtons(interest) {
    if (this.state.multipleSelectedData.includes(interest)) {
      _.remove(this.state.multipleSelectedData, ele => {
        return ele === interest;
      });
    } else {
      this.state.multipleSelectedData.push(interest);
    }

    this.setState({
      multipleSelectedData: this.state.multipleSelectedData
    });
  }

  _singleTapMultipleSelectedButtons_limited(interest) {
    if (this.state.multipleSelectedDataLimited.includes(interest)) {
      _.remove(this.state.multipleSelectedDataLimited, ele => {
        return ele === interest;
      });
    } else {
      if (this.state.multipleSelectedDataLimited.length < 3)
        this.state.multipleSelectedDataLimited.push(interest);
    }

    this.setState({
      multipleSelectedDataLimited: this.state.multipleSelectedDataLimited
    });
  }
}

const styles = StyleSheet.create({
  welcome: {
    margin: 10,
    marginTop: 30,
    color: "gray"
  }
});