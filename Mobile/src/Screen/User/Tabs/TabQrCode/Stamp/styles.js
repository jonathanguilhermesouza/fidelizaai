import CommonColor from "@Theme/Variables/CommonColor";

export default {
  container: {
    flex: 1,
    width: null,
    height: null,
    backgroundColor: CommonColor.getDefaultLightTextColor
  },
  profileInfoContainer: {
    borderColor: "black",
    borderWidth: 1,
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: CommonColor.getThemeColor,
    paddingTop: 10
  },
  profileUser: {
    borderColor: "black",
    borderWidth: 1,
    color: CommonColor.getDefaultLightTextColor,
    //alignSelf: "center",
    fontSize: 22,
    fontWeight: "bold",
    paddingBottom: 5
  },
  profileUserInfo: {
    //alignSelf: "center",
    opacity: 0.8,
    fontWeight: "bold",
    color: CommonColor.getDefaultLightTextColor
  },
  profilePic: {
    width: 100,
    height: 100,
    borderRadius: 50
  },
  profileInfo: {
    //alignSelf: "center",
    paddingTop: 5,
    paddingBottom: 10 
  },
  button: {
    margin: 5,
    borderRadius: 3,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1
  },
  text: {
    textAlign: 'center',
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 10,
    marginRight: 10,
  },
  headerInfoUser: {
    backgroundColor: CommonColor.getThemeColor,
    height: 150,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: 'row'
  },
  inverseTextColor: {
    color: CommonColor.getDefaultLightTextColor
  },
  containerSelectMultipleButton: {
    flexWrap: "wrap",
    flexDirection: "row",
    justifyContent: "center"
  },
  buttonViewStyleSelectMultipleButton: {
    borderRadius: 10,
    height: 40
  },
  highLightStyleSelectMultipleButton: {
    borderColor: "gray",
    backgroundColor: "transparent",
    textColor: "gray",
    borderTintColor: "#007AFF",
    backgroundTintColor: "#007AFF",
    textTintColor: "white"
  },
  buttonAddItem: {
    flex: 1,
    width: 60,
    height: 60,
    padding: 10,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    backgroundColor: '#EA3788',
  },
  iconButtonAdd: {
    color: CommonColor.getDefaultLightTextColor,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonSave: {
    height: 60,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    backgroundColor: '#EA3788',
  },
  viewSection: {
    flex: 1,
    paddingTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: "center",
    alignItems: "center"
  },
  viewSectionText: {
    color: "#585858",
    fontSize: 20,
    fontWeight: "bold"
  },
  buttonPoints: {
    borderColor: "#00aff0",
    borderWidth: 3,
    color: "transparent",
    alignItems: "center",
    justifyContent: "center",
    padding: 10,
    marginTop: 20,
    borderRadius: 100,
    width: 100,
    height: 100
  },
  buttonPointsText: {
    fontSize: 40,
    color: "#585858"
  },
  viewButtonAddPoints: {
    alignItems: 'center',
    justifyContent: "center",
    marginTop: 20
  }
};
