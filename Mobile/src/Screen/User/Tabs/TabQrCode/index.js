// @flow
import React, { Component } from "react";
import { TouchableOpacity } from "react-native";
import { View, Container } from "native-base";
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import CustomHeader from "@Component/CustomHeader";

import styles from "./styles";

export default class extends React.Component {
  render() {
    const navigation = this.props.navigation;
    return (
      <Container style={styles.container}>
        <CustomHeader navigation={navigation} />
        <View style={styles.viewButtonCamera}>
          <TouchableOpacity onPress={() => navigation.navigate("ScanScreen")} style={styles.buttonCamera}>
            <IconFontAwesome name={"camera"} style={styles.iconCamera} />
          </TouchableOpacity>
          {/*<Text style={{width:400, color:"#000"}}>{JSON.stringify(this.props)}</Text>*/}
        </View>
      </Container>
    );
  }
}
