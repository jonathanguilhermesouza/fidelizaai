{/*https://snack.expo.io/HyiTlAn0e , https://github.com/ekreutz/react-native-barcode-scanner-google


import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {
  View
} from "native-base";

import QRCodeScanner from 'react-native-qrcode-scanner';

export default class ScanScreen extends Component {
  onSuccess(e) {
    Linking
      .openURL(e.data)
      .catch(err => console.error('An error occured', err));
  }

  render() {
    const navigation = this.props.navigation;
    return (
        <QRCodeScanner
          showMarker={true}
          customMarker={
          <View style={styles.rectangleContainer}>
          <View style={styles.rectangle} />
        </View>}
          onRead={this.onSuccess.bind(this)}
          topContent={
            <Text style={styles.centerText}>
              Go to <Text style={styles.textBold}>wikipedia.org/wiki/QR_code</Text> on your computer and scan the QR code.
            </Text>
          }
          bottomContent={
            <TouchableOpacity style={styles.buttonTouchable} onPress={() => navigation.goBack()}>
              <Text style={styles.buttonText}>Voltar</Text>
            </TouchableOpacity>
          }
        />
    );
  }
}

const styles = StyleSheet.create({
  rectangleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  rectangle: {
    height: 250,
    width: 250,
    borderWidth: 2,
    borderColor: 'green',
    backgroundColor: 'transparent'
  },
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});*/}

/*https://github.com/moaazsidat/react-native-qrcode-scanner*/
/*https://stackoverflow.com/questions/48510350/react-native-camera-with-a-transparent-view-for-barcode-scanner-mask */
/*https://github.com/moaazsidat/react-native-qrcode-scanner/issues/115 */
import React, { Component } from "react";

import { View, Dimensions, Text, Image } from "react-native";
//import QRCodeScanner from "react-native-qrcode-scanner";
import { Container, Header, Button, Left, Right, Body, Icon } from "native-base";

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;
const headerLogo = require("@Asset/images/logo-light.png");

import styles from "./styles";
import GlobalStyle from '@Theme/GlobalStyle';
console.disableYellowBox = true;

export default class extends React.Component {
  onSuccess(infoUser) {
    this.props.navigation.navigate("Story", { infoUser })
  }

  componentDidMount() {
    setTimeout(() => {
      var infoUser = { name: 'Jonathan Guilherme', type: 'Cliente', points: 10, pathPhoto: 'https://s3.amazonaws.com/uifaces/faces/twitter/nemanjaivanovic/128.jpg' }
      this.props.navigation.navigate("Stamp", { infoUser })
    }, 2000)
  }

  makeSlideOutTranslation(translationType, fromValue) {
    return {
      from: {
        [translationType]: SCREEN_WIDTH * -0.18
      },
      to: {
        [translationType]: fromValue
      }
    };
  }

  render() {
    const navigation = this.props.navigation;
    return (
      <Container>
        <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs} >
          <Left style={{ flex: 1 }}>
            <Button transparent onPress={() => navigation.goBack()}>
              <Icon active name="arrow-back" />
            </Button>
          </Left>
          <Body style={{ flex: 3 }}>
            <Image source={headerLogo} style={[styles.imageHeader, { alignSelf: 'center' }]} />
          </Body>
          <Right  style={{ flex: 1 }}/>
        </Header>

        {/*<QRCodeScanner showMarker onRead={this.onSuccess.bind(this)} cameraStyle={{ height: SCREEN_HEIGHT }}
          customMarker={
            <View style={styles.rectangleContainer}>
              <View style={styles.topOverlay}>
                <Text style={styles.fontRetangleScreen}>
                  Escaneie o QR Code
              </Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={styles.rectangle}>
                </View>
              </View>
              <View style={styles.bottomOverlay} />
            </View>
          }
        />*/}
      </Container>
    );
  }
}