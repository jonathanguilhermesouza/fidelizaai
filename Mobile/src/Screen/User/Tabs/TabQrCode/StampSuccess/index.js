import React, { Component } from "react";
import { View, Text, Dimensions, TouchableOpacity } from "react-native";
import { Container } from "native-base";
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

import CustomHeaderWithTitle from "@Component/CustomHeaderWithTitle";
import CommonColor from "@Theme/Variables/CommonColor";
import styles from "./styles";

export default class extends React.Component {
  constructor(props) {
    super(props)
  }

  onGoOut() {
    this.props.navigation.navigate("TabsMerchant");
  }

  render() {
    return (
      <Container style={[styles.container]}>
        <CustomHeaderWithTitle title="Sucesso" />
        <View style={styles.viewContainer}>
          <Text style={styles.textStyle}>Prontinho!!!</Text>
          <Text style={styles.textStyle}>Ponto computado com sucesso</Text>
          <View style={styles.viewIconSuccess}>
            <IconFontAwesome name={"check"} size={100} color="#00aff0" />
          </View>
        </View>
        <View style={styles.viewContainerButtonGoOut}>
          <TouchableOpacity onPress={() => this.onGoOut()} style={[styles.buttonSave]}>
            <Text style={{ fontSize: 20, color: CommonColor.getDefaultLightTextColor }}>Sair</Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  }
}