const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  logo: {
    resizeMode: "contain",
    width: deviceHeight / 4,
    height: deviceHeight / 4,
    alignSelf: "center",
  },
  viewContainer: {
    flex: 1, 
    justifyContent: "center", 
    flexDirection: "column"
  },
  welcomeText: {
    alignSelf: "center"
  }
}