const React = require("react-native");
const { Dimensions, Platform } = React;
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const CommonColor = require("@Theme/Variables/CommonColor");

export default {
  signupContainer: {
    paddingLeft: 20,
    paddingRight: 20,
    marginTop:
      deviceWidth < 330
        ? Platform.OS === "android"
          ? deviceHeight / 9 - 20
          : deviceHeight / 10 - 20
        : Platform.OS === "android"
          ? deviceHeight / 9 - 20
          : deviceHeight / 8 - 20
  },
  signupHeader: {
    alignSelf: "center",
    fontSize: 22,
   /* padding: 10,*/
    fontWeight: "bold",
    color: "#FFF",
    /*marginTop:
      Platform.OS === "android" ? deviceHeight / 6 : deviceHeight / 6 + 10*/
    marginTop: Platform.OS === "android" ? deviceHeight / 13 : deviceHeight / 13 + 10
  },
  background: {
    flex: 1,
    width: null,
    height: null,
    backgroundColor: CommonColor.getThemeColor
  },
  formErrorIcon: {
    color: "#fff",
    marginTop: 5,
    right: 10
  },
  formErrorText1: {
    fontSize: Platform.OS === "android" ? 12 : 15,
    color: "red",
    textAlign: "right",
    top: -10
  },
  formErrorText2: {
    fontSize: Platform.OS === "android" ? 12 : 15,
    color: "transparent",
    textAlign: "right",
    top: -10
  },
  inputGrp: {
    flexDirection: "row",
    borderRadius: 25,
    backgroundColor: "rgba(255,255,255,0.2)",
    marginBottom: 10,
    borderWidth: 0,
    borderColor: "transparent"
  },
  input: {
    paddingLeft: 10,
    color: "#FFF"
  },
  signupBtn: {
    height: 50,
    marginTop: 10,
    backgroundColor: '#fff'
  },
  otherLinkText: {
    alignSelf: "center",
    opacity: 0.8,
    fontSize: 14,
    fontWeight: "bold",
    color: "#EFF"
  },
  otherLinksContainer: {
    flexDirection: "row",
    marginTop: 10
  },
  helpBtns: {
    opacity: 0.9,
    fontSize: 14,
    fontWeight: "bold",
    color: "#FFF",
    alignSelf: "center"
  },
  containerLoginExterno: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  containerButtonLoginExterno: {
    width: (deviceWidth-50)/2,
    height: 40,
    padding: 10,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:10,
    borderColor: '#fff', 
    borderWidth: 1
  },
};
