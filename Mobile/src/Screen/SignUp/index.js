// @flow
import React, { Component } from "react";
import { ImageBackground, TouchableOpacity, StatusBar } from "react-native";
import {
  Container,
  Content,
  Text,
  Button,
  Icon,
  Item,
  Input,
  View,
  Toast,
  Left,
  Right,
  Footer
} from "native-base";
import { reduxForm } from "redux-form";
import { Form, Field } from 'react-native-validate-form';

import styles from "./styles";
import commonColor from "@Theme/Variables/CommonColor";
import { TextField } from 'react-native-material-textfield';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import InputField from '@Component/Input/InputField';
const required = value => (value ? undefined : "Obrigatório");
const maxLength = max => value =>
  value && value.length > max ? `Deve ter no máximo ${max} caracteres` : undefined;
const maxLength15 = maxLength(15);
const minLength = min => value =>
  value && value.length < min ? `Deve ter no mímino ${min} caracteres` : undefined;
const minLength8 = minLength(8);
const minLength5 = minLength(5);
const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? "Invalid email address"
    : undefined;
const alphaNumeric = value =>
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? "Deve ter apenas caracteres alfanuméricos"
    : undefined;

//Servicos
import AccountService from '@Service/AccountService';

var AccountServiceInstance = new AccountService();
class SignUpForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      confirmPassword: '',
      provider: '',
      id: '',
      givenName: '',
      familyName: '',
      photo: ''
    }
  }

  componentWillMount(){
    if(this.props.navigation.state && this.props.navigation.state.params)
        this.setState({
          email: this.props.navigation.state.params.email,
          name: this.props.navigation.state.params.name,
          provider: this.props.navigation.state.params.provider,
          id: this.props.navigation.state.params.id,
          givenName: this.props.navigation.state.params.givenName,
          familyName: this.props.navigation.state.params.familyName,
          photo: this.props.navigation.state.params.photo
        });
  }

  textInput: any;
  renderInput({ input, label, type, meta: { touched, error, warning } }) {
    return (
      <View>
        {/*<Item error={error && touched} rounded style={styles.inputGrp}>
          <Icon
            active
            name={
              input.name === "username"
                ? "person"
                : input.name === "email" ? "mail" : "unlock"
            }
            style={{ color: "#fff" }}
          />
          <Input
            ref={c => (this.textInput = c)}
            placeholderTextColor="#FFF"
            style={styles.input}
            placeholder={
              input.name === "username"
                ? "Username"
                : input.name === "email" ? "Email" : "Password"
            }
            secureTextEntry={input.name === "password" ? true : false}
            {...input}
          />
          {touched && error
            ? <Icon
                active
                style={styles.formErrorIcon}
                onPress={() => this.textInput._root.clear()}
                name="close"
              />
            : <Text />}
        </Item>
        {touched && error
          ? <Text style={styles.formErrorText1}>
              {error}
            </Text>
        : <Text style={styles.formErrorText2}>> error here</Text>}*/}
        <TextField
                        autoCapitalize = {input.name === "email" ? "none" : "words"}
                        ref={c => (this.textInput = c)}
                        textColor= '#fff'
                        tintColor= '#fff'
                        baseColor= '#fff'
                        label={input.name === "name" ? "NOME" : input.name === "email" ? "EMAIL" : input.name === "confirmPassword" ? "CONFIRME A SENHA" : "SENHA"}
                        secureTextEntry={input.name === "password" | input.name === "confirmPassword" ? true : false}
                        {...input}
              />
         
          {touched && error
          ? <Text style={styles.formErrorText1}>
              {error}
            </Text>
          : <Text style={styles.formErrorText2}>error here</Text>}
      </View>
    );
  }

  save(){
    if(this.state.provider)
      this.signInWithExternalLogin();
    else
      this.register();
  }

  register(){

    var loginRequest = {
      email: this.state.email,
      name: this.state.name,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword
    }
    AccountServiceInstance.register(loginRequest)
      .then((response) => {
        //alert(JSON.stringify(response.data));
      if(response.isAuthenticated)
        this.props.navigation.navigate("TabsMerchant");
      if(response.response.data == 90)
        this.props.navigation.navigate("SignUp", loginRequest);
      })
      .catch((error) => {
        //alert(JSON.stringify(error.data));
        console.log(error);
      }).finally(() => {
        this.setState({ spinner: false });
      });
  }

  signInWithExternalLogin(){
    var loginRequest = {
      email: this.state.email,
      id: this.state.id,
      name: this.state.name,
      givenName: this.state.familyName,
      familyName: this.state.givenName,
      provider: this.state.provider,
      photo: this.state.photo
    }
    AccountServiceInstance.signInWithExternalLogin(loginRequest)
      .then((response) => {
        alert(JSON.stringify(response));
      if(response.is_loged)
        this.props.navigation.navigate("TabsMerchant");
      if(response.response.data == 90)
        this.props.navigation.navigate("SignUp", loginRequest);
      })
      .catch((error) => {
        alert(JSON.stringify(error.data));
        console.log(error);
      }).finally(() => {
        this.setState({ spinner: false });
      });
  }

  signUp() {
    if (this.props.valid) {
      this.props.navigation.goBack();
    } else {
      Toast.show({
        text: "Todos os campos são obrigataórios!",
        duration: 2500,
        position: "top",
        textStyle: { textAlign: "center" }
      });
    }
  }

  googleAuth(){
    this.setState({
      provider: 'Google'
    });
  }

  render() {
    return (
      <Container>
        {/*<StatusBar
          backgroundColor={commonColor.statusBarColor}
          barStyle="light-content"
        />
        {/*<ImageBackground
          source={require("../../../assets/bg-signup.png")}
          style={styles.background}
        >*/}
           <StatusBar barStyle="light-content" backgroundColor="#999" />
          <Content contentContainerStyle={styles.background}>
            <Text style={styles.signupHeader}>CRIAR CONTA {this.state.provider == 'Google' ? "COM GOOGLE" : this.state.provider == 'Facebook' ? "COM FACEBOOK" : ""}</Text>
            <View style={styles.signupContainer}>
            <Field
                      autoCapitalize="none"
                      textColor={"#fff"}
                      tintColor={"#fff"}
                      baseColor={"#fff"}
                      label="NOME"
                      secureTextEntry={false}
                      required
                      component={InputField}
                      validations={[required]}
                      name="name"
                      value={this.state.name}
                      onChangeText={(val) => this.setState({ name: val })}
                      customStyle={{ width: 100 }}
                      errors={this.state.errors}
                    />

                  {!this.state.provider || this.state.provider == '' ?
                  <View>
                  <Field
                      autoCapitalize="none"
                      textColor={"#fff"}
                      tintColor={"#fff"}
                      baseColor={"#fff"}
                      label="EMAIL"
                      secureTextEntry={false}
                      required
                      component={InputField}
                      validations={[required]}
                      name="email"
                      value={this.state.email}
                      onChangeText={(val) => this.setState({ email: val })}
                      customStyle={{ width: 100 }}
                      errors={this.state.errors}
                    />

                    <Field
                      autoCapitalize="none"
                      textColor={"#fff"}
                      tintColor={"#fff"}
                      baseColor={"#fff"}
                      label="SENHA"
                      secureTextEntry={false}
                      required
                      component={InputField}
                      validations={[alphaNumeric, minLength8, maxLength15, required]}
                      name="password"
                      value={this.state.password}
                      onChangeText={(val) => this.setState({ password: val })}
                      customStyle={{ width: 100 }}
                      errors={this.state.errors}
                    />

                  <Field
                      autoCapitalize="none"
                      textColor={"#fff"}
                      tintColor={"#fff"}
                      baseColor={"#fff"}
                      label="CONFIRMAR SENHA"
                      secureTextEntry={false}
                      required
                      component={InputField}
                      validations={[alphaNumeric, minLength8, maxLength15, required]}
                      name="password"
                      value={this.state.confirmPassword}
                      onChangeText={(val) => this.setState({ confirmPassword: val })}
                      customStyle={{ width: 100 }}
                      errors={this.state.errors}
                    />
                    </View>
                    : null}
              {/*<Field
                name="confirmPassword"
                component={this.renderInput}
                type="password"
                validate={[alphaNumeric, minLength8, maxLength15, required]}
              />

              <Field
                name="confirmPassword"
                component={this.renderInput}
                type="password"
                validate={[alphaNumeric, minLength8, maxLength15, required]}
              />*/}


              <TouchableOpacity style={{padding: 10,
                  backgroundColor: "#fff",
                  borderRadius: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop:80}} onPress={() => this.save()}>
                  <Text style={{ color: "#00aff0" }}>Salvar</Text>
              </TouchableOpacity>
              
            </View>
          </Content>
          <Footer
            style={{
              paddingLeft: 0,
              paddingRight: 0,
              backgroundColor: '#EA3788'
            
            }}
          >
            <Left style={{ flex: 1,  alignItems: "center",
              alignContent: "center" }}>
              <Button small transparent>
                <Text style={styles.helpBtns}>Termos & Condicões</Text>
              </Button>
            </Left>
            <Right style={{ flex: 1,  alignItems: "center",
              alignContent: "center" }}>
              <Button
                smalls
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Text style={styles.helpBtns}>Voltar para Login</Text>
              </Button>
            </Right>
          </Footer>
          {/*</ImageBackground>*/}
      </Container>
    );
  }
}

const SignUp = reduxForm({
  form: "signup"
})(SignUpForm);
export default SignUp;
