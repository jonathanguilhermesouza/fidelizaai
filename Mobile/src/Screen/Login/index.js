//Packages
import React, { Component } from "react";
import { Image, StatusBar, TouchableOpacity, AsyncStorage } from "react-native";
import {
  Container,
  Content,
  Text,
  View,
  Toast,
  ActionSheet,
  Switch,
  Left,
  Right,
  Button
} from "native-base";
import { Form, Field } from 'react-native-validate-form';
import { reduxForm } from "redux-form";
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import { GoogleSignin } from 'react-native-google-signin';
import { LoginManager, GraphRequest, GraphRequestManager, AccessToken } from 'react-native-fbsdk';

//Components
import InputField from '@Component/Input/InputField';
import PasswordInputText from '@Component/Input/PasswordInputText';

//Services
import AccountService from '@Service/AccountService';
import NetworkService from '@Service/NetworkService';

//Styles
import styles from "./styles";
import CommonColor from '@Theme/Variables/CommonColor';

//Consts
const lightColor = CommonColor.getDefaultLightTextColor;
const thumbTintColor = CommonColor.themeColor;
const onTintColor = "#cccccc";
const logo = require("@Asset/images/logo-light.png");
const required = value => (value ? undefined : "Obrigatório");
/*const maxLength = max => value => value && value.length > max ? `Deve ter no máximo ${max} caracteres` : undefined;
const minLength = min => value => value && value.length < min ? `Deve ter no mímino ${min} caracteres` : undefined;
Funcoes de validacao, descomente caso necessário utilizar alguma
const maxLength15 = maxLength(15);
const minLength8 = minLength(8);
const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? "Endereço de email inválido" : undefined;
const alphaNumeric = value => value && /[^a-zA-Z0-9 ]/i.test(value) ? "Deve ter apenas caracteres alfanuméricos" : undefined;
*/

var AccountServiceInstance = new AccountService();
class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      businessUnit: '',
      toggleRememberCredentials: true,
      toggleRememberLoginOffline: false,
      dateTimeLogin: new Date(),
      sessionId: '',
      nameIcon: 'visibility-off',
      isPasswordInvisible: true,
      errors: [],
      isInvalidForm: false,
      spinner: false
    }
  }

  componentWillMount() {
    Toast.toastInstance = null;
    ActionSheet.actionsheetInstance = null;
    this.getDataCredentials();
    this.setupGoogleSignin();
    //this.loginFacebook();
  }

  //https://www.codementor.io/microsem31/react-native-google-and-facebook-authentication-cohpznykf
  //https://github.com/react-native-community/react-native-google-signin
  //https://github.com/react-native-community/react-native-google-signin/blob/master/docs/android-guide.md
  //https://stackoverflow.com/questions/44577495/react-native-facebook-and-google-login
  //https://blog.benestudio.co/social-login-with-react-native-6157ba3cff1c
  setupGoogleSignin = async () =>{
    try {
      await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true});
      GoogleSignin.configure(
        {
          webClientId: '229102606843-auefa39qsdlvg9ltdkrr6pgnt46l1ek8.apps.googleusercontent.com' // client ID of type WEB for your server (needed to verify user ID and offline access)
        }
      );
    }
    catch (err) {
      console.log("Google signin error", err.code, err.message);
    }
  }

  getCurrentUser = async () => {
    const currentUser = await GoogleSignin.getCurrentUser();
    currentUser.user.provider = 'Google';
    this.signInWithExternalLogin(currentUser.user);
    //this.props.navigation.navigate("SignUp", currentUser.user);
  };

  loginFacebook = async () => {
    LoginManager.logInWithPermissions(['public_profile','email']).then((result) => {
        if (result.isCancelled) {
        } else {
          /*alert('Login was successful with permissions: '
            + result.grantedPermissions.toString());*/
            AccessToken.getCurrentAccessToken().then(
              (data) => {
                let accessToken = data.accessToken;
                const responseInfoCallback = (error, result) => {
                  if (error) {
                    console.log(error)
                    //alert('Error fetching data: ' + error.toString());
                  } else {
                    console.log(result);

                    let userData = {
                      id: result.id,
                      email: result.email,
                      name: result.name,
                      givenName: result.first_name,
                      familyName: result.name,
                      provider: 'Facebook',
                      photo: result.picture.data.url
                    }
                    this.props.navigation.navigate("SignUp", userData);
                  }
                }

                const infoRequest = new GraphRequest(
                  '/me',
                  {
                    accessToken: accessToken,
                    parameters: {
                      fields: {
                        string: 'email,name,first_name,middle_name,last_name,picture.type(large),gender'
                      }
                    }
                  },
                  responseInfoCallback
                );

                /*const infoRequest = new GraphRequest(
                  '/me?fields=id,first_name,last_name,name,picture.type(large),email,gender',
                  null,
                  responseInfoCallback,
                )*/

                // Start the graph request.
                new GraphRequestManager().addRequest(infoRequest).start()
              });
          }
      },
      function(error) {
      }
    ).catch(function(error){
    });
  }

  loginGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true});
      const userInfo = await GoogleSignin.signIn();
      //alert(JSON.stringify(userInfo));
      this.getCurrentUser();
      this.setState({ userInfo });
    } catch (error) {
      alert(JSON.stringify(error));
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  }

  //#region Local Storage
  setItemStorage = async (basicDataUserLogged) => {
    try {
      await AsyncStorage.setItem('BASIC_DATA_USER_LOGGED', JSON.stringify(basicDataUserLogged));
    } catch (error) {
    }
  }

  removeDataCredentials = async () => {
    try {
      AsyncStorage.removeItem('BASIC_DATA_USER_LOGGED');
    } catch (error) {
      console.log(error);
    }
  }

  getDataCredentials = async () => {
    try {
      AsyncStorage.getItem('BASIC_DATA_USER_LOGGED', (err, result) => {
        if (result !== null) {
          let basicDataUserLogged = JSON.parse(result);

          this.setState({
            username: basicDataUserLogged.username,
            password: basicDataUserLogged.password,
            businessUnit: basicDataUserLogged.businessUnit,
            sessionId: basicDataUserLogged.sessionId,
            dateTimeLogin: basicDataUserLogged.dateTimeLogin,
            toggleRememberCredentials: basicDataUserLogged.toggleRememberCredentials,
            toggleRememberLoginOffline: basicDataUserLogged.toggleRememberLoginOffline
          });
        }
      });
    } catch (error) {
      console.log(error);
    }
  }

  saveInfoLoginStorage() {
    let basicDataUserLogged = {
      username: this.state.username,
      password: this.state.password,
      businessUnit: this.state.businessUnit,
      sessionId: this.state.sessionId,
      dateTimeLogin: this.state.dateTimeLogin,
      toggleRememberCredentials: this.state.toggleRememberCredentials,
      toggleRememberLoginOffline: this.state.toggleRememberLoginOffline
    }
    if (this.state.toggleRememberCredentials)
      this.setItemStorage(basicDataUserLogged);
    else
      this.removeDataCredentials();
  }
  //#endregion

  //#region Login
  signInWithExternalLogin(user){

    var loginRequest = {
      email: user.email,
      id: user.id,
      name: user.name,
      givenName: user.familyName,
      familyName: user.givenName,
      provider: user.provider,
      photo: user.photo
    }
    AccountServiceInstance.signInWithExternalLogin(loginRequest)
      .then((response) => {
        //alert(JSON.stringify(response));
      if(response.data.is_loged)
        this.props.navigation.navigate("TabsUser");
      if(response.response.data == 90)
        this.props.navigation.navigate("SignUp", loginRequest);
      })
      .catch((error) => {
        console.log(error);
      }).finally(() => {
        this.setState({ spinner: false });
      });
  }

  login() {
    //this.setState({ spinner: true });
    let loginRequest = {
        email: this.state.username,
        password: this.state.password
    };
    AccountServiceInstance.login(loginRequest)
      .then((data) => {
        //this.setState({ sessionId: data.SessionId });
        //this.saveInfoLoginStorage();
        if (data.isAuthenticated)
          this.props.navigation.navigate("TabsUser");
        console.log(data);
      })
      .catch((error) => {
        console.log(error);
      }).finally(() => {
        //this.setState({ spinner: false });
      });
  }
  //#endregion Login

  showPassword() {
    let newState;
    if (this.state.isPasswordInvisible) {
      newState = {
        nameIcon: 'visibility',
        isPasswordInvisible: false
      }
    } else {
      newState = {
        nameIcon: 'visibility-off',
        isPasswordInvisible: true
      }
    }

    // set new state value
    this.setState(newState)
  }

  //#region Formulario
  submitForm() {
    let submitResults = this.loginForm.validate();
    let errors = [];

    submitResults.forEach(item => {
      errors.push({ field: item.fieldName, error: item.error });
    });
    this.setState({ errors: errors });
  }

  submitSuccess() {
    this.setState({ isInvalidForm: false });
    NetworkService.isConnected()
      .then((isConnected) => {
        if (isConnected)
          this.login();
      });
    console.log("Submit Success!");
  }

  submitFailed() {
    alert(JSON.stringify('submitFailed'));
    this.setState({ isInvalidForm: true });
    alert('Verifique os campos e tente novamente.');
    console.log("Submit Faield!");
  }
  //#endregion Formulario

  render() {
    return (
      <Container>
        {/*<Modal
          animationType="none"
          transparent={true}
          visible={this.state.spinner}
         >
          <View style={{ flex: 1, justifyContent: "center", backgroundColor: "rgba(0, 0, 0, 0.5)" }}>
            <ActivityIndicator size="large" color="#fff" />
          </View>
        </Modal>*/}

        {/*<ImageBackground source={require('@Asset/images/background-login.png')} style={styles.backgroundImage} >*/}
          <StatusBar barStyle="light-content" backgroundColor="#999" />
          <Content contentContainerStyle={styles.background}>

            {/*<Spinner style={{marginBottom: 50}} isVisible={this.state.spinner} size={100} type={"Circle"} color={"#fff"}/>*/}
            <View style={styles.containerLogo}>
              <Image source={logo} style={styles.logo} />
            </View>
            <View style={styles.container}>
              <View style={styles.form}>
                <Form
                  ref={(ref) => this.loginForm = ref}
                  validate={true}
                  submit={this.submitSuccess.bind(this)}
                  failed={this.submitFailed.bind(this)}
                  errors={this.state.errors}
                >
                  {/*Usuário*/}
                  <View>
                    <Field
                      autoCapitalize="none"
                      textColor={lightColor}
                      tintColor={lightColor}
                      baseColor={lightColor}
                      label="USUÁRIO"
                      secureTextEntry={false}
                      required
                      component={InputField}
                      validations={[required]}
                      name="username"
                      value={this.state.username}
                      onChangeText={(val) => this.setState({ username: val })}
                      customStyle={{ width: 100 }}
                      errors={this.state.errors}
                    />
                  </View>

                  {/*Senha*/}
                  <View>
                    <Field
                      autoCapitalize="none"
                      textColor={lightColor}
                      tintColor={lightColor}
                      baseColor={lightColor}
                      label="SENHA"
                      secureTextEntry={this.state.isPasswordInvisible}
                      required
                      component={PasswordInputText}
                      validations={[required]}
                      name="password"
                      value={this.state.password}
                      onChangeText={(password) => this.setState({ password })}
                      customStyle={{ width: 100 }}
                      errors={this.state.errors}
                      nameIcon={this.state.nameIcon}
                      onPress={() => this.showPassword()}
                      iconColor={lightColor}
                    />
                  </View>

                  <View style={styles.otherLinksContainer}>
                    <Left>
                      <Button
                      small
                      transparent
                      style={{ alignSelf: "flex-start" }}
                      onPress={() => this.props.navigation.navigate("SignUp")}>
                      <Text style={[styles.helpBtns, {paddingLeft: 0}]}>Maneiro! Quero um login</Text>
                      </Button>
                    </Left>
                    <Right>
                      <Button
                        small
                        transparent
                        style={{ alignSelf: "flex-end" }}
                        onPress={() => this.props.navigation.navigate("ForgotPassword")}>
                        <Text style={[styles.helpBtns, {paddingRight: 0}]}>Putz! Esqueci a senha</Text>
                      </Button>
                    </Right>
                  </View>
                </Form>

                {/*Botão Login*/}
                <TouchableOpacity style={styles.buttonLogin} onPress={this.submitForm.bind(this)} disabled={this.state.spinner}>
                  <Text style={{ color: CommonColor.defaultThemeTextColor }}>Entrar</Text>
                </TouchableOpacity>
                <View style={styles.containerLoginExterno}>
                  <TouchableOpacity style={styles.containerButtonLoginExterno} onPress={this.loginGoogle.bind(this)}>
                    <IconFontAwesome size={20} style={{ color: "#fff", justifyContent: "center", alignItems: "center" }} name="google" backgroundColor='transparent'>
                    </IconFontAwesome>
                  </TouchableOpacity>
                  <TouchableOpacity style={styles.containerButtonLoginExterno} onPress={this.loginFacebook.bind(this)}>
                    <IconFontAwesome size={20} style={{ color: "#fff", justifyContent: "center", alignItems: "center" }} name="facebook" backgroundColor='transparent'>
                    </IconFontAwesome>
                  </TouchableOpacity>
                </View>

                {/*Switch Container*/}
                <View style={styles.switchContainer}>
                  <View style={{ flexDirection: "row" }}>
                    <Switch
                      thumbTintColor={this.state.toggleRememberCredentials ? thumbTintColor : CommonColor.getDefaultLightTextColor}
                      onTintColor={onTintColor}
                      style={styles.switch}
                      onValueChange={(value) => this.setState({ toggleRememberCredentials: value })}
                      value={this.state.toggleRememberCredentials} />
                    <Text style={styles.switchLabel}>Lembrar usuário e senha</Text>
                  </View>
                </View>
              </View>
            </View>
          </Content>
       {/* </ImageBackground>*/}
      </Container>
    );
  }
}
const Login = reduxForm({
  form: "login"
})(LoginForm);
export default Login;
