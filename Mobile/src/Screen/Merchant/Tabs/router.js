import React from 'react';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import { TouchableOpacity, Platform } from 'react-native';
import { View, Text } from 'native-base';

import TabClient from "@Screen/Merchant/Tabs/TabClient";
import TabStamp from "@Screen/Merchant/Tabs/TabStamp";
import TabWarning from "@Screen/Merchant/Tabs/TabWarning";

import NavigationService from '@Service/Navigation';

//Styles
import styles from "./styles";


var param = {};

const TabsMerchant = createBottomTabNavigator({
    TabClient: {
        screen: props => <TabClient {...props} />,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) =>
                <View style={styles.viewTab}>
                    <IconFontAwesome name="users" color={tintColor} size={23} style={styles.iconTab}></IconFontAwesome>
                    <Text style={styles.textTab}>Clientes</Text>
                </View>
        },
    },
    TabStamp: {
        screen: props => <TabStamp {...props} />,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) =>
                <View style={styles.viewTab}>
                    <IconFontAwesome name="qrcode" color={tintColor} size={40} style={styles.iconTab}></IconFontAwesome>
                </View>
        },
    },
    TabWarning: {
        screen: TabWarning,
        navigationOptions: {
            tabBarIcon: ({ tintColor }) =>
                <View style={styles.viewTab}>
                    <IconFontAwesome name="bell" color={tintColor} size={23} style={styles.iconTab}></IconFontAwesome>
                    <Text style={styles.textTab}>Avisos</Text>
                </View>
        },
    },
}, {
        tabBarOptions: {
            activeTintColor: '#fff',
            inactiveTintColor: 'rgba(255,255,255,0.6)',
            style: {
                backgroundColor: 'rgba(0,175,240, 0.7)',
            },
            showLabel: false,
            tabBarSelectedItemStyle: {
                borderBottomWidth: 2,
            },
        },
        initialRouteName: "TabStamp",
        animationEnabled: true,
        swipeEnabled: true,
        tabBarPosition: 'bottom',
        tabBarComponent: (props) => {
            const {
                navigation: { state: { index, routes } },
                style,
                activeTintColor,
                inactiveTintColor,
                renderIcon,
                jumpTo
            } = props;
            //alert(JSON.stringify(props.navigation.state));
            return (
                <View style={[styles.viewButton, {
                    ...style
                }]}>
                    {
                        routes.map((route, idx) => (
                            <TouchableOpacity
                                key={route.key}
                                style={[styles.buttonTab, index === idx ? { borderTopColor: "#EA3788", borderTopWidth: 4 } : { borderTopWidth: 0 }]}
                                onPress={() => /*NavigationService.navigate("CustomerHistory", {props: props.navigation.state})*/ jumpTo(route.key)}>
                                {renderIcon({
                                    route,
                                    focused: index === idx,
                                    tintColor: index === idx ? activeTintColor : inactiveTintColor
                                })}
                            </TouchableOpacity>

                        ))
                    }
                </View >
            );
        },
    });
export default createAppContainer(TabsMerchant);