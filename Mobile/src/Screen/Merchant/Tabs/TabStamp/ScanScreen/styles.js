import { Dimensions } from "react-native";

const deviceWidth = Dimensions.get("window").width;

const overlayColor = "rgba(0,0,0,0.5)"; // this gives us a black color with a 50% transparency
//const rectDimensions = deviceWidth * 0.65; // this is equivalent to 255 from a 393 device width
const rectDimensions = deviceWidth; // this is equivalent to 255 from a 393 device width
const rectBorderWidth = deviceWidth * 0.005; // this is equivalent to 2 from a 393 device width
const rectBorderColor = "transparent";
const scanBarWidth = deviceWidth * 0.46; // this is equivalent to 180 from a 393 device width
const scanBarHeight = deviceWidth * 0.0025; //this is equivalent to 1 from a 393 device width
const scanBarColor = "#22ff00";

export default {
    imageHeader: {
        height: 25,
        width: 95,
        resizeMode: "contain"
    },

    rectangleContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "transparent"
    },

    rectangle: {
        height: rectDimensions,
        width: rectDimensions,
        borderWidth: rectBorderWidth,
        borderColor: rectBorderColor,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "transparent"
    },

    topOverlay: {
        flex: 1,
        height: deviceWidth,
        width: deviceWidth,
        backgroundColor: overlayColor,
        justifyContent: "center",
        alignItems: "center"
    },

    bottomOverlay: {
        flex: 1,
        height: deviceWidth,
        width: deviceWidth,
        backgroundColor: overlayColor,
        paddingBottom: deviceWidth * 0.25
    },

    leftAndRightOverlay: {
        height: deviceWidth * 0.65,
        width: deviceWidth,
        backgroundColor: overlayColor
    },

    scanBar: {
        width: scanBarWidth,
        height: scanBarHeight,
        backgroundColor: scanBarColor
    },
    fontRetangleScreen:{
        fontSize: 30, 
        color: "white"
    }
}