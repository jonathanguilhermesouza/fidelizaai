import { Dimensions } from "react-native";

const variablesTheme = require("@Theme/Variables/CommonColor");
const deviceWidth = Dimensions.get("window").width;

export default {
    container: {
        flex: 1,
        width: null,
        height: null,
        backgroundColor: variablesTheme.getDefaultLightTextColor
    },
    fontColor: {
        color: "#585858"
    },
    fontSizeSubtitlesAndDescriptions: {
        fontSize: 20
    },
    descriptionTitle: {

    },
    borderLineSection: {
        borderBottomColor: '#585858',
        borderBottomWidth: 1,
        marginTop: 10
    },
    styleRowView: {
        flexDirection: "row",
        marginTop: 10
    },
    textTitleInfo: {
        fontWeight: "bold"
    },
    viewRowStamp: {
        flexDirection: "row",
        justifyContent: "center"
    },
    viewRowStampItem: {
        width: deviceWidth / 4,
        height: deviceWidth / 4,
        borderRadius: 100,
        borderColor: "#00aff0",
        borderWidth: 3,
        margin: deviceWidth * 0.01,
        alignItems: "center",
        justifyContent: "center"
    },
    iconStamp: {
        fontSize: 80,
        color: variablesTheme.getDefaultLightTextColor
    },
    textStamp: {
        color: "#585858",
        fontSize: 30
    },
    viewDescription: {
        marginTop: 10,
        minHeight: 40
    },
    scrollView: {
        marginLeft: 10,
        marginRight: 10
    },
    viewSectionInfo: {
        alignItems: "center",
        marginTop: 30
    },
    viewSectionPoints: {
        alignItems: "center",
        marginTop: 10,
        marginTop: 40
    },
    viewContainerStamps: {
        justifyContent: "center",
        marginTop: 40
    }
}