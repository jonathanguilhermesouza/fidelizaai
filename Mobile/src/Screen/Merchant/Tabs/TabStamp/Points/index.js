import React, { Component } from "react";
import { View, Text, Dimensions, ScrollView } from "react-native";
import { Container } from "native-base";
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import moment from 'moment';

import CustomHeaderWithTitle from "@Component/CustomHeaderWithTitle";
import styles from "./styles";

const deviceWidth = Dimensions.get("window").width;

export default class extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      stampPoints: [],
      campaign: {}
    }
  }

  componentDidMount() {
    this.setState({
      stampPoints: [
        {
          containsStamp: true
        },
        {
          containsStamp: true
        },
        {
          containsStamp: true
        },
        {
          containsStamp: true
        },
        {
          containsStamp: true
        },
        {
          containsStamp: true
        },
        {
          containsStamp: true
        },
        {
          containsStamp: true
        },
        {
          containsStamp: false
        },
        {
          containsStamp: false
        }, {
          containsStamp: false
        }, {
          containsStamp: false
        }
      ],
      campaign: {
        description: 'Ganhe um corte',
        shelfLife: new Date(),
        totalPointsGain: 5,
        totalPointsToWin: 10
      }
    })
  }

  onGoOut() {
    this.props.navigation.navigate("TabsMerchant");
  }

  render() {
    const { navigation } = this.props;

    var stamps = [];
    let cols = 3;
    let size = this.state.stampPoints.length / cols;
    let item = 0;
    for (let i = 0; i < size; i++) {
      let row = [];
      for (let j = 0; j < cols; j++) {
        if (item >= this.state.stampPoints.length)
          break;

        row.push(
          <View style={[styles.viewRowStampItem, { backgroundColor: this.state.stampPoints[item].containsStamp ? "#00aff0" : "#FFF" }]}>
            {this.state.stampPoints[item].containsStamp ? <IconFontAwesome name={"smile-o"} style={styles.iconStamp} /> : <Text style={styles.textStamp}>{item + 1}</Text>}
          </View>
        );
        item++;
      }
      stamps.push(
        <View style={styles.viewRowStamp}>
          {row}
        </View>
      );
    }

    return (
      <Container style={styles.container}>
        <CustomHeaderWithTitle title="Campanha" navigation={navigation} />
        <ScrollView style={styles.scrollView}>
          <View style={styles.viewSectionInfo}>
            <Text style={[styles.fontColor, styles.textTitleInfo, { fontSize: 25 }]}>Informaçoēs da promoção</Text>
          </View>
          <View style={styles.borderLineSection} />
          <View style={{ marginTop: 30 }}>
            <Text style={[styles.fontColor, styles.fontSizeSubtitlesAndDescriptions, styles.textTitleInfo]}>Descrição:</Text>
          </View>
          <View style={styles.viewDescription}>
            <Text style={[styles.fontColor, styles.fontSizeSubtitlesAndDescriptions]}>{this.state.campaign.description}</Text>
          </View>
          <View style={styles.styleRowView}>
            <Text style={[styles.fontColor, styles.fontSizeSubtitlesAndDescriptions, styles.textTitleInfo, { flex: 6 }]}>Validade:</Text>
            <Text style={[styles.fontColor, styles.fontSizeSubtitlesAndDescriptions, { flex: 4 }]}>{moment(Date(this.state.campaign.shelfLife)).format('DD/MM/YYYY')}</Text>
          </View>
          <View style={styles.styleRowView}>
            <Text style={[styles.fontColor, styles.fontSizeSubtitlesAndDescriptions, styles.textTitleInfo, { flex: 6 }]}>Total a acumular:</Text>
            <Text style={[styles.fontColor, styles.fontSizeSubtitlesAndDescriptions, { flex: 4 }]}>{this.state.campaign.totalPointsToWin}</Text>
          </View>
          <View style={styles.styleRowView}>
            <Text style={[styles.fontColor, styles.fontSizeSubtitlesAndDescriptions, styles.textTitleInfo, { flex: 6 }]}>Total acumulado:</Text>
            <Text style={[styles.fontColor, styles.fontSizeSubtitlesAndDescriptions, { flex: 4 }]}>{this.state.campaign.totalPointsGain}</Text>
          </View>
          <View style={styles.viewSectionPoints}>
            <Text style={[styles.fontColor, styles.textTitleInfo, { fontSize: 25 }]}>Pontos</Text>
          </View>
          <View style={styles.borderLineSection} />
          <View style={styles.viewContainerStamps}>
            {stamps}
          </View>
        </ScrollView>
      </Container>
    );
  }
}