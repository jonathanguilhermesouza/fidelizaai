//import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements'
//import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
//import { TextInput } from 'react-native';
//import { SelectMultipleButton } from "../../components/SelectMultipleButton/export";

import React, { Component } from "react";
import { View, Text, TouchableOpacity, Dimensions } from "react-native";
import NumericInput from 'react-native-numeric-input'
import { create, PREDEF_RES } from 'react-native-pixel-perfect'
import { Container, Thumbnail, } from "native-base";

import CustomHeaderWithTitle from "@Component/CustomHeaderWithTitle";
import CommonColor from "@Theme/Variables/CommonColor";
import styles from "./styles";
import _ from "lodash";

const calcSize = create(PREDEF_RES.iphone7.px)

export default class extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      infoUser: this.props.navigation.state.params.infoUser,
      itemSelectMultipleButton: '',
      multipleSelectedData: [],
      multipleSelectedDataLimited: [],
      multipleData: ["Corte masculino degrade", "corte masculino militar", "Barba", "Bigode", "Cerveja Skol"],
      isItemSelectMultipleButtonValid: true,
    }
  }

  _singleTapMultipleSelectedButtons(interest) {
    if (this.state.multipleSelectedData.includes(interest)) {
      _.remove(this.state.multipleSelectedData, ele => {
        return ele === interest;
      });
    } else {
      this.state.multipleSelectedData.push(interest);
    }

    this.setState({
      multipleSelectedData: this.state.multipleSelectedData
    });
  }

  _singleTapMultipleSelectedButtons_limited(interest) {
    if (this.state.multipleSelectedDataLimited.includes(interest)) {
      _.remove(this.state.multipleSelectedDataLimited, ele => {
        return ele === interest;
      });
    } else {
      if (this.state.multipleSelectedDataLimited.length < 3)
        this.state.multipleSelectedDataLimited.push(interest);
    }

    this.setState({
      multipleSelectedDataLimited: this.state.multipleSelectedDataLimited
    });
  }

  validarCampo(value) {
    if (!value) {
      this.setState({
        isItemSelectMultipleButtonValid: false
      });
    } else {
      this.setState({
        isItemSelectMultipleButtonValid: true
      });
    }
  }

  onAddItem() {
    this.validarCampo(this.state.itemSelectMultipleButton);
    if (this.state.itemSelectMultipleButton != '') {
      this.state.multipleData.push(this.state.itemSelectMultipleButton);
      this.setState({
        multipleData: this.state.multipleData,
        itemSelectMultipleButton: ''
      });
    }
  }

  onChangeInputValue(value) {
    this.setState({
      itemSelectMultipleButton: value
    });
    this.validarCampo(value);
  }

  onSave() {
    this.props.navigation.navigate("StampSuccess");
  }

  render() {
    const { navigation } = this.props;
    return (
      <Container style={[styles.container]}>
        <CustomHeaderWithTitle title="Carimbo" navigation={navigation} />
        {/*<View style={[styles.headerInfoUser, { alignItems: 'center' }]}>
            <View style={{ flex: 2 }}>
              <Thumbnail source={{ uri: this.state.infoUser.pathPhoto }} style={[styles.profilePic, { alignContent: 'center' }]} />
            </View>
            <View style={{ flex: 4 }}>
              <Text style={[styles.inverseTextColor, { color: "#1A346D" }]}>{this.state.infoUser.type}</Text>
              <Text style={[styles.inverseTextColor, { fontSize: 20 }]}>{this.state.infoUser.name}</Text>
            </View>
            <View>
              <View style={{ backgroundColor: "#BFFE0C", alignItems: "center", padding: 10, borderRadius: 50 }}>
                <Text style={[styles.inverseTextColor, { fontSize: 30 }]}>{this.state.infoUser.points}</Text>
              </View>
              <Text style={[styles.inverseTextColor, { fontSize: 10, alignSelf: "center", marginTop: 2 }]}>carimbos</Text>
            </View>
    </View>*/}

        <View style={styles.profileInfoContainer}>
          <View style={{ /*alignSelf: "center" */}}>
            <Thumbnail source={{ uri: this.state.infoUser.pathPhoto }} style={styles.profilePic} />
          </View>
          <View style={styles.profileInfo}>
            <Text style={styles.profileUser}>{this.state.infoUser.name}</Text>
            <Text note style={styles.profileUserInfo}>
              {this.state.infoUser.type}
            </Text>
          </View>
        </View>

        <View style={styles.viewSection}>
          <Text style={styles.viewSectionText}>Carimbos do cliente ;)</Text>
          <TouchableOpacity onPress={() => navigation.navigate("Points")} style={styles.buttonPoints}>
            <Text style={styles.buttonPointsText}>{this.state.infoUser.points}</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.viewSection}>
          <Text style={styles.viewSectionText}>Quantos pontos esse cliente merece?</Text>
          <View style={styles.viewButtonAddPoints}>
            <NumericInput rounded initValue={this.state.v8} value={this.state.v8} onChange={(v8) => this.setState({ v8 })}
              totalWidth={calcSize(300)} totalHeight={calcSize(100)} textColor="#585858"
              iconStyle={{ color: CommonColor.getDefaultLightTextColor }} rightButtonBackgroundColor='#00aff0' leftButtonBackgroundColor='#00aff0' />
          </View>

          {/*
        <View style={[styles.containerSelectMultipleButton, {flex:2}]}>
          <Text style={styles.welcome}>Selecione os produtos comprados e servicos realizados</Text>
          {this.state.multipleData.map(interest => (
            <SelectMultipleButton key={interest} buttonViewStyle={styles.buttonViewStyleSelectMultipleButton} textStyle={{ fontSize: 15 }}
              highLightStyle={styles.highLightStyleSelectMultipleButton} value={interest} selected={this.state.multipleSelectedData.includes(interest)}
              singleTap={valueTap => this._singleTapMultipleSelectedButtons(interest)} />
          ))}
        </View>
        <View style={{flex:1}}>
          <FormLabel>Nome do produto ou serviço</FormLabel>
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 5 }}>
              <TextInput placeholder="Informe um produto ou serviço" onChangeText={(text) => this.onChangeInputValue(text)} value={this.state.itemSelectMultipleButton} style={{ margin: 10, paddingBottom: 10, borderBottomColor: '#bebebe', borderBottomWidth: 0.5 }} />
            </View>
            <View style={{ flex: 1, height: 60 }}>
              <TouchableOpacity onPress={() => this.onAddItem()} style={styles.buttonAddItem}>
                <IconFontAwesome size={30} style={styles.iconButtonAdd} name="plus" backgroundColor='transparent'></IconFontAwesome>
              </TouchableOpacity></View>
          </View>
          
          <FormInput value={this.state.itemSelectMultipleButton}  placeholder="Informe um produto ou serviço" />
          <FormValidationMessage>{!this.state.isItemSelectMultipleButtonValid ? "Error message" : ""}</FormValidationMessage>
          
        </View>
        */}
        </View>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <TouchableOpacity onPress={() => this.onSave()} style={[styles.buttonSave, { alignSelf: "flex-end", flex: 1 }]}>
            <Text style={{ fontSize: 20, color: CommonColor.getDefaultLightTextColor }}>Salvar</Text>
          </TouchableOpacity>
        </View>
      </Container>
    );
  }
}

//Exemplo input feito pelo Jonathan
{/*<TextInput placeholder="digite aqui" value={this.state.itemSelectMultipleButton.length} style={{ margin: 10, paddingBottom: 10, borderBottomColor: '#bebebe', borderBottomWidth: 0.5 }} />*/ }