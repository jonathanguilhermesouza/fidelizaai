import CommonColor from "@Theme/Variables/CommonColor";

const React = require("react-native");
const { Dimensions } = React;

const deviceWidth = Dimensions.get("window").width;

export default {
  container: {
    flex: 1,
    width: null,
    height: null,
    backgroundColor: CommonColor.getDefaultLightTextColor
  },
  buttonSave: {
    flex: 1,
    height: 60,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    backgroundColor: '#EA3788',
    alignSelf: "flex-end"
  },
  viewContainer: {
    flex: 9,
    justifyContent: "center",
    alignItems: "center"
  },
  textStyle: {
    color: "#585858",
    fontSize: 20,
    fontWeight: "bold"
  },
  viewIconSuccess: {
    marginTop: 20,
    width: deviceWidth / 2,
    height: deviceWidth / 2,
    borderRadius: 100,
    backgroundColor: "tranparent",
    borderColor: "#00aff0",
    borderWidth: 2,
    justifyContent: "center",
    alignItems: "center",
    color: CommonColor.getDefaultLightTextColor
  },
  viewContainerButtonGoOut: {
    flex: 1,
    flexDirection: "row"
  }
};
