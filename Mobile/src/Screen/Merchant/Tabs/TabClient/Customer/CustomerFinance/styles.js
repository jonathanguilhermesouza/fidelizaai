const React = require("react-native");
const { Platform, Dimensions } = React;

import CommonColor from '@Theme/Variables/CommonColor';

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  viewContainer: {
    flex: 1,
    //justifyContent: "center",
    flexDirection: "column"
  },
  welcomeText: {
    //alignSelf: "center"
  },
  headerDetailCustomer: {
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: CommonColor.getThemeColor,
    height: 120
  },
  imageHeaderProfile: {
    borderRadius: Platform.OS === 'android' ? 55 : 35,
    width: 70,
    height: 70,
    alignSelf: "center"
  },
  viewImageHeaderProfile: {
    flex: 1,
    justifyContent: "center"
  },
  viewDescriptionHeaderProfile: {
    flex: 3,
    flexDirection: "column",
    justifyContent: "center"
  },
  typeUser: {
    fontSize: 15,
    color: '#333'
  },
  textNameUser: {
    color: "#fff",
    fontSize: 18,
    fontWeight: "bold"
  },
  viewInfoCustomer: {
    marginLeft: 20,
    marginRight: 20,
    flex: 1,
  },
  title: {
    fontSize: 15,
    marginTop: 30,
    color: '#262626'
  },
  description: {
    fontSize: 22,
    marginTop: 5,
    color: "#999"
  },
  button: {
    width: 100,
    height: 100,
    padding: 10,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#25D366",
  },
  icon: {
    color: "#fff",
    fontSize: 65,
    backgroundColor: "transparent",
    textAlign: "center",
    marginTop: 5
  },
  viewRow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: 'center'
  },
  descriptionIcon: {
    marginTop: 10, 
    color: "#999"
  }
}