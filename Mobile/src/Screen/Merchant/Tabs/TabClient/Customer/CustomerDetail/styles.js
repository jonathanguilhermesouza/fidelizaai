const React = require("react-native");
const { Platform, Dimensions } = React;

import CommonColor from '@Theme/Variables/CommonColor';

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  viewContainer: {
    flex: 1,
    //justifyContent: "center",
    flexDirection: "column"
  },
  welcomeText: {
    //alignSelf: "center"
  },
  headerDetailCustomer: {
    justifyContent: "center",
    flexDirection: "row",
    backgroundColor: CommonColor.getThemeColor,
    height: 120
  },
  imageHeaderProfile: {
    borderRadius: Platform.OS === 'android' ? 55 : 35,
    width: 70,
    height: 70,
    alignSelf: "center"
  },
  viewImageHeaderProfile: {
    flex: 1,
    justifyContent: "center"
  },
  viewDescriptionHeaderProfile: {
    flex: 3,
    flexDirection: "column",
    justifyContent: "center"
  },
  typeUser: {
    fontSize: 15,
    color: '#333'
  },
  textNameUser: {
    color: "#fff",
    fontSize: 18,
    fontWeight: "bold"
  },
  viewInfoCustomer: {
    marginLeft: 20,
    marginRight: 20,
    flex: 1
  },
  title: {
    fontSize: 15,
    marginTop: 30,
    color: '#262626'
  },
  description: {
    fontSize: 18,
    marginTop: 5,
    color: "#999"
  },
  viewContact: {
    flexDirection: "row",
    marginTop: 40,
    marginBottom: 100,
    justifyContent: 'space-between'
  },
  buttonContact: {
    width: 50,
    height: 50,
    padding: 10,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    //backgroundColor: "#25D366"
    backgroundColor: "#EA3788"
  },
  iconContact: {
    color: "#fff",
    fontSize: 30,
    backgroundColor: "transparent"
  }
}