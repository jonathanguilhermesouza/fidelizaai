//Components
import React from "react";
import { Container, Text, View } from "native-base";
import { Header, Button, Icon, Left, Right, Body, Switch } from 'native-base';
import { StatusBar, TouchableOpacity, FlatList, Image, Modal, Linking } from 'react-native';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import { Searchbar } from 'react-native-paper';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';
import ActionSheet from 'react-native-actionsheet';

//Services
import NavigationService from '@Service/Navigation';
import CustomerService from "@Service/CustomerService";

//Styles
import styles from "./styles";
import GlobalStyle from '@Theme/GlobalStyle';
import CommonColor from '@Theme/Variables/CommonColor';

//const
const avatarGeneric = require("@Asset/images/avatar-generic.jpg");
var CustomerServiceInstance = new CustomerService();

export default class extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            listClientsTemp: [],
            listClientsFull: [],
            showInputSearch: false,
            textInputSearch: '',
            isModalFilterVisible: false,
            orderBy: 0,
            skip: 0
        }
    }

    loadCustomer() {
        let contains = this.state.textInputSearch ? 'and ' + this.state.textInputSearch : '';
        var request = {
            conditionsUrl: "?$select=CardCode, CardName, City, Phone1, Phone2, Country &$filter=CardType ne 'cSupplier' and Valid eq 'tYES' " + contains + "&$orderby=CardName" + "&$skip=" + this.state.skip
        }
        CustomerServiceInstance.list(request)
            .then((data) => {
                let newList = this.state.listClientsFull;
                data.map((item) => {
                    newList.push(item);
                });
                this.setState({
                    listClientsFull: newList,
                    skip: this.state.skip + 20
                }, () => { this.setState({ listClientsTemp: this.state.listClientsFull }) });
                console.log(data);
            })
            .catch((error) => {
                console.log(error);
            });
    }

    //#region ComponentDidMount
    componentDidMount() {
        this._sub = this.props.navigation.addListener('didFocus', () => (
            this.setState({
                listClientsTemp: [],
                listClientsFull: [],
                textInputSearch: '',
                skip: 0
            },
                () => this.loadCustomer())
        ))
    }
    //#endregion

    onToggleInputSearch() {
        let booleanToogle = this.state.showInputSearch ? false : true;
        this.setState({
            showInputSearch: booleanToogle
        });
    }

    onFilterListClients(text) {
        let newListClients = this.state.listClientsFull.filter(function (item) {
            let cardName = item.CardName.toUpperCase();
            let city = item.City ? item.City.toUpperCase() : '';
            //let phoneNumber = item.phoneNumber ? item.phoneNumber.replace(/[^0-9]/g, "") : '';
            let itemText = text.replace(/[^a-zA-Z0-9]/g, "").toUpperCase();
            return cardName.indexOf(itemText) > - 1 || city.indexOf(itemText) > - 1
        });

        this.setState({
            listClientsTemp: newListClients
        })
    }

    onSearchByText(text) {
        this.setState({
            textInputSearch: text
        }, () => {
            this.onFilterListClients(this.state.textInputSearch);
        })
    }

    onOrderList() {
        let listOrdened = [];

        this.setState({
            listClientsTemp: [],
        }, () => {

            if (this.state.orderBy == 0)
                listOrdened = this.state.listClientsFull.sort((a, b) => a.CardCode.toString() > b.CardCode.toString())
            else if (this.state.orderBy == 1)
                listOrdened = this.state.listClientsFull.sort((a, b) => a.City.toString() > b.City.toString())

            this.setState({
                listClientsTemp: listOrdened,
                isModalFilterVisible: false
            });

        });
    }

    onOpenWhatsApp(phone) {
        let link = 'https://api.whatsapp.com/send?1=pt_BR&phone=55' + phone;

        if (link) {
            Linking.canOpenURL(link)
                .then(supported => {
                    if (!supported) {
                        Alert.alert(
                            'Por favor, instale o whatsApp caso queira enviar mensagens'
                        );
                    } else {
                        return Linking.openURL(link);
                    }
                })
                .catch(err => console.error('An error occurred', err));
        } else {
            console.log('sendWhatsAppMessage -----> ', 'message link is undefined');
        }
    }

    onGoToCustomer(cardCode) {
        NavigationService.navigate("Customer", { cardCode: cardCode });
    }

    onGoToCustomerCreate() {
        NavigationService.navigate("CustomerCreate");
    }

    selectActionSheet(option) {
        if (option == 1) {
            this.setState({ isModalFilterVisible: true })
        }
    }

    renderCustomerRow(customer) {
        return (
            <TouchableOpacity onPress={() => this.onGoToCustomer(customer.CardCode)}>
                <View style={styles.listItemContainer}>
                    <View style={styles.iconContainer}>
                        <Image source={customer.avatar ? { uri: customer.avatar } : avatarGeneric} style={styles.initStyle} resizeMode='contain' />
                    </View>
                    <View style={styles.callerDetailsContainer}>
                        <View style={styles.callerDetailsContainerWrap}>
                            <View style={[styles.nameContainer, { flex: 3 }]}>
                                <Text style={{ fontSize: 12 }}>{customer.firstName} {customer.lastName}</Text>
                                <View style={styles.subtitleContainer}>
                                    <Text style={styles.subtitleFont}>Email: {customer.email}</Text>
                                </View>
                            </View>
                            <View style={[styles.callIconContainer]}>
                                <TouchableOpacity onPress={() => this.onOpenWhatsApp(customer.phoneNumber)} style={styles.btnWhatsApp}>
                                    <IconFontAwesome name={"whatsapp"} style={styles.iconWhatsApp}>
                                    </IconFontAwesome>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {

        const listOptionsOrderBy = [
            { label: 'Nome', value: 0 },
            { label: 'Cidade', value: 1 }
        ];

        return (
            <Container>

                <Modal visible={this.state.isModalFilterVisible}>
                    <Container style={{ backgroundColor: "#fff" }}>
                        <StatusBar barStyle="dark-content" />
                        <View style={styles.viewModalContainer}>
                            <View style={{ flexDirection: "row" }}>
                                <Left style={styles.headerLeft}>
                                    <Text style={styles.headerLeftText}>ORDENAÇÃO</Text>
                                </Left>
                                <Body style={{ flex: 1 }}></Body>
                                <Right style={styles.headerRight}>
                                    <TouchableOpacity onPress={() => this.setState({ isModalFilterVisible: false })}>
                                        <Text style={styles.btnClose}>Fechar</Text>
                                    </TouchableOpacity>
                                </Right>
                            </View>
                            <View style={styles.viewDescription}>
                                <Text style={styles.viewDescriptionText}>Escolha uma das opçoēs para ordenar</Text>
                            </View>
                            <View style={{ flex: 1 }}>
                                <RadioForm
                                    labelStyle={styles.labelStyleRadioForm}
                                    buttonSize={30}
                                    style={{ padding: 10 }}
                                    radio_props={listOptionsOrderBy}
                                    initial={this.state.orderBy}
                                    formHorizontal={false}
                                    labelHorizontal={true}
                                    buttonColor={CommonColor.getThemeColor}
                                    selectedButtonColor={CommonColor.getThemeColor}
                                    animation={true}
                                    onPress={(value) => { this.setState({ orderBy: value }) }}
                                />
                            </View>
                            <View style={styles.viewBtnSave}>
                                <TouchableOpacity onPress={() => this.onOrderList()} style={styles.btnSaveTouchableOpacity}>
                                    <Text style={styles.btnSave}>Ordenar</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </Container>
                </Modal>

                <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs}>
                    <StatusBar barStyle="light-content" backgroundColor="#999" />
                    <Left style={{ flex: 1 }}>
                        <Button transparent onPress={() => NavigationService.openDrawer()}>
                            <Icon active name='menu' style={GlobalStyle.textWhite} type="MaterialCommunityIcons" />
                        </Button>
                    </Left>
                    <Body style={{ flex: 3 }}>
                        <Text style={[GlobalStyle.actionBarText, { alignSelf: 'center' }]}>{'clientes'.toUpperCase()}</Text>
                    </Body>
                    <Right style={{ flex: 1 }}>
                        <Button transparent onPress={() => this.onToggleInputSearch()}>
                            <IconFontAwesome name={"search"} color={"#fff"} size={25} />
                        </Button>
                        <Button transparent onPress={() => this.setState({ isModalFilterVisible: true })}>
                            <IconFontAwesome name={"filter"} color={"#fff"} size={25} />
                        </Button>
                        <Button transparent onPress={() => this.onGoToCustomerCreate()}>
                            <IconFontAwesome name={"plus"} color={"#fff"} size={25} />
                            {/*<Icon active name='dots-vertical' style={{ color: "#fff" }} type="MaterialCommunityIcons" />*/}
                        </Button>
                    </Right>
                </Header>
                <ActionSheet
                    ref={o => this.ActionSheet = o}
                    title={'O que deseja fazer ?'}
                    options={['Adicionar cliente', 'Ordenar lista', 'Cancelar']}
                    cancelButtonIndex={2}
                    //destructiveButtonIndex={1}
                    onPress={(index) => { this.selectActionSheet(index) }}
                />
                {this.state.showInputSearch ?
                    <Searchbar
                        placeholder="Procurar"
                        onChangeText={text => { this.onSearchByText(text) }}
                        value={this.state.textInputSearch}
                        blur
                    /> : null}
                <View style={styles.viewContainer}>
                    <FlatList
                        keyboardShouldPersistTaps={'handled'}
                        data={this.state.listClientsTemp}
                        extraData={this.state.orderBy}
                        renderItem={(customer) => this.renderCustomerRow(customer.item)}
                        keyExtractor={item => item.id.toString()}
                        onEndReachedThreshold={0.5}
                        onEndReached={() => {
                            this.loadCustomer();
                        }}
                    />
                </View>
            </Container>
        )
    }
}