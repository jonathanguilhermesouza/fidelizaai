import CommonColor from '@Theme/Variables/CommonColor';

const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

export default {
  viewContainer: {
    flex: 1,
    marginTop: 2,
    backgroundColor: "#fff"
  },
  listItemContainer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    padding: 10
  },
  iconContainer: {
    flex: 1,
    alignItems: "flex-start"
  },
  callerDetailsContainer: {
    flex: 4,
    justifyContent: "center",
    borderBottomColor: "rgba(92,94,94,0.5)",
    borderBottomWidth: 0.25
  },
  callerDetailsContainerWrap: {
    flex: 1,
    alignItems: "center",
    flexDirection: "row"
  },
  nameContainer: {
    alignItems: "flex-start",
    flex: 1
  },
  subtitleContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 5
  },
  callIconContainer: {
    flex: 1,
    alignItems: "flex-end"
  },
  initStyle: {
    borderRadius: Platform.OS === 'android' ? 50 : 30,
    width: 60,
    height: 60
  },
  btnWhatsApp: {
    width: 50,
    height: 50,
    padding: 10,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#25D366"
  },
  subtitleFont: {
    fontWeight: '400',
    color: '#666',
    fontSize: 11
  },
  iconWhatsApp: {
    color: "#fff",
    fontSize: 33,
    backgroundColor: "transparent"
  },
  //Modal OrderBy
  viewModalContainer: {
    flex: 1,
    marginTop: 2,
    backgroundColor: "#fff",
    marginTop: 40
  },
  headerLeft: {
    flex: 2,
    padding: 10,
    marginTop: 10
  },
  headerRight: {
    flex: 1,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  btnClose: {
    fontSize: 20,
    color: CommonColor.getThemeColor
  },
  headerLeftText: {
    fontSize: 20,
    fontWeight: "bold",
    color: CommonColor.defaultTextColor
  },
  viewDescription: {
    flexDirection: "row",
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10
  },
  viewDescriptionText: {
    fontSize: 20,
    color: CommonColor.defaultTextColor
  },
  labelStyleRadioForm: {
    fontSize: 18,
    color: CommonColor.defaultTextColor
  },
  viewBtnSave: {
    flex: 1,
    flexDirection: "row"
  },
  btnSave: {
    fontSize: 20,
    color: "#fff"
  },
  btnSaveTouchableOpacity: {
    flex: 1,
    height: 60,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    backgroundColor: '#25D366',
    alignSelf: "flex-end"
  }
}