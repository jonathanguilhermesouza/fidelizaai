import 'reflect-metadata';
import {createConnection} from "typeorm";

createConnection({
    type: 'react-native',
    database: 'test',
    location: 'default',
    logging: ['error', 'query', 'schema'],
    synchronize: true,
    entities: [
    ],
    logging: false
}).then(connection => {
    // here you can start to work with your entities
    alert('banco conectado');
}).catch(error => alert(error));