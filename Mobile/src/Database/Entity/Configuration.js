import { Entity, OneToOne, JoinColumn, PrimaryGeneratedColumn } from "typeorm";
import { BusinessPlace } from "@Database/Entity/BusinessPlace";

@Entity()
export class Configuration {

    @PrimaryGeneratedColumn()
    id: number;

    @OneToOne(type => BusinessPlace)
    @JoinColumn()
    businessPlace: BusinessPlace;
}