import { Entity, Column, OneToMany, OneToOne, JoinColumn, PrimaryGeneratedColumn } from "typeorm";
import { Synchronization } from "@Database/Entity/Synchronization";
import { Configuration } from "@Database/Entity/Configuration";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column("varchar", {
        length: 100
    })
    name: string;

    @Column("varchar", {
        length: 100
    })
    username: string;

    @Column("varchar", {
        length: 100
    })
    password: string;

    @OneToOne(type => Configuration)
    @JoinColumn()
    configuration: Configuration;

    @OneToMany(type => Synchronization, synchronization => synchronization.user)
    synchronizations: Synchronization[];
}