import React, { Component } from "react";
import { Image } from "react-native";
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge,
  View,
} from "native-base";
import styles from "./Style";


class Footer extends Component {
  constructor(props) {
    super(props);
    
  }

 
  render() {
    return (
      <Container>
        <Content
        >
         <Footer style={Style.greyTopLine}>
            <FooterTab style={Style.bgBot}>
                <Button style={Style.bgBot} onPress={() => {
                    NavigationService.navigate('PublicHome')
                }}>
                    <Icon name="home" type="FontAwesome" style={Style.textBlue} />
                </Button>
                <Button style={Style.bgBot} onPress={() => {
                    NavigationService.navigate('PublicPropertySearch')
                }}>
                    <Icon name='search' type="FontAwesome" style={Style.textBlue} />
                </Button>
                
                <Button style={Style.bgBot} onPress={() => {
                    NavigationService.navigate('MemberFavorites')
                }}>
                    <Icon name="heart" type="FontAwesome" style={Style.textBlue} />
                </Button>
                <Button style={Style.bgBot} onPress={() => {
                    NavigationService.navigate('MemberHome')
                }}>
                    <Icon name="user" type="FontAwesome" style={Style.textBlue} />
                </Button>
                
            </FooterTab>
            </Footer>

        </Content>
      </Container>
    );
  }
}

export default Footer;

