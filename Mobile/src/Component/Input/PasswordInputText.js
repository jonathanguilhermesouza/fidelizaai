import React from 'react';
import { Text, View, StyleSheet, Platform } from 'react-native';
import { TextField } from 'react-native-material-textfield';
import CommonColor from '@Theme/Variables/CommonColor';
import Icon from 'react-native-vector-icons/MaterialIcons';

const PasswordInputText = ({
    secureTextEntry,
    textColor,
    autoCapitalize,
    tintColor,
    baseColor,
    name,
    label,        // field name - required
    customStyle,
    onChangeText,   // event
    value,          // field value
    disabled,
    placeholder,
    onPress,
    nameIcon,
    iconColor,
    errors,         // this array prop is automatically passed down to this component from <Form />
}) => {



    return (
        <View>

            <TextField
                autoCapitalize={autoCapitalize}
                textColor={textColor}
                tintColor={tintColor}
                baseColor={baseColor}
                label={label}
                value={value && value}
                secureTextEntry={secureTextEntry}
                placeholder={placeholder ? placeholder : ""}
                disabled={disabled}
                onChangeText={onChangeText ? (val) => onChangeText(val) : null}
                style={customStyle ? customStyle : {}}
            />

            {errors && errors.length > 0 && errors.map((item, index) =>
                item.field === name && item.error ?
                    <Text style={styles.formErrorText}>
                        {item.error}
                    </Text>
                    : <View />
            )
            }

            <Icon style={styles.icon}
                name={nameIcon && nameIcon}
                onPress={onPress ? (val) => onPress(val) : null}
                //onPress={this.changePwdType}
                size={25}
                color={iconColor}

            />
        </View>
    );
}

export default PasswordInputText;

const styles = StyleSheet.create({
    formErrorText: {
        fontSize: Platform.OS === "android" ? 12 : 15,
        color: CommonColor.getDangerColor,
        textAlign: "right",
        top: -10
    },
    icon: {
        position: 'absolute',
        top: 33,
        right: 0
    }
});

PasswordInputText.defaultProps = {
    iconSize: 25,
}