import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
    View,
    StyleSheet
} from 'react-native';
import {
    TextField
} from 'react-native-material-textfield';
import { Field } from 'react-native-validate-form';

export default class PasswordInputTextOLD extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            icEye: 'visibility-off',
            isPasswordInvisible: true
        }
    }

    changePwdType = () => {
        let newState;
        if (this.state.isPasswordInvisible) {
            newState = {
                icEye: 'visibility',
                isPasswordInvisible: false
            }
        } else {
            newState = {
                icEye: 'visibility-off',
                isPasswordInvisible: true
            }
        }

        // set new state value
        this.setState(newState)

    };

    render() {
        return (
            <View>
                {/* Padrão */}
                {/*<TextField {...this.props}
                           secureTextEntry={this.state.isPasswordInvisible}/>*/}

                

            

                    <TextField
                        autoCapitalize={this.props.autoCapitalize}
                        textColor={this.props.textColor}
                        tintColor={this.props.tintColor}
                        baseColor={this.props.baseColor}
                        label={this.props.label}
                        value={this.props.value && this.props.value}
                        secureTextEntry={this.props.secureTextEntry}
                        placeholder={this.props.placeholder ? this.props.placeholder : ""}
                        disabled={this.props.disabled}
                        onChangeText={this.props.onChangeText}
                        style={this.props.customStyle ? this.props.customStyle : {}}
                    />

                    {this.props.errors && this.props.errors.length > 0 && this.props.errors.map((item, index) =>
                        item.field === this.props.name && item.error ?
                            <Text style={styles.formErrorText}>
                                {item.error}
                            </Text>
                            : <View />
                    )
                    }
             


                <Icon style={styles.icon}
                    name={this.state.icEye}
                    size={this.props.iconSize}
                    color={this.props.iconColor}
                    onPress={this.changePwdType}
                />
            </View>
        );
    }
}


export const styles = StyleSheet.create({

    icon: {
        position: 'absolute',
        top: 33,
        right: 0
    }

});

PasswordInputTextOLD.defaultProps = {
    iconSize: 25,
}