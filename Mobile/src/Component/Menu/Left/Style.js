const React = require("react-native");
const { Platform, Dimensions } = React;

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

import CommonColor from '@Theme/Variables/CommonColor';

export default {
  drawerCover: {
    flex: 1,
    height: deviceHeight / 3.5,
    justifyContent: "center",
    backgroundColor: CommonColor.getThemeColor
  },
  drawerImage: {
    alignSelf: "center", 
    resizeMode: "cover",
    borderRadius: Platform.OS === 'android' ? 50 : 30,
    width: 60,
    height: 60
  },
  drawerText: {
    alignSelf: "center", 
    marginTop: 20,
    fontSize: 16,
    color: '#FFF',
    fontFamily: 'Montserrat-SemiBold',
  },
  text: {
    fontWeight: Platform.OS === "ios" ? "500" : "400",
    fontSize: 12,
    marginLeft: 20,
    color: '#333',
    fontFamily: 'Montserrat-Regular',
  },
  badgeText: {
    fontSize: Platform.OS === "ios" ? 13 : 11,
    fontWeight: "400",
    textAlign: "center",
    marginTop: Platform.OS === "android" ? -3 : undefined,
    justifyContent: "center",
  },
  divider: {
    borderBottomWidth: 1,
    borderColor: '#CCC',
    paddingBottom: 20,
    marginBottom: 20,
  },
  
};
