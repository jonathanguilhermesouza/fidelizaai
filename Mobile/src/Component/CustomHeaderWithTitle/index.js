// @flow
import React, { Component } from "react";
import { Text, Platform } from "react-native";
import { Icon, Button, Left, Right, Body, Header } from "native-base";

import GlobalStyle from '@Theme/GlobalStyle';

class CustomHeaderWithTitle extends Component {
  render() {
    const navigation = this.props.navigation;
    return (
      <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs} >
        <Left style={{flex: 1}}>
          {navigation ?
            <Button transparent onPress={() => navigation.goBack()}>
              <Icon active name="arrow-back" />
            </Button> 
            : null
          }

        </Left>
        <Body style={{flex: 3}}>
          <Text style={{alignSelf: 'center' , color: "#fff", fontSize: 20, paddingTop: 10, marginTop: Platform.OS === "android" ? -10 : 0 }}>{this.props.title}</Text>
        </Body>
        <Right style={{flex: 1}}/>
      </Header>
    );
  }
}

export default CustomHeaderWithTitle;
