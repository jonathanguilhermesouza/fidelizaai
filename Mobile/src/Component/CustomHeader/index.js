// @flow
import React, { Component } from "react";
import { Image, Text } from "react-native";
import { Icon, Button, Left, Right, Body, Header } from "native-base";

import styles from "./styles";
import GlobalStyle from '@Theme/GlobalStyle';

const headerLogo = require("@Asset/images/logo-light.png");

class CustomHeader extends Component {
  render() {
    const navigation = this.props.navigation;
    return (
      <Header style={GlobalStyle.navigation} hasTabs={this.props.hasTabs} >
        <Left style={{ flex: 1 }}>
          <Button transparent onPress={() => navigation.openDrawer()}>
            <Icon active name="menu" />
          </Button>
        </Left>
        <Body style={{ flex: 3 }}>
          {/*<Image source={headerLogo} style={[styles.imageHeader, { alignSelf: 'center' }]} />*/}
          <Text style={styles.titleText}>FidelizaAí</Text>
        </Body>
        <Right  style={{ flex: 1 }}/>
      </Header>
    );
  }
}

export default CustomHeader;
