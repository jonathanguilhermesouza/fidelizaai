//Packages
import React from 'react'
import { Dimensions, Animated, Easing, NativeModules } from 'react-native'
import { Root } from "native-base";
import { createDrawerNavigator } from "react-navigation-drawer"
import { createStackNavigator } from "react-navigation-stack"
import { createAppContainer } from "react-navigation"
//import Loader from 'react-native-mask-loader';
import SplashScreen from 'react-native-splash-screen'
import { createStore } from 'redux';
import { Provider } from 'react-redux';
//import os from 'react-native-os';

//Components
import DrawerContent from '@Component/Menu/Left'
import InitialScreen from '@Screen/InitialScreen'
import allReducers from '@Reducer/index.js';
import NavigationService from '@Service/Navigation';
import ConfigurationView from '@Screen/Configuration';
import BusinessPlaceView from '@Screen/BusinessPlaceView';
import Dashboard from '@Screen/Dashboard';
import Login from '@Screen/Login';
import SignUp from '@Screen/SignUp';
import ForgotPassword from '@Screen/ForgotPassword';

import ScanScreen from '@Screen/Merchant/Tabs/TabStamp/ScanScreen';
import Stamp from '@Screen/Merchant/Tabs/TabStamp/Stamp';
import Points from '@Screen/Merchant/Tabs/TabStamp/Points';
import StampSuccess from '@Screen/Merchant/Tabs/TabStamp/StampSuccess';
import Customer from '@Screen/Merchant/Tabs/TabClient/Customer/router';
import CustomerDetail from '@Screen/Merchant/Tabs/TabClient/Customer/CustomerDetail';
import CustomerFinance from '@Screen/Merchant/Tabs/TabClient/Customer/CustomerFinance';
import CustomerHistory from '@Screen/Merchant/Tabs/TabClient/Customer/CustomerHistory';

import MerchantRouter from '@Screen/User/Tabs/TabMarketplace/Merchant/router';
import MerchantDetail from '@Screen/User/Tabs/TabMarketplace/Merchant/MerchantDetail';
import MerchantFinance from '@Screen/User/Tabs/TabMarketplace/Merchant/MerchantFinance';
import MerchantHistory from '@Screen/User/Tabs/TabMarketplace/Merchant/MerchantHistory';

import TabsMerchant from '@Screen/Merchant/Tabs/router';
import TabClient from '@Screen/Merchant/Tabs/TabClient';
import TabStamp from '@Screen/Merchant/Tabs/TabStamp';
import TabWarning from '@Screen/Merchant/Tabs/TabWarning';

import TabsUser from '@Screen/User/Tabs/router';
import TabMarketplace from '@Screen/User/Tabs/TabMarketplace';
import TabQrCode from '@Screen/User/Tabs/TabQrCode';
import TabUserWarning from '@Screen/User/Tabs/TabUserWarning';

//import '@Database/index';
import 'reflect-metadata';
import { createConnection, getRepository } from "typeorm";
import { Cliente } from '@Database/Entity/Cliente';
import { User } from '@Database/Entity/User';
import { Configuration } from '@Database/Entity/Configuration';
import { BusinessPlace } from '@Database/Entity/BusinessPlace';
import { Synchronization } from '@Database/Entity/Synchronization';

//const path = require("path");
//const path = os.homedir();
//var dirName = require('NativeModules').DirName;
const store = createStore(allReducers);
const deviceWidth = Dimensions.get("window").width;

const Drawer = createDrawerNavigator(
  {
    TabsUser: {
      screen: TabsUser
    },
    TabsMerchant: {
      screen: TabsMerchant
    },
    MerchantRouter: {
      screen: MerchantRouter
    },
    Customer: {
      screen: Customer
    },
    ConfigurationView: {
      screen: ConfigurationView
    },
    BusinessPlaceView: {
      screen: BusinessPlaceView
    },
    Dashboard: {
      screen: Dashboard
    }
  },
  {
    contentComponent: DrawerContent,
    contentOptions: {
      activeTintColor: "#e91e63"
    },
    headerMode: "none",
    initialRouteName: "Dashboard",
    drawerWidth: deviceWidth - 50
  }
)

const AppNav = createStackNavigator(
  {
    TabMarketplace: {
      screen: TabMarketplace
    },
    TabQrCode: {
      screen: TabQrCode
    },
    TabUserWarning: {
      screen: TabUserWarning
    },
    TabClient: {
      screen: TabClient
    },
    TabWarning: {
      screen: TabWarning
    },
    TabStamp: {
      screen: TabStamp
    },
    Stamp: {
      screen: Stamp
    },
    Points: {
      screen: Points
    },
    Points: {
      screen: Points
    },
    StampSuccess: {
      screen: StampSuccess
    },
    CustomerDetail: {
      screen: CustomerDetail
    },
    CustomerFinance: {
      screen: CustomerFinance
    },
    CustomerHistory: {
      screen: CustomerHistory
    },
    Customer: {
      screen: Customer
    },
    ConfigurationView: {
      screen: ConfigurationView
    },
    BusinessPlaceView: {
      screen: BusinessPlaceView
    },
    Login: {
      screen: Login
    },
    SignUp: {
      screen: SignUp
    },
    ForgotPassword: {
      screen: ForgotPassword
    },
    InitialScreen: {
      screen: InitialScreen
    },
    ScanScreen: {
      screen: ScanScreen
    },
    Drawer: {
      screen: Drawer
    },
    MerchantDetail: {
      screen: MerchantDetail
    },
    MerchantFinance: {
      screen: MerchantFinance
    },
    MerchantHistory: {
      screen: MerchantHistory
    },
  },
  {
    headerMode: "none",
    initialRouteName: 'InitialScreen'/*'InitialScreen'*/,
    //Remove transicao de tela, efeitos
    transitionConfig : () => ({
      transitionSpec: {
        duration: 0,
        timing: Animated.timing,
        easing: Easing.step0,
      },
    }),
  }
)
console.disableYellowBox = true;

const AppContainer = createAppContainer(AppNav);

export default class App extends React.Component {
  state = {
    appReady: false,
    rootKey: Math.random(),
  };

  constructor() {
    super();
    //alert(path);
  }

  componentDidMount() {
    SplashScreen.hide();
    this.resetAnimation();
  }

  resetAnimation() {
    this.setState({
      appReady: false,
      rootKey: Math.random()
    });

    setTimeout(() => {
      this.setState({
        appReady: true,
      });
    }, 2000);
  }
  render() {
    return (
      <Provider store={store}>
        <Root>
          {/*<Loader
          isLoaded={this.state.appReady}
          imageSource={require('./assets/images/music_6x.png')}
          backgroundStyle={{ backgroundColor: "#DF3A62" }}
        >
          <AppContainer
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
        </Loader>
        */}
          <AppContainer
            ref={navigatorRef => {
              NavigationService.setTopLevelNavigator(navigatorRef);
            }}
          />
        </Root>
      </Provider>
    );
  }
}