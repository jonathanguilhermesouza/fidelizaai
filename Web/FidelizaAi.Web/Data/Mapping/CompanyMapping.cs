﻿using FidelizaAi.Web.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FidelizaAi.Web.Data.Mapping
{
    public class CompanyMapping : IEntityTypeConfiguration<Company>
    {
        public CompanyMapping()
        {

        }
        public void Configure(EntityTypeBuilder<Company> builder)
        {
            builder.HasKey(e => e.Cnpj);

            builder.ToTable("Company", "BKO");

            builder.Property(e => e.Cnpj)
                .HasMaxLength(14)
                .ValueGeneratedNever();

            builder.Property(e => e.Bairro).HasMaxLength(20);

            builder.Property(e => e.Cep).HasMaxLength(8);

            builder.Property(e => e.CnpjMatriz).HasMaxLength(14);

            builder.Property(e => e.Complemento).HasMaxLength(20);

            builder.Property(e => e.CorporateName).HasMaxLength(150);

            builder.Property(e => e.Email).HasMaxLength(100);

            builder.Property(e => e.FantasyName).HasMaxLength(100);

            builder.Property(e => e.Localidade).HasMaxLength(40);

            builder.Property(e => e.Logradouro).HasMaxLength(30);

            builder.Property(e => e.Number).HasMaxLength(10);

            builder.Property(e => e.PhoneNumber1).HasMaxLength(20);

            builder.Property(e => e.PhoneNumber2).HasMaxLength(20);

            builder.Property(e => e.Uf).HasMaxLength(2);

            builder.Property(e => e.WhatsApp).HasMaxLength(20);
            
            builder.Property(e => e.WebSite).HasMaxLength(50);

            builder.Property(e => e.Facebook).HasMaxLength(50);

            builder.Property(e => e.Instagram).HasMaxLength(50);

            builder.Property(e => e.IconPath).HasMaxLength(100);

            builder.HasOne(d => d.CompanySize)
                .WithMany(p => p.Company)
                .HasForeignKey(d => d.CompanySizeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Company_CompanySize");

            builder.HasOne(d => d.CompanyType)
                .WithMany(p => p.Company)
                .HasForeignKey(d => d.CompanyTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Company_CompanyType");

            builder.HasOne(d => d.Subscriber)
                .WithMany(p => p.Company)
                .HasForeignKey(d => d.SubscriberId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Company_Subscriber");
        }
    }
}