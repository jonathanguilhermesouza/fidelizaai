﻿using FidelizaAi.Web.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FidelizaAi.Web.Data.Mapping
{
    public class StampRegisterMapping : IEntityTypeConfiguration<StampRegister>
    {
        public void Configure(EntityTypeBuilder<StampRegister> builder)
        {
            builder.ToTable("StampRegister", "APP");

            builder.Property(e => e.Description).HasMaxLength(100);

            builder.Property(e => e.UserId)
                .IsRequired()
                .HasMaxLength(450);

            builder.HasOne(d => d.LoyaltyCampaingn)
                .WithMany(p => p.StampRegister)
                .HasForeignKey(d => d.LoyaltyCampaingnId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_StampRegister_LoyaltyCampaign");

            builder.HasOne(d => d.User)
                .WithMany(p => p.StampRegister)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UserLoyaltyCampaingn_User");
        }
    }
}
