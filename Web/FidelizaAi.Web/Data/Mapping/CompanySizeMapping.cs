﻿using FidelizaAi.Web.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FidelizaAi.Web.Data.Mapping
{
    public class CompanySizeMapping : IEntityTypeConfiguration<CompanySize>
    {
        public void Configure(EntityTypeBuilder<CompanySize> builder)
        {
            builder.ToTable("CompanySize", "SIS");

            builder.Property(e => e.Description).HasMaxLength(100);

            builder.Property(e => e.Name).HasMaxLength(20);
        }
    }
}
