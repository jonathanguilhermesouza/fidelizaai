﻿using FidelizaAi.Web.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FidelizaAi.Web.Data.Mapping
{
    public class NotificationTypeMapping : IEntityTypeConfiguration<NotificationType>
    {
        public void Configure(EntityTypeBuilder<NotificationType> builder)
        {
            builder.ToTable("NotificationType", "SIS");

            builder.Property(e => e.ColorHexa).HasMaxLength(7);

            builder.Property(e => e.Descrpition).HasMaxLength(100);

            builder.Property(e => e.Name).HasMaxLength(20);
        }
    }
}
