﻿using FidelizaAi.Web.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FidelizaAi.Web.Data.Mapping
{
    public class UserMapping : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {

            builder.ToTable("User", "ACCOUNT");

            builder.Property(e => e.Bairro).HasMaxLength(20);

            builder.Property(e => e.Cep).HasMaxLength(8);

            builder.Property(e => e.Complemento).HasMaxLength(20);

            builder.Property(e => e.Cpf).HasMaxLength(11);

            builder.Property(e => e.Email).HasMaxLength(100);

            builder.Property(e => e.LastName).HasMaxLength(100);

            builder.Property(e => e.Localidade).HasMaxLength(40);

            builder.Property(e => e.Logradouro).HasMaxLength(30);

            builder.Property(e => e.Name).HasMaxLength(20);

            builder.Property(e => e.PhoneNumber1).HasMaxLength(20);

            builder.Property(e => e.PhoneNumber2).HasMaxLength(20);

            builder.Property(e => e.IconPath).HasMaxLength(100);

            builder.Property(e => e.Uf)
                    .HasMaxLength(2)
                    .IsFixedLength();

            builder.Property(e => e.WhatsApp).HasMaxLength(20);

            builder.HasOne(d => d.Profile)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_Profile");
        }
    }
}
