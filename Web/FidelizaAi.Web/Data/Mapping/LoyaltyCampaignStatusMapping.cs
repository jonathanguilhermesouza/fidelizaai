﻿using FidelizaAi.Web.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FidelizaAi.Web.Data.Mapping
{
    public class LoyaltyCampaignStatusMapping : IEntityTypeConfiguration<LoyaltyCampaignStatus>
    {
        public void Configure(EntityTypeBuilder<LoyaltyCampaignStatus> builder)
        {
            builder.ToTable("LoyaltyCampaignStatus", "SIS");

            builder.Property(e => e.Description).HasMaxLength(100);

            builder.Property(e => e.Name).HasMaxLength(20);
        }
    }
}
