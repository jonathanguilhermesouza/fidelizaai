﻿using FidelizaAi.Web.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FidelizaAi.Web.Data.Mapping
{
    public class SubscriberMapping : IEntityTypeConfiguration<Subscriber>
    {
        public void Configure(EntityTypeBuilder<Subscriber> builder)
        {

            builder.ToTable("Subscriber", "BKO");

            builder.Property(e => e.Document).HasMaxLength(14);

            builder.Property(e => e.Name).HasMaxLength(20);

            builder.Property(e => e.OwnerId)
                    .IsRequired()
                    .HasMaxLength(450);

            builder.HasOne(d => d.Owner)
                    .WithMany(p => p.Subscriber)
                    .HasForeignKey(d => d.OwnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Subscriber_User");

            builder.HasOne(d => d.Plan)
                    .WithMany(p => p.Subscriber)
                    .HasForeignKey(d => d.PlanId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Subscriber_Plan");
        }
    }
}
