﻿using FidelizaAi.Web.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FidelizaAi.Web.Data.Mapping
{
    public class NotificationMapping : IEntityTypeConfiguration<Notification>
    {
        public void Configure(EntityTypeBuilder<Notification> builder)
        {
            builder.ToTable("Notification", "APP");

            builder.Property(e => e.CompanyCnpj)
                .IsRequired()
                .HasMaxLength(14);

            builder.Property(e => e.Description).HasMaxLength(100);

            builder.Property(e => e.Title).HasMaxLength(20);

            builder.Property(e => e.UserId)
                .IsRequired()
                .HasMaxLength(450);

            builder.HasOne(d => d.CompanyCnpjNavigation)
                .WithMany(p => p.Notification)
                .HasForeignKey(d => d.CompanyCnpj)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Notification_Company");

            builder.HasOne(d => d.NotificationType)
                .WithMany(p => p.Notification)
                .HasForeignKey(d => d.NotificationTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Notification_NotificationType");

            builder.HasOne(d => d.User)
                .WithMany(p => p.Notification)
                .HasForeignKey(d => d.UserId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Notification_User");
        }
    }
}
