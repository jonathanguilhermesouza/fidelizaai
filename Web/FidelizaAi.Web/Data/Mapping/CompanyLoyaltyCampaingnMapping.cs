﻿using FidelizaAi.Web.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace FidelizaAi.Web.Data.Mapping
{
    public class CompanyLoyaltyCampaingnMapping : IEntityTypeConfiguration<CompanyLoyaltyCampaingn>
    {
        public void Configure(EntityTypeBuilder<CompanyLoyaltyCampaingn> builder)
        {
            builder.HasKey(e => new { e.LoyaltyCampaingnId, e.CompanyCnpj });

            builder.ToTable("CompanyLoyaltyCampaingn", "APP");

            builder.Property(e => e.CompanyCnpj).HasMaxLength(14);

            builder.HasOne(d => d.CompanyCnpjNavigation)
                    .WithMany(p => p.CompanyLoyaltyCampaingn)
                    .HasForeignKey(d => d.CompanyCnpj)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CompanyLoyaltyCampaingn_Company");

            builder.HasOne(d => d.LoyaltyCampaingn)
                    .WithMany(p => p.CompanyLoyaltyCampaingn)
                    .HasForeignKey(d => d.LoyaltyCampaingnId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CompanyLoyaltyCampaingn_LoyaltyCampaign");
        }
    }
}
