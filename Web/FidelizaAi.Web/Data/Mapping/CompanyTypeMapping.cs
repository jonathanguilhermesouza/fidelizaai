﻿using FidelizaAi.Web.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FidelizaAi.Web.Data.Mapping
{
    public class CompanyTypeMapping : IEntityTypeConfiguration<CompanyType>
    {
        public void Configure(EntityTypeBuilder<CompanyType> builder)
        {
            builder.ToTable("CompanyType", "SIS");

            builder.Property(e => e.Description).HasMaxLength(100);

            builder.Property(e => e.Name).HasMaxLength(20);
        }
    }
}
