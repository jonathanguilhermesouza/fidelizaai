﻿using FidelizaAi.Web.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FidelizaAi.Web.Data.Mapping
{
    public class LoyaltyCampaignMapping : IEntityTypeConfiguration<LoyaltyCampaign>
    {
        public void Configure(EntityTypeBuilder<LoyaltyCampaign> builder)
        {
            builder.ToTable("LoyaltyCampaign", "APP");

            builder.Property(e => e.Description).HasMaxLength(300);

            builder.Property(e => e.ExclusiveCompanyCnpj).HasMaxLength(14);

            builder.Property(e => e.Name).HasMaxLength(50);

            builder.HasOne(d => d.ExclusiveCompanyCnpjNavigation)
                .WithMany(p => p.LoyaltyCampaign)
                .HasForeignKey(d => d.ExclusiveCompanyCnpj)
                .HasConstraintName("FK_LoyaltyCampaign_Company");

            builder.HasOne(d => d.Status)
                .WithMany(p => p.LoyaltyCampaign)
                .HasForeignKey(d => d.StatusId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_LoyaltyCampaign_LoyaltyCampaignStatus");
        }
    }
}
