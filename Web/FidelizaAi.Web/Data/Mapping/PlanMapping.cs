﻿using FidelizaAi.Web.Models.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FidelizaAi.Web.Data.Mapping
{
    public class PlanMapping : IEntityTypeConfiguration<Plan>
    {
        public void Configure(EntityTypeBuilder<Plan> builder)
        {
            builder.ToTable("Plan", "BKO");

            builder.Property(e => e.AnualPrice).HasColumnType("decimal(19, 2)");

            builder.Property(e => e.Description).HasMaxLength(100);

            builder.Property(e => e.MonthPrice).HasColumnType("decimal(19, 2)");

            builder.Property(e => e.Name).HasMaxLength(20);
        }
    }
}
