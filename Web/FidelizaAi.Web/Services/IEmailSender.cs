﻿using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FidelizaAi.Web.Services
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
        Task SendEmailAsync(string htmlContent, string from, string apiKei, string email, string subject, string htmlMessage, List<Attachment> attachments = null);

    }
}
