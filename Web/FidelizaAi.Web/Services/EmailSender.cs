﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FidelizaAi.Web.Services
{
    // This class is used by the application to send email for account confirmation and password reset.
    // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
    public class EmailSender : IEmailSender
    {
        public Task SendEmailAsync(string email, string subject, string message)
        {
            return Task.CompletedTask;
        }

        public Task SendEmailAsync(string htmlContent, string from, string apiKei, string email, string subject, string message, List<Attachment> listAttachment)
        {
            return Execute(htmlContent, from, apiKei, subject, message, email, listAttachment);
        }

        public Task Execute(string htmlContent, string emailFrom, string apiKei, string subject, string message, string email, List<Attachment> listAttachment = null)
        {

            var client = new SendGridClient(apiKei);
            var from = new EmailAddress(emailFrom);
            var to = new EmailAddress(email);
            //var plainTextContent = "and easy to do anywhere, even with C#";
            //var htmlContent = "<strong>and easy to do anywhere, even with C#</strong>";
            var msg = MailHelper.CreateSingleEmail(from, to, subject, message, htmlContent);

            if (listAttachment != null)
            {
                msg.AddAttachments(listAttachment);
            }

            return client.SendEmailAsync(msg);
        }
    }
}
