﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FidelizaAi.Web.Data;
using FidelizaAi.Web.Models.Domain;

namespace FidelizaAi.Web.Controllers
{
    public class CompanySizeController : Controller
    {
        private readonly FidelizaaiContext _context;

        public CompanySizeController(FidelizaaiContext context)
        {
            _context = context;
        }

        // GET: CompanySize
        public async Task<IActionResult> Index()
        {
            return View(await _context.CompanySize.ToListAsync());
        }

        // GET: CompanySize/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companySize = await _context.CompanySize
                .FirstOrDefaultAsync(m => m.CompanySizeId == id);
            if (companySize == null)
            {
                return NotFound();
            }

            return View(companySize);
        }

        // GET: CompanySize/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CompanySize/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CompanySizeId,Name,Description")] CompanySize companySize)
        {
            if (ModelState.IsValid)
            {
                _context.Add(companySize);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(companySize);
        }

        // GET: CompanySize/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companySize = await _context.CompanySize.FindAsync(id);
            if (companySize == null)
            {
                return NotFound();
            }
            return View(companySize);
        }

        // POST: CompanySize/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CompanySizeId,Name,Description")] CompanySize companySize)
        {
            if (id != companySize.CompanySizeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(companySize);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompanySizeExists(companySize.CompanySizeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(companySize);
        }

        // GET: CompanySize/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companySize = await _context.CompanySize
                .FirstOrDefaultAsync(m => m.CompanySizeId == id);
            if (companySize == null)
            {
                return NotFound();
            }

            return View(companySize);
        }

        // POST: CompanySize/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var companySize = await _context.CompanySize.FindAsync(id);
            _context.CompanySize.Remove(companySize);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompanySizeExists(int id)
        {
            return _context.CompanySize.Any(e => e.CompanySizeId == id);
        }
    }
}
