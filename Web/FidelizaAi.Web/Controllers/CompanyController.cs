﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FidelizaAi.Web.Data;
using FidelizaAi.Web.Models.Domain;

namespace FidelizaAi.Web.Controllers
{
    public class CompanyController : Controller
    {
        private readonly FidelizaaiContext _context;

        public CompanyController(FidelizaaiContext context)
        {
            _context = context;
        }

        // GET: Company
        public async Task<IActionResult> Index()
        {
            var fidelizaaiContext = _context.Company.Include(c => c.CompanyType).Include(c => c.Subscriber);
            return View(await fidelizaaiContext.ToListAsync());
        }

        // GET: Company/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Company
                .Include(c => c.CompanyType)
                .Include(c => c.Subscriber)
                .FirstOrDefaultAsync(m => m.Cnpj == id);
            if (company == null)
            {
                return NotFound();
            }

            return View(company);
        }

        // GET: Company/Create
        public IActionResult Create()
        {
            ViewData["CompanyTypeId"] = new SelectList(_context.CompanyType, "CompanyTypeId", "Name");
            ViewData["CompanySizeId"] = new SelectList(_context.CompanySize, "CompanySizeId", "Name");
            ViewData["SubscriberId"] = new SelectList(_context.Subscriber, "SubscriberId", "Name");
            return View();
        }

        // POST: Company/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Cnpj,SubscriberId,CompanyTypeId,CompanySizeId,CorporateName,FantasyName,PhoneNumber1,PhoneNumber2,WhatsApp,Email,CnpjMatriz,Cep,Logradouro,Complemento,Bairro,Localidade,Uf,IsActive")] Company company)
        {
            if (ModelState.IsValid)
            {
                _context.Add(company);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyTypeId"] = new SelectList(_context.CompanyType, "CompanyTypeId", "CompanyTypeId", company.CompanyTypeId);
            ViewData["CompanySizeId"] = new SelectList(_context.CompanySize, "CompanySizeId", "Name");
            ViewData["SubscriberId"] = new SelectList(_context.Subscriber, "SubscriberId", "OwnerId", company.SubscriberId);
            return View(company);
        }

        // GET: Company/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Company.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }
            ViewData["CompanyTypeId"] = new SelectList(_context.CompanyType, "CompanyTypeId", "CompanyTypeId", company.CompanyTypeId);
            ViewData["SubscriberId"] = new SelectList(_context.Subscriber, "SubscriberId", "OwnerId", company.SubscriberId);
            return View(company);
        }

        // POST: Company/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Cnpj,SubscriberId,CompanyTypeId,CorporateName,FantasyName,PhoneNumber1,PhoneNumber2,WhatsApp,Email,CnpjMatriz,Cep,Logradouro,Complemento,Bairro,Localidade,Uf,IsActive")] Company company)
        {
            if (id != company.Cnpj)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(company);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompanyExists(company.Cnpj))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyTypeId"] = new SelectList(_context.CompanyType, "CompanyTypeId", "CompanyTypeId", company.CompanyTypeId);
            ViewData["SubscriberId"] = new SelectList(_context.Subscriber, "SubscriberId", "OwnerId", company.SubscriberId);
            return View(company);
        }

        // GET: Company/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var company = await _context.Company
                .Include(c => c.CompanyType)
                .Include(c => c.Subscriber)
                .FirstOrDefaultAsync(m => m.Cnpj == id);
            if (company == null)
            {
                return NotFound();
            }

            return View(company);
        }

        // POST: Company/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var company = await _context.Company.FindAsync(id);
            _context.Company.Remove(company);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompanyExists(string id)
        {
            return _context.Company.Any(e => e.Cnpj == id);
        }
    }
}
