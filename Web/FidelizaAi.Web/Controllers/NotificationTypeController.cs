﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FidelizaAi.Web.Data;
using FidelizaAi.Web.Models.Domain;

namespace FidelizaAi.Web.Controllers
{
    public class NotificationTypeController : Controller
    {
        private readonly FidelizaaiContext _context;

        public NotificationTypeController(FidelizaaiContext context)
        {
            _context = context;
        }

        // GET: NotificationType
        public async Task<IActionResult> Index()
        {
            return View(await _context.NotificationType.ToListAsync());
        }

        // GET: NotificationType/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificationType = await _context.NotificationType
                .FirstOrDefaultAsync(m => m.NotificationTypeId == id);
            if (notificationType == null)
            {
                return NotFound();
            }

            return View(notificationType);
        }

        // GET: NotificationType/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: NotificationType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("NotificationTypeId,Name,Descrpition,ColorHexa")] NotificationType notificationType)
        {
            if (ModelState.IsValid)
            {
                _context.Add(notificationType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(notificationType);
        }

        // GET: NotificationType/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificationType = await _context.NotificationType.FindAsync(id);
            if (notificationType == null)
            {
                return NotFound();
            }
            return View(notificationType);
        }

        // POST: NotificationType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("NotificationTypeId,Name,Descrpition,ColorHexa")] NotificationType notificationType)
        {
            if (id != notificationType.NotificationTypeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(notificationType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NotificationTypeExists(notificationType.NotificationTypeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(notificationType);
        }

        // GET: NotificationType/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificationType = await _context.NotificationType
                .FirstOrDefaultAsync(m => m.NotificationTypeId == id);
            if (notificationType == null)
            {
                return NotFound();
            }

            return View(notificationType);
        }

        // POST: NotificationType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var notificationType = await _context.NotificationType.FindAsync(id);
            _context.NotificationType.Remove(notificationType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NotificationTypeExists(int id)
        {
            return _context.NotificationType.Any(e => e.NotificationTypeId == id);
        }
    }
}
