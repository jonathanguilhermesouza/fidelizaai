﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FidelizaAi.Web.Data;
using FidelizaAi.Web.Models.Domain;

namespace FidelizaAi.Web.Controllers
{
    public class LoyaltyCampaignStatusController : Controller
    {
        private readonly FidelizaaiContext _context;

        public LoyaltyCampaignStatusController(FidelizaaiContext context)
        {
            _context = context;
        }

        // GET: LoyaltyCampaignStatus
        public async Task<IActionResult> Index()
        {
            return View(await _context.LoyaltyCampaignStatus.ToListAsync());
        }

        // GET: LoyaltyCampaignStatus/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loyaltyCampaignStatus = await _context.LoyaltyCampaignStatus
                .FirstOrDefaultAsync(m => m.LoyaltyCampaignStatusId == id);
            if (loyaltyCampaignStatus == null)
            {
                return NotFound();
            }

            return View(loyaltyCampaignStatus);
        }

        // GET: LoyaltyCampaignStatus/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: LoyaltyCampaignStatus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("LoyaltyCampaignStatusId,Name,Description")] LoyaltyCampaignStatus loyaltyCampaignStatus)
        {
            if (ModelState.IsValid)
            {
                _context.Add(loyaltyCampaignStatus);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(loyaltyCampaignStatus);
        }

        // GET: LoyaltyCampaignStatus/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loyaltyCampaignStatus = await _context.LoyaltyCampaignStatus.FindAsync(id);
            if (loyaltyCampaignStatus == null)
            {
                return NotFound();
            }
            return View(loyaltyCampaignStatus);
        }

        // POST: LoyaltyCampaignStatus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("LoyaltyCampaignStatusId,Name,Description")] LoyaltyCampaignStatus loyaltyCampaignStatus)
        {
            if (id != loyaltyCampaignStatus.LoyaltyCampaignStatusId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(loyaltyCampaignStatus);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LoyaltyCampaignStatusExists(loyaltyCampaignStatus.LoyaltyCampaignStatusId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(loyaltyCampaignStatus);
        }

        // GET: LoyaltyCampaignStatus/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loyaltyCampaignStatus = await _context.LoyaltyCampaignStatus
                .FirstOrDefaultAsync(m => m.LoyaltyCampaignStatusId == id);
            if (loyaltyCampaignStatus == null)
            {
                return NotFound();
            }

            return View(loyaltyCampaignStatus);
        }

        // POST: LoyaltyCampaignStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var loyaltyCampaignStatus = await _context.LoyaltyCampaignStatus.FindAsync(id);
            _context.LoyaltyCampaignStatus.Remove(loyaltyCampaignStatus);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LoyaltyCampaignStatusExists(int id)
        {
            return _context.LoyaltyCampaignStatus.Any(e => e.LoyaltyCampaignStatusId == id);
        }
    }
}
