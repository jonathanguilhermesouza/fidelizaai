﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FidelizaAi.Web.Data;
using FidelizaAi.Web.Models.Domain;
using FidelizaAi.Web.Models.ViewModel;

namespace FidelizaAi.Web.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyApiController : ControllerBase
    {
        private readonly FidelizaaiContext _context;

        public CompanyApiController(FidelizaaiContext context)
        {
            _context = context;
        }

        // GET: api/CompanyApi
        [HttpGet]
        [Route("GetListCompany")]
        public async Task<ActionResult<IEnumerable<MarketplaceListItemViewModel>>> GetListCompany()
        {
            List<MarketplaceListItemViewModel> listCompany = new List<MarketplaceListItemViewModel>();
            var companies = await _context.Company.Include(x => x.CompanyType).ToListAsync();
            foreach (var item in companies)
            {
                var companyItem = new MarketplaceListItemViewModel()
                {
                    CompanyId = item.Cnpj,
                    CompanyType = item.CompanyType.Name,
                    Name = item.FantasyName,
                    IconPath = item.IconPath
                };
                listCompany.Add(companyItem);
            }

            return listCompany;
        }

        // GET: api/CompanyApi
        [HttpGet("GetDetailCompanyById/{id}")]
        public async Task<ActionResult<MarketplaceDetailViewModel>> GetDetailCompanyById(string id)
        {
            List<LoyaltyCampaingnListItemViewModel> listLoyalty = new List<LoyaltyCampaingnListItemViewModel>();
            var companyData = await _context.Company.Include(x => x.LoyaltyCampaign).FirstOrDefaultAsync(x => x.Cnpj == id);

            if (companyData == null)
                return Ok();

            foreach (var item in companyData.LoyaltyCampaign)
            {
                var loyaltyVm = new LoyaltyCampaingnListItemViewModel()
                {
                    EndDate = item.EndDate,
                    StartDate = item.StartDate,
                    LoyaltyCampaignId = item.LoyaltyCampaignId,
                    Name = item.Name,
                    NumberStampsTotalMax = item.NumberStampsTotalMax
                };
                listLoyalty.Add(loyaltyVm);
            }
            
            var companyVm = new MarketplaceDetailViewModel()
            {
                Email = companyData.Email,
                Name = companyData.FantasyName,
                PhoneNumber1 = companyData.PhoneNumber1,
                PhoneNumber2 = companyData.PhoneNumber2,
                WhatsApp = companyData.WhatsApp,
                Site = companyData.WebSite,
                Facebook = companyData.Facebook,
                Instagram = companyData.Instagram,
                FullAddress = $"{companyData.Logradouro}, {companyData.Number}, {companyData.Bairro}, {companyData.Localidade} - {companyData.Uf}",
                Galery = new List<string>() { string.Empty, string.Empty},
                ListLoyaltyCampaingn = listLoyalty
            };

            return companyVm;
        }

        // GET: api/CompanyApi
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Company>>> GetCompany()
        {
            return await _context.Company.ToListAsync();
        }

        // GET: api/CompanyApi/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Company>> GetCompany(string id)
        {
            var company = await _context.Company.FindAsync(id);

            if (company == null)
            {
                return NotFound();
            }

            return company;
        }

        // PUT: api/CompanyApi/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCompany(string id, Company company)
        {
            if (id != company.Cnpj)
            {
                return BadRequest();
            }

            _context.Entry(company).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CompanyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CompanyApi
        [HttpPost]
        public async Task<ActionResult<Company>> PostCompany(Company company)
        {
            _context.Company.Add(company);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (CompanyExists(company.Cnpj))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetCompany", new { id = company.Cnpj }, company);
        }

        // DELETE: api/CompanyApi/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Company>> DeleteCompany(string id)
        {
            var company = await _context.Company.FindAsync(id);
            if (company == null)
            {
                return NotFound();
            }

            _context.Company.Remove(company);
            await _context.SaveChangesAsync();

            return company;
        }

        private bool CompanyExists(string id)
        {
            return _context.Company.Any(e => e.Cnpj == id);
        }
    }
}
