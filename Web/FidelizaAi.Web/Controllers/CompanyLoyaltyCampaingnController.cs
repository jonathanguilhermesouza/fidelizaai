﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FidelizaAi.Web.Data;
using FidelizaAi.Web.Models.Domain;

namespace FidelizaAi.Web.Controllers
{
    public class CompanyLoyaltyCampaingnController : Controller
    {
        private readonly FidelizaaiContext _context;

        public CompanyLoyaltyCampaingnController(FidelizaaiContext context)
        {
            _context = context;
        }

        // GET: CompanyLoyaltyCampaingn
        public async Task<IActionResult> Index()
        {
            var fidelizaaiContext = _context.CompanyLoyaltyCampaingn.Include(c => c.CompanyCnpjNavigation).Include(c => c.LoyaltyCampaingn);
            return View(await fidelizaaiContext.ToListAsync());
        }

        // GET: CompanyLoyaltyCampaingn/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyLoyaltyCampaingn = await _context.CompanyLoyaltyCampaingn
                .Include(c => c.CompanyCnpjNavigation)
                .Include(c => c.LoyaltyCampaingn)
                .FirstOrDefaultAsync(m => m.LoyaltyCampaingnId == id);
            if (companyLoyaltyCampaingn == null)
            {
                return NotFound();
            }

            return View(companyLoyaltyCampaingn);
        }

        // GET: CompanyLoyaltyCampaingn/Create
        public IActionResult Create()
        {
            ViewData["CompanyCnpj"] = new SelectList(_context.Company, "Cnpj", "Cnpj");
            ViewData["LoyaltyCampaingnId"] = new SelectList(_context.LoyaltyCampaign, "LoyaltyCampaignId", "LoyaltyCampaignId");
            return View();
        }

        // POST: CompanyLoyaltyCampaingn/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("LoyaltyCampaingnId,CompanyCnpj")] CompanyLoyaltyCampaingn companyLoyaltyCampaingn)
        {
            if (ModelState.IsValid)
            {
                _context.Add(companyLoyaltyCampaingn);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyCnpj"] = new SelectList(_context.Company, "Cnpj", "Cnpj", companyLoyaltyCampaingn.CompanyCnpj);
            ViewData["LoyaltyCampaingnId"] = new SelectList(_context.LoyaltyCampaign, "LoyaltyCampaignId", "LoyaltyCampaignId", companyLoyaltyCampaingn.LoyaltyCampaingnId);
            return View(companyLoyaltyCampaingn);
        }

        // GET: CompanyLoyaltyCampaingn/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyLoyaltyCampaingn = await _context.CompanyLoyaltyCampaingn.FindAsync(id);
            if (companyLoyaltyCampaingn == null)
            {
                return NotFound();
            }
            ViewData["CompanyCnpj"] = new SelectList(_context.Company, "Cnpj", "Cnpj", companyLoyaltyCampaingn.CompanyCnpj);
            ViewData["LoyaltyCampaingnId"] = new SelectList(_context.LoyaltyCampaign, "LoyaltyCampaignId", "LoyaltyCampaignId", companyLoyaltyCampaingn.LoyaltyCampaingnId);
            return View(companyLoyaltyCampaingn);
        }

        // POST: CompanyLoyaltyCampaingn/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("LoyaltyCampaingnId,CompanyCnpj")] CompanyLoyaltyCampaingn companyLoyaltyCampaingn)
        {
            if (id != companyLoyaltyCampaingn.LoyaltyCampaingnId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(companyLoyaltyCampaingn);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompanyLoyaltyCampaingnExists(companyLoyaltyCampaingn.LoyaltyCampaingnId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyCnpj"] = new SelectList(_context.Company, "Cnpj", "Cnpj", companyLoyaltyCampaingn.CompanyCnpj);
            ViewData["LoyaltyCampaingnId"] = new SelectList(_context.LoyaltyCampaign, "LoyaltyCampaignId", "LoyaltyCampaignId", companyLoyaltyCampaingn.LoyaltyCampaingnId);
            return View(companyLoyaltyCampaingn);
        }

        // GET: CompanyLoyaltyCampaingn/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companyLoyaltyCampaingn = await _context.CompanyLoyaltyCampaingn
                .Include(c => c.CompanyCnpjNavigation)
                .Include(c => c.LoyaltyCampaingn)
                .FirstOrDefaultAsync(m => m.LoyaltyCampaingnId == id);
            if (companyLoyaltyCampaingn == null)
            {
                return NotFound();
            }

            return View(companyLoyaltyCampaingn);
        }

        // POST: CompanyLoyaltyCampaingn/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var companyLoyaltyCampaingn = await _context.CompanyLoyaltyCampaingn.FindAsync(id);
            _context.CompanyLoyaltyCampaingn.Remove(companyLoyaltyCampaingn);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CompanyLoyaltyCampaingnExists(long id)
        {
            return _context.CompanyLoyaltyCampaingn.Any(e => e.LoyaltyCampaingnId == id);
        }
    }
}
