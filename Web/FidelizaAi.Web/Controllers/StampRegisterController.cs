﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FidelizaAi.Web.Data;
using FidelizaAi.Web.Models.Domain;

namespace FidelizaAi.Web.Controllers
{
    public class StampRegisterController : Controller
    {
        private readonly FidelizaaiContext _context;

        public StampRegisterController(FidelizaaiContext context)
        {
            _context = context;
        }

        // GET: StampRegister
        public async Task<IActionResult> Index()
        {
            var fidelizaaiContext = _context.StampRegister.Include(s => s.LoyaltyCampaingn).Include(s => s.User);
            return View(await fidelizaaiContext.ToListAsync());
        }

        // GET: StampRegister/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var stampRegister = await _context.StampRegister
                .Include(s => s.LoyaltyCampaingn)
                .Include(s => s.User)
                .FirstOrDefaultAsync(m => m.StampRegisterId == id);
            if (stampRegister == null)
            {
                return NotFound();
            }

            return View(stampRegister);
        }

        // GET: StampRegister/Create
        public IActionResult Create()
        {
            ViewData["LoyaltyCampaingnId"] = new SelectList(_context.LoyaltyCampaign, "LoyaltyCampaignId", "LoyaltyCampaignId");
            ViewData["UserId"] = new SelectList(_context.User, "UserId", "UserId");
            return View();
        }

        // POST: StampRegister/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StampRegisterId,UserId,LoyaltyCampaingnId,NumberStamps,RegisterDate,Description")] StampRegister stampRegister)
        {
            if (ModelState.IsValid)
            {
                _context.Add(stampRegister);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["LoyaltyCampaingnId"] = new SelectList(_context.LoyaltyCampaign, "LoyaltyCampaignId", "LoyaltyCampaignId", stampRegister.LoyaltyCampaingnId);
            ViewData["UserId"] = new SelectList(_context.User, "UserId", "UserId", stampRegister.UserId);
            return View(stampRegister);
        }

        // GET: StampRegister/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var stampRegister = await _context.StampRegister.FindAsync(id);
            if (stampRegister == null)
            {
                return NotFound();
            }
            ViewData["LoyaltyCampaingnId"] = new SelectList(_context.LoyaltyCampaign, "LoyaltyCampaignId", "LoyaltyCampaignId", stampRegister.LoyaltyCampaingnId);
            ViewData["UserId"] = new SelectList(_context.User, "UserId", "UserId", stampRegister.UserId);
            return View(stampRegister);
        }

        // POST: StampRegister/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("StampRegisterId,UserId,LoyaltyCampaingnId,NumberStamps,RegisterDate,Description")] StampRegister stampRegister)
        {
            if (id != stampRegister.StampRegisterId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(stampRegister);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StampRegisterExists(stampRegister.StampRegisterId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["LoyaltyCampaingnId"] = new SelectList(_context.LoyaltyCampaign, "LoyaltyCampaignId", "LoyaltyCampaignId", stampRegister.LoyaltyCampaingnId);
            ViewData["UserId"] = new SelectList(_context.User, "UserId", "UserId", stampRegister.UserId);
            return View(stampRegister);
        }

        // GET: StampRegister/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var stampRegister = await _context.StampRegister
                .Include(s => s.LoyaltyCampaingn)
                .Include(s => s.User)
                .FirstOrDefaultAsync(m => m.StampRegisterId == id);
            if (stampRegister == null)
            {
                return NotFound();
            }

            return View(stampRegister);
        }

        // POST: StampRegister/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var stampRegister = await _context.StampRegister.FindAsync(id);
            _context.StampRegister.Remove(stampRegister);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StampRegisterExists(long id)
        {
            return _context.StampRegister.Any(e => e.StampRegisterId == id);
        }
    }
}
