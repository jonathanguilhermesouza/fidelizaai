﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FidelizaAi.Web.Models.AccountViewModels;
using FidelizaAi.Web.Models;
using FidelizaAi.Web.Extensions;
using FidelizaAi.Web.Services;
using System.Threading;
using System.Security.Principal;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using FidelizaAi.Web.Models.Enum;
using System.Net;
using System.Text.Encodings.Web;

/*
 Estoudo
 http://codereform.com/blog/post/asp-net-core-api-authentication-using-jwt-bearer-tokens/
 https://www.codeproject.com/Articles/5160941/ASP-NET-CORE-Token-Authentication-and-Authorizatio
 */

namespace FidelizaAi.Web.Controllers
{
    //[Authorize]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AccountController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly IConfiguration _configuration;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            IConfiguration configuration)
            : base(configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _configuration = configuration;
        }

        [HttpGet]
        [Authorize(Roles = "CommonUser")]
        public async Task<IActionResult> IsAuthenticated()
        {
            return Ok(User.Claims);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Listar()
        {
            return Ok("Teste");
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWithExternalProvider(ExternalLoginViewModel login)
        {
            try
            {
                ClaimsPrincipal principal = null;
                Task<IList<Claim>> claimsUser = null;
                ApplicationUser user = null;

                if (login == null)
                {
                    _logger.LogInformation("Dados de login não informados");
                    return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.NullData, "Dados de login não informados"));
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogInformation("Algum campo não foi informado");
                    return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.ModelStateInvalid, "Algum campo não foi informado"));
                }

                //Cria o usuário caso não exista
                //bool existUser = _signInManager.UserManager.Users.Any(x => x.Email == login.Email);
                user = _signInManager.UserManager.Users.FirstOrDefault(x => x.Email == login.Email);
                if (user != null)
                {
                    claimsUser = _signInManager.UserManager.GetClaimsAsync(user);
                    bool isProviderIdentity = claimsUser.Result.Any(x => x.Value == "Identity");

                    if (isProviderIdentity)
                    {
                        _logger.LogInformation("Este usuário está registrado através de uma conta cadastrada na tela de login, tente logar informando seu usuário e senha na tela principal");
                        return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.CreateUserError, "Este usuário está registrado através de uma conta cadastrada na tela de login, tente logar informando seu usuário e senha na tela principal"));
                    }
                    else
                    {
                        var provider = claimsUser.Result.FirstOrDefault(x => x.Type == "Provider");
                        if (provider.Value == "Google" && login.Provider == "Facebook")
                        {
                            _logger.LogInformation("Este usuário está registrado através de uma conta Google na tela de login, tente logar clicando no botão Google");
                            return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.CreateUserError, "Este usuário está registrado através de uma conta Google na tela de login, tente logar clicando no botão Google"));
                        }
                        if (provider.Value == "Facebook" && login.Provider == "Google")
                        {
                            _logger.LogInformation("Este usuário está registrado através de uma conta Facebook na tela de login, tente logar clicando no botão Facebook");
                            return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.CreateUserError, "Este usuário está registrado através de uma conta Facebook na tela de login, tente logar clicando no botão Facebook"));
                        }

                    }
                }
                else
                {
                    var newUser = new ApplicationUser
                    {
                        UserName = login.Email,
                        Email = login.Email,
                        EmailConfirmed = true,
                        Name = login.Name,
                        LastName = login.LastName
                    };
                    login.Role = "CommonUser";

                    var createResult = await _userManager.CreateAsync(newUser);
                    if (!createResult.Succeeded)
                    {
                        _logger.LogInformation("Não foi possível criar o usuário, provavelmente você se registrou sem o vínculo com Google/Facebook, tente logar informando seu usuário e senha na tela principal");
                        return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.CreateUserError, "Não foi possível criar o usuário, provavelmente você se registrou sem o vínculo com Google/Facebook, tente logar informando seu usuário e senha na tela principal"));
                    }

                    principal = base.SetClaims(login);
                    ExternalLoginInfo info = new ExternalLoginInfo(principal, login.Provider, login.Id, login.Name);

                    await _userManager.AddLoginAsync(newUser, info);
                    //var newUserClaims = info.Principal.Claims.Append(new Claim("userId", newUser.Id));
                    await _userManager.AddClaimsAsync(newUser, principal.Claims);
                    await _signInManager.SignInAsync(newUser, isPersistent: false);
                    await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
                }

                //Login
                var result = await _signInManager.ExternalLoginSignInAsync(login.Provider, login.Id, isPersistent: false);
                if (!result.Succeeded)
                {
                    _logger.LogInformation("Usuário ou senha incorreto(s)");
                    return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.UserOrPasswordIncorrect, "Usuário ou senha incorreto(s)"));
                }

                if (principal == null)
                {
                    user = _signInManager.UserManager.Users.FirstOrDefault(x => x.Email == login.Email);
                    claimsUser = _signInManager.UserManager.GetClaimsAsync(user);
                    var role = claimsUser.Result.FirstOrDefault(x => x.Type == ClaimTypes.Role);
                    login.Role = role.Value;
                    principal = base.SetClaims(login);
                }

                Thread.CurrentPrincipal = principal;
                var response = this.GetAccessToken(login);
                return Ok(new ResponseAccountViewModel(HttpStatusCode.OK, EResponseAccount.Sucess, response));
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message);
                return BadRequest((new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.Catch, "Ocorreu um erro, entre em contato com o suporte.")));
            }
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    _logger.LogInformation("Algum campo não foi informado");
                    return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.ModelStateInvalid, "Algum campo não foi informado"));
                }

                var user = _signInManager.UserManager.Users.FirstOrDefault(x => x.Email == model.Email);
                if (user == null)
                {
                    _logger.LogInformation("Este usuário ainda não está cadastrado");
                    return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.UserNotFound, "Este usuário ainda não está cadastrado"));
                }

                var claimsUser = _signInManager.UserManager.GetClaimsAsync(user);
                var isExternalProvider = claimsUser.Result.Any(x => x.Value == "Google" || x.Value == "Facebook");

                if (isExternalProvider)
                {
                    _logger.LogInformation("Este usuário está registrado através de uma conta Google/Facebook, tente logar clicando no botão Google ou Facebook");
                    return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.CreateUserError, "Este usuário está registrado através de uma conta Google/Facebook, tente logar clicando no botão Google ou Facebook"));
                }

                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (!result.Succeeded)
                {
                    _logger.LogInformation("Usuário ou senha incorreto(s)");
                    return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.UserOrPasswordIncorrect, "Usuário ou senha incorreto(s)"));
                }

                _logger.LogInformation("User logged in.");
                ExternalLoginViewModel login = new ExternalLoginViewModel();
                login.Email = model.Email;
                login.FamilyName = model.Email;
                //var user = _signInManager.UserManager.Users.FirstOrDefault(x => x.Email == login.Email);
                //var claimsUser = _signInManager.UserManager.GetClaimsAsync(user);
                var role = claimsUser.Result.FirstOrDefault(x => x.Type == ClaimTypes.Role);
                login.Role = role.Value;

                var response = this.GetAccessToken(login);
                return Ok(new ResponseAccountViewModel(HttpStatusCode.OK, EResponseAccount.Sucess, response));

                //if (result.RequiresTwoFactor)
                //{
                //    //return RedirectToAction(nameof(LoginWith2fa), new { returnUrl, model.RememberMe });
                //}
                //if (result.IsLockedOut)
                //{
                //    _logger.LogWarning("User account locked out.");
                //    return RedirectToAction(nameof(Lockout));
                //}
                //else
                //{
                //    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                //    return Ok(model);
                //}
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message);
                return BadRequest((new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.Catch, "Ocorreu um erro, entre em contato com o suporte.")));
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            try
            {
                ClaimsPrincipal principal = null;

                if (!ModelState.IsValid)
                {
                    _logger.LogInformation("Algum campo não foi informado");
                    return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.ModelStateInvalid, "Algum campo não foi informado"));
                }

                var user = new ApplicationUser { UserName = model.Email, Email = model.Email, Name = model.Name, LastName = model.LastName };
                var result = await _userManager.CreateAsync(user, model.Password);

                if (!result.Succeeded)
                {
                    {
                        _logger.LogInformation("Não foi possível criar o usuário, provavelmente você se registrou com vínculo Google/Facebook, tente logar clicando no botão do Google ou Facebook");
                        return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.CreateUserError, "Não foi possível criar o usuário, provavelmente você se registrou com vínculo Google/Facebook, tente logar cliando no botão do Google ou Facebook"));
                    }
                }

                _logger.LogInformation("User created a new account with password.");

                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
                await _emailSender.SendEmailConfirmationAsync(model.Email, callbackUrl);

                _logger.LogInformation("User created a new account with password.");

                await _signInManager.SignInAsync(user, isPersistent: false);

                ExternalLoginViewModel login = new ExternalLoginViewModel();
                login.Email = model.Email;
                login.FamilyName = model.Email;
                login.Name = model.Name;
                login.Role = "CommonUser";
                login.Provider = "Identity";

                principal = base.SetClaims(login);
                //ExternalLoginInfo info = new ExternalLoginInfo(principal, login.Provider, login.Id, login.Name);
                //UserLoginInfo info = new UserLoginInfo(user.Email, user.Id, user.Email);

                // await _userManager.AddLoginAsync(newUser, info);
                //var newUserClaims = info.Principal.Claims.Append(new Claim("userId", newUser.Id));
                await _userManager.AddClaimsAsync(user, principal.Claims);
                await _signInManager.SignInAsync(user, isPersistent: false);
                await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

                Thread.CurrentPrincipal = principal;
                var response = this.GetAccessToken(login);

                return Ok(new ResponseAccountViewModel(HttpStatusCode.OK, EResponseAccount.Sucess, response));
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message);
                return BadRequest((new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.Catch, "Ocorreu um erro, entre em contato com o suporte.")));
            }
        }

        #region Outros
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            return Ok(returnUrl);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWith2fa(bool rememberMe, string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();

            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var model = new LoginWith2faViewModel { RememberMe = rememberMe };

            return Ok(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWith2fa(LoginWith2faViewModel model, bool rememberMe, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return Ok(model);
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var authenticatorCode = model.TwoFactorCode.Replace(" ", string.Empty).Replace("-", string.Empty);

            var result = await _signInManager.TwoFactorAuthenticatorSignInAsync(authenticatorCode, rememberMe, model.RememberMachine);

            if (result.Succeeded)
            {
                _logger.LogInformation("User with ID {UserId} logged in with 2fa.", user.Id);
                return RedirectToLocal(returnUrl);
            }
            else if (result.IsLockedOut)
            {
                _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                _logger.LogWarning("Invalid authenticator code entered for user with ID {UserId}.", user.Id);
                ModelState.AddModelError(string.Empty, "Invalid authenticator code.");
                return Ok();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> LoginWithRecoveryCode(string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            return Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginWithRecoveryCode(LoginWithRecoveryCodeViewModel model, string returnUrl = null)
        {
            if (!ModelState.IsValid)
            {
                return Ok(model);
            }

            var user = await _signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new ApplicationException($"Unable to load two-factor authentication user.");
            }

            var recoveryCode = model.RecoveryCode.Replace(" ", string.Empty);

            var result = await _signInManager.TwoFactorRecoveryCodeSignInAsync(recoveryCode);

            if (result.Succeeded)
            {
                _logger.LogInformation("User with ID {UserId} logged in with a recovery code.", user.Id);
                return RedirectToLocal(returnUrl);
            }
            if (result.IsLockedOut)
            {
                _logger.LogWarning("User with ID {UserId} account locked out.", user.Id);
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                _logger.LogWarning("Invalid recovery code entered for user with ID {UserId}", user.Id);
                ModelState.AddModelError(string.Empty, "Invalid recovery code entered.");
                return Ok();
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Lockout()
        {
            return Ok();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register(string returnUrl = null)
        {
            return Ok();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public IActionResult ExternalLogin(string provider, string returnUrl = null)
        {
            // Request a redirect to the external login provider.
            var redirectUrl = Url.Action(nameof(ExternalLoginCallback), "Account", new { returnUrl });
            var properties = _signInManager.ConfigureExternalAuthenticationProperties(provider, redirectUrl);
            return Challenge(properties, provider);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ExternalLoginCallback(string returnUrl = null, string remoteError = null)
        {
            if (remoteError != null)
            {
                return RedirectToAction(nameof(Login));
            }
            var info = await _signInManager.GetExternalLoginInfoAsync();
            if (info == null)
            {
                return RedirectToAction(nameof(Login));
            }

            // Sign in the user with this external login provider if the user already has a login.
            var result = await _signInManager.ExternalLoginSignInAsync(info.LoginProvider, info.ProviderKey, isPersistent: false, bypassTwoFactor: true);
            if (result.Succeeded)
            {
                _logger.LogInformation("User logged in with {Name} provider.", info.LoginProvider);
                return RedirectToLocal(returnUrl);
            }
            if (result.IsLockedOut)
            {
                return RedirectToAction(nameof(Lockout));
            }
            else
            {
                // If the user does not have an account, then ask the user to create an account.
                var email = info.Principal.FindFirstValue(ClaimTypes.Email);
                return CreatedAtAction("ExternalLogin", new ExternalLoginViewModel { Email = email });
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ExternalLoginConfirmation(ExternalLoginViewModel model, string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await _signInManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    throw new ApplicationException("Error loading external login information during confirmation.");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await _userManager.AddLoginAsync(user, info);
                    if (result.Succeeded)
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        _logger.LogInformation("User created an account using {Name} provider.", info.LoginProvider);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            return CreatedAtAction(nameof(ExternalLogin), model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }
            var result = await _userManager.ConfirmEmailAsync(user, code);
            return Ok(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return Ok();
        }

        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    _logger.LogInformation("Algum campo não foi informado");
                    return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.ModelStateInvalid, "Algum campo não foi informado"));
                }

                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    _logger.LogInformation("Usuário não existe");
                    return BadRequest(new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.UserNotFound, "Usuário não existe"));
                }

                // For more information on how to enable account confirmation and password reset please
                // visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
                //var callbackUrl1 = Url.Page(
                //    "/Account/ResetPassword",
                //    pageHandler: null,
                //    values: new { code },
                //    protocol: Request.Scheme);

                string apiKey = _configuration["email:apiSendKeyGrid"];
                string emailFrom = _configuration["email:from"];

                await _emailSender.SendEmailAsync(
                    $"Por favor, reset sua senha <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicando aqui</a>.",
                    emailFrom,
                    apiKey,
                    model.Email,
                    "Reset de senha",
                    $"Por favor, reset sua senha <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicando aqui</a>."
                    );

                return Ok(new ResponseAccountViewModel(HttpStatusCode.OK, EResponseAccount.Sucess, "Um email de recuperação de  senha foi enviado para seu email"));
            }
            catch (Exception ex)
            {
                _logger.LogInformation(ex.Message);
                return BadRequest((new ResponseAccountViewModel(HttpStatusCode.BadRequest, EResponseAccount.Catch, "Ocorreu um erro, entre em contato com o suporte.")));
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return Ok();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            var model = new ResetPasswordViewModel { Code = code };
            return Ok(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Ok(model);
            }
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }
            AddErrors(result);
            return Ok();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return Ok();
        }


        [HttpGet]
        public IActionResult AccessDenied()
        {
            return Ok();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion
        #endregion Outros
    }
}