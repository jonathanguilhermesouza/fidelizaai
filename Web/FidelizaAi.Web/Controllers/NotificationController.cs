﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FidelizaAi.Web.Data;
using FidelizaAi.Web.Models.Domain;

namespace FidelizaAi.Web.Controllers
{
    public class NotificationController : Controller
    {
        private readonly FidelizaaiContext _context;

        public NotificationController(FidelizaaiContext context)
        {
            _context = context;
        }

        // GET: Notification
        public async Task<IActionResult> Index()
        {
            var fidelizaaiContext = _context.Notification.Include(n => n.CompanyCnpjNavigation).Include(n => n.NotificationType).Include(n => n.User);
            return View(await fidelizaaiContext.ToListAsync());
        }

        // GET: Notification/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notification = await _context.Notification
                .Include(n => n.CompanyCnpjNavigation)
                .Include(n => n.NotificationType)
                .Include(n => n.User)
                .FirstOrDefaultAsync(m => m.NotificationId == id);
            if (notification == null)
            {
                return NotFound();
            }

            return View(notification);
        }

        // GET: Notification/Create
        public IActionResult Create()
        {
            ViewData["CompanyCnpj"] = new SelectList(_context.Company, "Cnpj", "Cnpj");
            ViewData["NotificationTypeId"] = new SelectList(_context.NotificationType, "NotificationTypeId", "NotificationTypeId");
            ViewData["UserId"] = new SelectList(_context.User, "UserId", "UserId");
            return View();
        }

        // POST: Notification/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("NotificationId,CompanyCnpj,UserId,NotificationTypeId,IsRead,Title,Description,RegisterDate")] Notification notification)
        {
            if (ModelState.IsValid)
            {
                _context.Add(notification);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyCnpj"] = new SelectList(_context.Company, "Cnpj", "Cnpj", notification.CompanyCnpj);
            ViewData["NotificationTypeId"] = new SelectList(_context.NotificationType, "NotificationTypeId", "NotificationTypeId", notification.NotificationTypeId);
            ViewData["UserId"] = new SelectList(_context.User, "UserId", "UserId", notification.UserId);
            return View(notification);
        }

        // GET: Notification/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notification = await _context.Notification.FindAsync(id);
            if (notification == null)
            {
                return NotFound();
            }
            ViewData["CompanyCnpj"] = new SelectList(_context.Company, "Cnpj", "Cnpj", notification.CompanyCnpj);
            ViewData["NotificationTypeId"] = new SelectList(_context.NotificationType, "NotificationTypeId", "NotificationTypeId", notification.NotificationTypeId);
            ViewData["UserId"] = new SelectList(_context.User, "UserId", "UserId", notification.UserId);
            return View(notification);
        }

        // POST: Notification/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("NotificationId,CompanyCnpj,UserId,NotificationTypeId,IsRead,Title,Description,RegisterDate")] Notification notification)
        {
            if (id != notification.NotificationId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(notification);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NotificationExists(notification.NotificationId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CompanyCnpj"] = new SelectList(_context.Company, "Cnpj", "Cnpj", notification.CompanyCnpj);
            ViewData["NotificationTypeId"] = new SelectList(_context.NotificationType, "NotificationTypeId", "NotificationTypeId", notification.NotificationTypeId);
            ViewData["UserId"] = new SelectList(_context.User, "UserId", "UserId", notification.UserId);
            return View(notification);
        }

        // GET: Notification/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notification = await _context.Notification
                .Include(n => n.CompanyCnpjNavigation)
                .Include(n => n.NotificationType)
                .Include(n => n.User)
                .FirstOrDefaultAsync(m => m.NotificationId == id);
            if (notification == null)
            {
                return NotFound();
            }

            return View(notification);
        }

        // POST: Notification/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var notification = await _context.Notification.FindAsync(id);
            _context.Notification.Remove(notification);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NotificationExists(long id)
        {
            return _context.Notification.Any(e => e.NotificationId == id);
        }
    }
}
