﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FidelizaAi.Web.Data;
using FidelizaAi.Web.Models.Domain;

namespace FidelizaAi.Web.Controllers
{
    public class LoyaltyCampaignController : Controller
    {
        private readonly FidelizaaiContext _context;

        public LoyaltyCampaignController(FidelizaaiContext context)
        {
            _context = context;
        }

        // GET: LoyaltyCampaign
        public async Task<IActionResult> Index()
        {
            var fidelizaaiContext = _context.LoyaltyCampaign.Include(l => l.ExclusiveCompanyCnpjNavigation).Include(l => l.Status);
            return View(await fidelizaaiContext.ToListAsync());
        }

        // GET: LoyaltyCampaign/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loyaltyCampaign = await _context.LoyaltyCampaign
                .Include(l => l.ExclusiveCompanyCnpjNavigation)
                .Include(l => l.Status)
                .FirstOrDefaultAsync(m => m.LoyaltyCampaignId == id);
            if (loyaltyCampaign == null)
            {
                return NotFound();
            }

            return View(loyaltyCampaign);
        }

        // GET: LoyaltyCampaign/Create
        public IActionResult Create()
        {
            ViewData["ExclusiveCompanyCnpj"] = new SelectList(_context.Company, "Cnpj", "Cnpj");
            ViewData["StatusId"] = new SelectList(_context.LoyaltyCampaignStatus, "LoyaltyCampaignStatusId", "LoyaltyCampaignStatusId");
            return View();
        }

        // POST: LoyaltyCampaign/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("LoyaltyCampaignId,ExclusiveCompanyCnpj,StatusId,Name,Description,StartDate,EndDate,Rules,NumberStampsTotalMax,NumberStampsLimitMaxAtTime,NumberStampsLimitMinAtTime,IsActive")] LoyaltyCampaign loyaltyCampaign)
        {
            if (ModelState.IsValid)
            {
                _context.Add(loyaltyCampaign);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ExclusiveCompanyCnpj"] = new SelectList(_context.Company, "Cnpj", "Cnpj", loyaltyCampaign.ExclusiveCompanyCnpj);
            ViewData["StatusId"] = new SelectList(_context.LoyaltyCampaignStatus, "LoyaltyCampaignStatusId", "LoyaltyCampaignStatusId", loyaltyCampaign.StatusId);
            return View(loyaltyCampaign);
        }

        // GET: LoyaltyCampaign/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loyaltyCampaign = await _context.LoyaltyCampaign.FindAsync(id);
            if (loyaltyCampaign == null)
            {
                return NotFound();
            }
            ViewData["ExclusiveCompanyCnpj"] = new SelectList(_context.Company, "Cnpj", "Cnpj", loyaltyCampaign.ExclusiveCompanyCnpj);
            ViewData["StatusId"] = new SelectList(_context.LoyaltyCampaignStatus, "LoyaltyCampaignStatusId", "LoyaltyCampaignStatusId", loyaltyCampaign.StatusId);
            return View(loyaltyCampaign);
        }

        // POST: LoyaltyCampaign/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("LoyaltyCampaignId,ExclusiveCompanyCnpj,StatusId,Name,Description,StartDate,EndDate,Rules,NumberStampsTotalMax,NumberStampsLimitMaxAtTime,NumberStampsLimitMinAtTime,IsActive")] LoyaltyCampaign loyaltyCampaign)
        {
            if (id != loyaltyCampaign.LoyaltyCampaignId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(loyaltyCampaign);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LoyaltyCampaignExists(loyaltyCampaign.LoyaltyCampaignId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ExclusiveCompanyCnpj"] = new SelectList(_context.Company, "Cnpj", "Cnpj", loyaltyCampaign.ExclusiveCompanyCnpj);
            ViewData["StatusId"] = new SelectList(_context.LoyaltyCampaignStatus, "LoyaltyCampaignStatusId", "LoyaltyCampaignStatusId", loyaltyCampaign.StatusId);
            return View(loyaltyCampaign);
        }

        // GET: LoyaltyCampaign/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var loyaltyCampaign = await _context.LoyaltyCampaign
                .Include(l => l.ExclusiveCompanyCnpjNavigation)
                .Include(l => l.Status)
                .FirstOrDefaultAsync(m => m.LoyaltyCampaignId == id);
            if (loyaltyCampaign == null)
            {
                return NotFound();
            }

            return View(loyaltyCampaign);
        }

        // POST: LoyaltyCampaign/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var loyaltyCampaign = await _context.LoyaltyCampaign.FindAsync(id);
            _context.LoyaltyCampaign.Remove(loyaltyCampaign);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LoyaltyCampaignExists(long id)
        {
            return _context.LoyaltyCampaign.Any(e => e.LoyaltyCampaignId == id);
        }
    }
}
