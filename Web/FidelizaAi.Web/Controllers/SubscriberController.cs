﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using FidelizaAi.Web.Data;
using FidelizaAi.Web.Models.Domain;

namespace FidelizaAi.Web.Controllers
{
    public class SubscriberController : Controller
    {
        private readonly FidelizaaiContext _context;

        public SubscriberController(FidelizaaiContext context)
        {
            _context = context;
        }

        // GET: Subscriber
        public async Task<IActionResult> Index()
        {
            var fidelizaaiContext = _context.Subscriber.Include(s => s.Owner).Include(s => s.Plan);
            return View(await fidelizaaiContext.ToListAsync());
        }

        // GET: Subscriber/Details/5
        public async Task<IActionResult> Details(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subscriber = await _context.Subscriber
                .Include(s => s.Owner)
                .Include(s => s.Plan)
                .FirstOrDefaultAsync(m => m.SubscriberId == id);
            if (subscriber == null)
            {
                return NotFound();
            }

            return View(subscriber);
        }

        // GET: Subscriber/Create
        public IActionResult Create()
        {
            ViewData["OwnerId"] = new SelectList(_context.User, "UserId", "Name");
            ViewData["PlanId"] = new SelectList(_context.Plan, "PlanId", "Name");
            return View();
        }

        // POST: Subscriber/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SubscriberId,OwnerId,PlanId,Name,Document,RegisterDate,StartContractDate,EndContractDate,IsActive,IsBlocked")] Subscriber subscriber)
        {
            if (ModelState.IsValid)
            {
                _context.Add(subscriber);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["OwnerId"] = new SelectList(_context.User, "UserId", "UserId", subscriber.OwnerId);
            ViewData["PlanId"] = new SelectList(_context.Plan, "PlanId", "PlanId", subscriber.PlanId);
            return View(subscriber);
        }

        // GET: Subscriber/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subscriber = await _context.Subscriber.FindAsync(id);
            if (subscriber == null)
            {
                return NotFound();
            }
            ViewData["OwnerId"] = new SelectList(_context.User, "UserId", "UserId", subscriber.OwnerId);
            ViewData["PlanId"] = new SelectList(_context.Plan, "PlanId", "PlanId", subscriber.PlanId);
            return View(subscriber);
        }

        // POST: Subscriber/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("SubscriberId,OwnerId,PlanId,Name,Document,RegisterDate,StartContractDate,EndContractDate,IsActive,IsBlocked")] Subscriber subscriber)
        {
            if (id != subscriber.SubscriberId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(subscriber);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SubscriberExists(subscriber.SubscriberId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["OwnerId"] = new SelectList(_context.User, "UserId", "UserId", subscriber.OwnerId);
            ViewData["PlanId"] = new SelectList(_context.Plan, "PlanId", "PlanId", subscriber.PlanId);
            return View(subscriber);
        }

        // GET: Subscriber/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subscriber = await _context.Subscriber
                .Include(s => s.Owner)
                .Include(s => s.Plan)
                .FirstOrDefaultAsync(m => m.SubscriberId == id);
            if (subscriber == null)
            {
                return NotFound();
            }

            return View(subscriber);
        }

        // POST: Subscriber/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
            var subscriber = await _context.Subscriber.FindAsync(id);
            _context.Subscriber.Remove(subscriber);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SubscriberExists(long id)
        {
            return _context.Subscriber.Any(e => e.SubscriberId == id);
        }
    }
}
