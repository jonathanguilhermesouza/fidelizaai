﻿using FidelizaAi.Web.Models.AccountViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FidelizaAi.Web.Controllers
{
    public class BaseController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        public HttpResponseMessage ResponseMessage;
        public BaseController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //public async Task<IActionResult> Response(object result, string success, string error, Dictionary<string, string> notifications)
        //{
        //    if (notifications.Count > 0)
        //    {
        //        return BadRequest(new { success = false, message = error, data = result, errors = notifications });
        //    }
        //    else
        //    {
        //        try
        //        {
        //            return Ok(new { success = true, message = success, data = result, errors = "" });
        //        }
        //        catch
        //        {
        //            return BadRequest(new { success = false, message = "Ocorreu um erro no servidor" });
        //        }
        //    }
        //}

        //https://www.wellingtonjhn.com/posts/autenticação-em-apis-asp.net-core-com-jwt---refresh-token/
        //https://www.wellingtonjhn.com/posts/autenticação-em-apis-asp.net-core-com-jwt/
        //https://jasonwatmore.com/post/2018/08/14/aspnet-core-21-jwt-authentication-tutorial-with-example-api
        //https://imasters.com.br/back-end/asp-net-core-implementando-e-consumindo-json-web-tokens-jwt
        protected UserTokenViewModel GetAccessToken(ExternalLoginViewModel userInfo)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, userInfo.Email),
                new Claim(ClaimTypes.Email, userInfo.Email),
                new Claim(ClaimTypes.Role, userInfo.Role)
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["jwt:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            // tempo de expiração do token: 1 hora
            var expiration = DateTime.UtcNow.AddMinutes(1);
            JwtSecurityToken token = new JwtSecurityToken(
               issuer: _configuration["jwt:issuer"],
               audience: _configuration["jwt:audience"],
               claims: claims,
               expires: expiration,
               signingCredentials: creds);
            return new UserTokenViewModel()
            {
                is_loged = true,
                access_token = new JwtSecurityTokenHandler().WriteToken(token),
                expires_in = expiration,
                token_type = "bearer"
            };
        }

        protected ClaimsPrincipal SetClaims(ExternalLoginViewModel login)
        {
            var identity = new ClaimsIdentity();

            //identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, login.Id));
            identity.AddClaim(new Claim(ClaimTypes.Name, login.Name));
            //identity.AddClaim(new Claim(ClaimTypes.GivenName, login.GivenName));
            //identity.AddClaim(new Claim(ClaimTypes.Surname, login.FamilyName));
            identity.AddClaim(new Claim(ClaimTypes.Email, login.Email));
            identity.AddClaim(new Claim(ClaimTypes.Role, login.Role));
            identity.AddClaim(new Claim("Provider", login.Provider));

            return new ClaimsPrincipal(identity);
        }
    }
}
