﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FidelizaAi.Web.Models.Enum
{
    public enum EResponseAccount
    {
        UserNotFound = 90,
        NullData = 91,
        ModelStateInvalid = 92,
        CreateUserError = 93,
        Catch = 94,
        Sucess = 95,
        UserOrPasswordIncorrect = 96
    }
}
