﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FidelizaAi.Web.Models.AccountViewModels
{
    public class UserTokenViewModel
    {
        public string access_token { get; set; }
        public DateTime expires_in { get; set; }
        public string token_type { get; set; }
        public bool is_loged { get; set; }
    }
}
