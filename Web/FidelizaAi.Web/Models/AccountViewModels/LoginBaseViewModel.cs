﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FidelizaAi.Web.Models.AccountViewModels
{
    public class LoginBaseViewModel
    {
        public string Role { get; set; }
    }
}
