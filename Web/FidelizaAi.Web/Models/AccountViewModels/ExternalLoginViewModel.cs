﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FidelizaAi.Web.Models.AccountViewModels
{
    public class ExternalLoginViewModel : LoginBaseViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Photo { get; set; }
        [Required]
        public string FamilyName { get; set; }
        [Required]
        public string GivenName { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Id { get; set; }
        [Required]
        public string Provider { get; set; }
        public string LastName { get; set; }
    }
}
