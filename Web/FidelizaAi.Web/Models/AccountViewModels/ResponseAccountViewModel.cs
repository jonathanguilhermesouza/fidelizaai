﻿using FidelizaAi.Web.Models.Enum;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace FidelizaAi.Web.Models.AccountViewModels
{
    public class ResponseAccountViewModel
    {
        public ResponseAccountViewModel(HttpStatusCode statusCode, EResponseAccount responseAccount, object result)
        {
            this.Data = result;
            this.StatusCodeAccount = responseAccount;
            this.StatusCode = statusCode;
        }
        public ResponseAccountViewModel(HttpStatusCode statusCode, EResponseAccount responseAccount)
        {
            this.StatusCodeAccount = responseAccount;
            this.StatusCode = statusCode;
        }
        public HttpStatusCode StatusCode { get; set; }
        public object Data { get; set; }
        public EResponseAccount StatusCodeAccount { get; set; }
    }
}
