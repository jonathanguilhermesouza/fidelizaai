﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FidelizaAi.Web.Models.ViewModel
{
    public class UserListItemViewModel
    {
        public string Name { get; set; }
        public string IconPath { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Frequence { get; set; }
        public string WhatsApp { get; set; }
    }
}
