﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FidelizaAi.Web.Models.ViewModel
{
    public class MarketplaceDetailViewModel
    {
        public string Name { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string WhatsApp { get; set; }
        public string Email { get; set; }
        public string FullAddress { get; set; }
        public string Site { get; set; }
        public string Facebook { get; set; }
        public string Instagram { get; set; }
        public IEnumerable<string> Galery { get; set; }
        public IEnumerable<LoyaltyCampaingnListItemViewModel> ListLoyaltyCampaingn { get; set; }
    }
}
