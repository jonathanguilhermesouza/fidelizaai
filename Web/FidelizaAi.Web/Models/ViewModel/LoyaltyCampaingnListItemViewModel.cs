﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FidelizaAi.Web.Models.ViewModel
{
    public class LoyaltyCampaingnListItemViewModel
    {
        public long LoyaltyCampaignId { get; set; }
        public string Name { get; set; }
        public int? NumberStampsTotalMax { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
