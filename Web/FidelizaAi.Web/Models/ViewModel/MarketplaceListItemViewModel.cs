﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FidelizaAi.Web.Models.ViewModel
{
    public class MarketplaceListItemViewModel
    {
        public string CompanyId { get; set; }
        public string IconPath { get; set; }
        public string Name { get; set; }
        public string CompanyType { get; set; }
    }
}
