USE [FidelizaAi]
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'467FA8B1-9679-4511-9FC7-C77E9EA42AD1', N'Customer', N'CUSTOMER', NULL)
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'795A042D-3F63-4859-B331-DE1D9C66A323', N'Partner', N'PARTNER', NULL)
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'8D113CAE-CE6B-4DDE-BCF0-84328D33B864', N'Master', N'MASTER', NULL)
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'BA639665-F1A9-447E-8593-0E9323DD5493', N'CommonUser', N'COMMONUSER', NULL)
