USE [master]
GO
/****** Object:  Database [FidelizaAi]    Script Date: 28/12/2019 15:18:43 ******/
CREATE DATABASE [FidelizaAi]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FidelizaAi', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER2017\MSSQL\DATA\FidelizaAi.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'FidelizaAi_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER2017\MSSQL\DATA\FidelizaAi_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [FidelizaAi] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FidelizaAi].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FidelizaAi] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FidelizaAi] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FidelizaAi] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FidelizaAi] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FidelizaAi] SET ARITHABORT OFF 
GO
ALTER DATABASE [FidelizaAi] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FidelizaAi] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FidelizaAi] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FidelizaAi] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FidelizaAi] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FidelizaAi] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FidelizaAi] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FidelizaAi] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FidelizaAi] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FidelizaAi] SET  DISABLE_BROKER 
GO
ALTER DATABASE [FidelizaAi] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FidelizaAi] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FidelizaAi] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FidelizaAi] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FidelizaAi] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FidelizaAi] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [FidelizaAi] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FidelizaAi] SET RECOVERY FULL 
GO
ALTER DATABASE [FidelizaAi] SET  MULTI_USER 
GO
ALTER DATABASE [FidelizaAi] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FidelizaAi] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FidelizaAi] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FidelizaAi] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [FidelizaAi] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'FidelizaAi', N'ON'
GO
ALTER DATABASE [FidelizaAi] SET QUERY_STORE = OFF
GO
USE [FidelizaAi]
GO
/****** Object:  Schema [ACCOUNT]    Script Date: 28/12/2019 15:18:43 ******/
CREATE SCHEMA [ACCOUNT]
GO
/****** Object:  Schema [APP]    Script Date: 28/12/2019 15:18:43 ******/
CREATE SCHEMA [APP]
GO
/****** Object:  Schema [BKO]    Script Date: 28/12/2019 15:18:43 ******/
CREATE SCHEMA [BKO]
GO
/****** Object:  Schema [SIS]    Script Date: 28/12/2019 15:18:43 ******/
CREATE SCHEMA [SIS]
GO
/****** Object:  Table [ACCOUNT].[Profile]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ACCOUNT].[Profile](
	[ProfileId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](20) NULL,
	[Description] [nvarchar](100) NULL,
 CONSTRAINT [PK_Profile] PRIMARY KEY CLUSTERED 
(
	[ProfileId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ACCOUNT].[User]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ACCOUNT].[User](
	[UserId] [nvarchar](450) NOT NULL,
	[ProfileId] [int] NOT NULL,
	[Name] [nvarchar](20) NULL,
	[LastName] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[Cpf] [nvarchar](11) NULL,
	[DateOfBirth] [datetime2](7) NULL,
	[PhoneNumber1] [nvarchar](20) NULL,
	[PhoneNumber2] [nvarchar](20) NULL,
	[WhatsApp] [nvarchar](20) NULL,
	[Cep] [nvarchar](8) NULL,
	[Logradouro] [nvarchar](30) NULL,
	[Complemento] [nvarchar](20) NULL,
	[Bairro] [nvarchar](20) NULL,
	[Localidade] [nvarchar](40) NULL,
	[Uf] [nchar](2) NULL,
	[IsActive] [bit] NULL,
	[IsBlocked] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [APP].[CompanyLoyaltyCampaingn]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [APP].[CompanyLoyaltyCampaingn](
	[LoyaltyCampaingnId] [bigint] NOT NULL,
	[CompanyCnpj] [nvarchar](14) NOT NULL,
 CONSTRAINT [PK_CompanyLoyaltyCampaingn] PRIMARY KEY CLUSTERED 
(
	[LoyaltyCampaingnId] ASC,
	[CompanyCnpj] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [APP].[LoyaltyCampaign]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [APP].[LoyaltyCampaign](
	[LoyaltyCampaignId] [bigint] IDENTITY(1,1) NOT NULL,
	[ExclusiveCompanyCnpj] [nvarchar](14) NULL,
	[StatusId] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Description] [nvarchar](300) NULL,
	[StartDate] [datetime2](7) NULL,
	[EndDate] [datetime2](7) NULL,
	[Rules] [nvarchar](max) NULL,
	[NumberStampsTotalMax] [int] NULL,
	[NumberStampsLimitMaxAtTime] [int] NULL,
	[NumberStampsLimitMinAtTime] [int] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_LoyaltyCampaign] PRIMARY KEY CLUSTERED 
(
	[LoyaltyCampaignId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [APP].[Notification]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [APP].[Notification](
	[NotificationId] [bigint] IDENTITY(1,1) NOT NULL,
	[CompanyCnpj] [nvarchar](14) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[NotificationTypeId] [int] NOT NULL,
	[IsRead] [bit] NULL,
	[Title] [nvarchar](20) NULL,
	[Description] [nvarchar](100) NULL,
	[RegisterDate] [datetime2](7) NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[NotificationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [APP].[StampRegister]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [APP].[StampRegister](
	[StampRegisterId] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[LoyaltyCampaingnId] [bigint] NOT NULL,
	[NumberStamps] [int] NULL,
	[RegisterDate] [datetime2](7) NULL,
	[Description] [nvarchar](100) NULL,
 CONSTRAINT [PK_UserLoyaltyCampaingn] PRIMARY KEY CLUSTERED 
(
	[StampRegisterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [BKO].[Company]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [BKO].[Company](
	[Cnpj] [nvarchar](14) NOT NULL,
	[SubscriberId] [bigint] NOT NULL,
	[CompanyTypeId] [int] NOT NULL,
	[CorporateName] [nvarchar](150) NULL,
	[FantasyName] [nvarchar](100) NULL,
	[PhoneNumber1] [nvarchar](20) NULL,
	[PhoneNumber2] [nvarchar](20) NULL,
	[WhatsApp] [nvarchar](20) NULL,
	[Email] [nvarchar](100) NULL,
	[CnpjMatriz] [nvarchar](14) NULL,
	[Cep] [nvarchar](8) NULL,
	[Logradouro] [nvarchar](30) NULL,
	[Complemento] [nvarchar](20) NULL,
	[Bairro] [nvarchar](20) NULL,
	[Localidade] [nvarchar](40) NULL,
	[Uf] [nchar](2) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[Cnpj] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [BKO].[Plan]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [BKO].[Plan](
	[PlanId] [bigint] IDENTITY(1,1) NOT NULL,
	[MonthPrice] [decimal](19, 2) NULL,
	[AnualPrice] [decimal](19, 2) NULL,
	[StartDate] [datetime2](7) NULL,
	[EndDate] [datetime2](7) NULL,
	[Name] [nvarchar](20) NULL,
	[Description] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Plan] PRIMARY KEY CLUSTERED 
(
	[PlanId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [BKO].[Subscriber]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [BKO].[Subscriber](
	[SubscriberId] [bigint] IDENTITY(1,1) NOT NULL,
	[OwnerId] [nvarchar](450) NOT NULL,
	[PlanId] [bigint] NOT NULL,
	[Name] [nvarchar](20) NULL,
	[Document] [nvarchar](14) NULL,
	[RegisterDate] [datetime2](7) NULL,
	[StartContractDate] [datetime2](7) NULL,
	[EndContractDate] [datetime2](7) NULL,
	[IsActive] [bit] NULL,
	[IsBlocked] [bit] NULL,
 CONSTRAINT [PK_Subscriber] PRIMARY KEY CLUSTERED 
(
	[SubscriberId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NULL,
	[CpfCnpj] [nvarchar](14) NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [SIS].[CompanyType]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SIS].[CompanyType](
	[CompanyTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](20) NULL,
	[Description] [nvarchar](100) NULL,
 CONSTRAINT [PK_CompanyType] PRIMARY KEY CLUSTERED 
(
	[CompanyTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [SIS].[LoyaltyCampaignStatus]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SIS].[LoyaltyCampaignStatus](
	[LoyaltyCampaignStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](20) NULL,
	[Description] [nvarchar](100) NULL,
 CONSTRAINT [PK_LoyaltyCampaignStatus] PRIMARY KEY CLUSTERED 
(
	[LoyaltyCampaignStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [SIS].[NotificationType]    Script Date: 28/12/2019 15:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [SIS].[NotificationType](
	[NotificationTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](20) NULL,
	[Descrpition] [nvarchar](100) NULL,
	[ColorHexa] [nvarchar](7) NULL,
 CONSTRAINT [PK_NotificationType] PRIMARY KEY CLUSTERED 
(
	[NotificationTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [ACCOUNT].[Profile] ON 

INSERT [ACCOUNT].[Profile] ([ProfileId], [Name], [Description]) VALUES (1, N'Comum', N'Comum')
INSERT [ACCOUNT].[Profile] ([ProfileId], [Name], [Description]) VALUES (2, N'Administrador', N'Administrador')
SET IDENTITY_INSERT [ACCOUNT].[Profile] OFF
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'00000000000000_CreateIdentitySchema', N'2.2.4-servicing-10062')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'467FA8B1-9679-4511-9FC7-C77E9EA42AD1', N'Customer', N'CUSTOMER', NULL)
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'795A042D-3F63-4859-B331-DE1D9C66A323', N'Partner', N'PARTNER', NULL)
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'8D113CAE-CE6B-4DDE-BCF0-84328D33B864', N'Master', N'MASTER', NULL)
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'BA639665-F1A9-447E-8593-0E9323DD5493', N'CommonUser', N'COMMONUSER', NULL)
SET IDENTITY_INSERT [dbo].[AspNetUserClaims] ON 

INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (26, N'84f1aba8-b13a-4911-adab-5477aa07ee57', N'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name', N'Jonathan Guilherme - Compartilhando conhecimento')
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (27, N'84f1aba8-b13a-4911-adab-5477aa07ee57', N'http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress', N'jonathanguilhermesouza@gmail.com')
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (28, N'84f1aba8-b13a-4911-adab-5477aa07ee57', N'http://schemas.microsoft.com/ws/2008/06/identity/claims/role', N'CommonUser')
INSERT [dbo].[AspNetUserClaims] ([Id], [UserId], [ClaimType], [ClaimValue]) VALUES (29, N'84f1aba8-b13a-4911-adab-5477aa07ee57', N'Provider', N'Google')
SET IDENTITY_INSERT [dbo].[AspNetUserClaims] OFF
INSERT [dbo].[AspNetUserLogins] ([LoginProvider], [ProviderKey], [ProviderDisplayName], [UserId]) VALUES (N'Google', N'102078863567030420485', N'Jonathan Guilherme - Compartilhando conhecimento', N'84f1aba8-b13a-4911-adab-5477aa07ee57')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [Name], [LastName], [CpfCnpj]) VALUES (N'84f1aba8-b13a-4911-adab-5477aa07ee57', N'jonathanguilhermesouza@gmail.com', N'JONATHANGUILHERMESOUZA@GMAIL.COM', N'jonathanguilhermesouza@gmail.com', N'JONATHANGUILHERMESOUZA@GMAIL.COM', 1, N'AQAAAAEAACcQAAAAEJHoK+kzNGYcaVAz+k0dqVrqqST0sC/zqXikLMak/fzbMEtBFT3a5UtjJjNiGqsupA==', N'ANZ7Q5X2TOOYKMJ6K2TSFQT3SOR3QIAW', N'34160f1e-0518-4093-9217-9fe083e96918', NULL, 0, 0, NULL, 1, 0, N'Jonathan Guilherme - Compartilhando conhecimento', NULL, NULL)
SET IDENTITY_INSERT [SIS].[CompanyType] ON 

INSERT [SIS].[CompanyType] ([CompanyTypeId], [Name], [Description]) VALUES (1, N'Matriz', N'Matriz')
INSERT [SIS].[CompanyType] ([CompanyTypeId], [Name], [Description]) VALUES (2, N'Filial', N'Filial')
SET IDENTITY_INSERT [SIS].[CompanyType] OFF
SET IDENTITY_INSERT [SIS].[LoyaltyCampaignStatus] ON 

INSERT [SIS].[LoyaltyCampaignStatus] ([LoyaltyCampaignStatusId], [Name], [Description]) VALUES (1, N'Em andamento', N'Em andamento')
INSERT [SIS].[LoyaltyCampaignStatus] ([LoyaltyCampaignStatusId], [Name], [Description]) VALUES (2, N'Finalizada', N'Finalizada')
INSERT [SIS].[LoyaltyCampaignStatus] ([LoyaltyCampaignStatusId], [Name], [Description]) VALUES (3, N'Pausada', N'Pausada')
SET IDENTITY_INSERT [SIS].[LoyaltyCampaignStatus] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetRoleClaims_RoleId]    Script Date: 28/12/2019 15:18:43 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId] ON [dbo].[AspNetRoleClaims]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 28/12/2019 15:18:43 ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserClaims_UserId]    Script Date: 28/12/2019 15:18:43 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserLogins_UserId]    Script Date: 28/12/2019 15:18:43 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 28/12/2019 15:18:43 ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 28/12/2019 15:18:43 ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 28/12/2019 15:18:43 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [ACCOUNT].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Profile] FOREIGN KEY([ProfileId])
REFERENCES [ACCOUNT].[Profile] ([ProfileId])
GO
ALTER TABLE [ACCOUNT].[User] CHECK CONSTRAINT [FK_User_Profile]
GO
ALTER TABLE [APP].[CompanyLoyaltyCampaingn]  WITH CHECK ADD  CONSTRAINT [FK_CompanyLoyaltyCampaingn_Company] FOREIGN KEY([CompanyCnpj])
REFERENCES [BKO].[Company] ([Cnpj])
GO
ALTER TABLE [APP].[CompanyLoyaltyCampaingn] CHECK CONSTRAINT [FK_CompanyLoyaltyCampaingn_Company]
GO
ALTER TABLE [APP].[CompanyLoyaltyCampaingn]  WITH CHECK ADD  CONSTRAINT [FK_CompanyLoyaltyCampaingn_LoyaltyCampaign] FOREIGN KEY([LoyaltyCampaingnId])
REFERENCES [APP].[LoyaltyCampaign] ([LoyaltyCampaignId])
GO
ALTER TABLE [APP].[CompanyLoyaltyCampaingn] CHECK CONSTRAINT [FK_CompanyLoyaltyCampaingn_LoyaltyCampaign]
GO
ALTER TABLE [APP].[LoyaltyCampaign]  WITH CHECK ADD  CONSTRAINT [FK_LoyaltyCampaign_Company] FOREIGN KEY([ExclusiveCompanyCnpj])
REFERENCES [BKO].[Company] ([Cnpj])
GO
ALTER TABLE [APP].[LoyaltyCampaign] CHECK CONSTRAINT [FK_LoyaltyCampaign_Company]
GO
ALTER TABLE [APP].[LoyaltyCampaign]  WITH CHECK ADD  CONSTRAINT [FK_LoyaltyCampaign_LoyaltyCampaignStatus] FOREIGN KEY([StatusId])
REFERENCES [SIS].[LoyaltyCampaignStatus] ([LoyaltyCampaignStatusId])
GO
ALTER TABLE [APP].[LoyaltyCampaign] CHECK CONSTRAINT [FK_LoyaltyCampaign_LoyaltyCampaignStatus]
GO
ALTER TABLE [APP].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_Company] FOREIGN KEY([CompanyCnpj])
REFERENCES [BKO].[Company] ([Cnpj])
GO
ALTER TABLE [APP].[Notification] CHECK CONSTRAINT [FK_Notification_Company]
GO
ALTER TABLE [APP].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_NotificationType] FOREIGN KEY([NotificationTypeId])
REFERENCES [SIS].[NotificationType] ([NotificationTypeId])
GO
ALTER TABLE [APP].[Notification] CHECK CONSTRAINT [FK_Notification_NotificationType]
GO
ALTER TABLE [APP].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_User] FOREIGN KEY([UserId])
REFERENCES [ACCOUNT].[User] ([UserId])
GO
ALTER TABLE [APP].[Notification] CHECK CONSTRAINT [FK_Notification_User]
GO
ALTER TABLE [APP].[StampRegister]  WITH CHECK ADD  CONSTRAINT [FK_StampRegister_LoyaltyCampaign] FOREIGN KEY([LoyaltyCampaingnId])
REFERENCES [APP].[LoyaltyCampaign] ([LoyaltyCampaignId])
GO
ALTER TABLE [APP].[StampRegister] CHECK CONSTRAINT [FK_StampRegister_LoyaltyCampaign]
GO
ALTER TABLE [APP].[StampRegister]  WITH CHECK ADD  CONSTRAINT [FK_UserLoyaltyCampaingn_User] FOREIGN KEY([UserId])
REFERENCES [ACCOUNT].[User] ([UserId])
GO
ALTER TABLE [APP].[StampRegister] CHECK CONSTRAINT [FK_UserLoyaltyCampaingn_User]
GO
ALTER TABLE [BKO].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_CompanyType] FOREIGN KEY([CompanyTypeId])
REFERENCES [SIS].[CompanyType] ([CompanyTypeId])
GO
ALTER TABLE [BKO].[Company] CHECK CONSTRAINT [FK_Company_CompanyType]
GO
ALTER TABLE [BKO].[Company]  WITH CHECK ADD  CONSTRAINT [FK_Company_Subscriber] FOREIGN KEY([SubscriberId])
REFERENCES [BKO].[Subscriber] ([SubscriberId])
GO
ALTER TABLE [BKO].[Company] CHECK CONSTRAINT [FK_Company_Subscriber]
GO
ALTER TABLE [BKO].[Subscriber]  WITH CHECK ADD  CONSTRAINT [FK_Subscriber_Plan] FOREIGN KEY([PlanId])
REFERENCES [BKO].[Plan] ([PlanId])
GO
ALTER TABLE [BKO].[Subscriber] CHECK CONSTRAINT [FK_Subscriber_Plan]
GO
ALTER TABLE [BKO].[Subscriber]  WITH CHECK ADD  CONSTRAINT [FK_Subscriber_User] FOREIGN KEY([OwnerId])
REFERENCES [ACCOUNT].[User] ([UserId])
GO
ALTER TABLE [BKO].[Subscriber] CHECK CONSTRAINT [FK_Subscriber_User]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
USE [master]
GO
ALTER DATABASE [FidelizaAi] SET  READ_WRITE 
GO
